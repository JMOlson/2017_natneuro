foldedhdRateMaps = nanmean(cat(4,squeeze(hdRateMaps(1:36,:,:,1)),squeeze(hdRateMaps(37:72,:,:,1))),4);

for iOverallNeuron = 1:235
[pValHDrTest(iOverallNeuron), ~] = circ_rtest(hdRateMaps(:,iOverallNeuron,2,1));
[pValHDoTest(iOverallNeuron), ~] = circ_otest(hdRateMaps(:,iOverallNeuron,2,1));
[pValHDAxisrTest(iOverallNeuron), ~] = circ_rtest(foldedhdRateMaps(:,iOverallNeuron,2));
[pValHDAxisoTest(iOverallNeuron), ~] = circ_otest(foldedhdRateMaps(:,iOverallNeuron,2));

[pValHDrTestPlat(iOverallNeuron), ~] = circ_rtest(hdRateMaps(:,iOverallNeuron,1,1));
[pValHDoTestPlat(iOverallNeuron), ~] = circ_otest(hdRateMaps(:,iOverallNeuron,1,1));
[pValHDAxisrTestPlat(iOverallNeuron), ~] = circ_rtest(foldedhdRateMaps(:,iOverallNeuron,1));
[pValHDAxisoTestPlat(iOverallNeuron), ~] = circ_otest(foldedhdRateMaps(:,iOverallNeuron,1));
end

resultantHD = circ_r(hdRateMaps(:,:,2,1));
resultantAxis = circ_r(foldedhdRateMaps(:,:,2));

[~,ind] = max(hdRateMaps(:,:,2,1));
for i=1:235
    maxHDFR(i) = hdRateMaps(ind(i),i,2,1);
    if ind(i) == 36
        oppHDFR(i) = hdRateMaps(72,i,2,1);
        ninetyPlusHDFR(i) = hdRateMaps(54,i,2,1);
        ninetyMinusHDFR(i) = hdRateMaps(18,i,2,1);
    elseif ind(i) == 18
        oppHDFR(i) = hdRateMaps(54,i,2,1);
        ninetyPlusHDFR(i) = hdRateMaps(36,i,2,1);
        ninetyMinusHDFR(i) = hdRateMaps(72,i,2,1);
    elseif ind(i) == 54
        oppHDFR(i) = hdRateMaps(18,i,2,1);
        ninetyPlusHDFR(i) = hdRateMaps(72,i,2,1);
        ninetyMinusHDFR(i) = hdRateMaps(36,i,2,1);
    else
        oppHDFR(i) = hdRateMaps(mod(ind(i)+36,72),i,2,1);
        ninetyPlusHDFR(i) = hdRateMaps(mod(ind(i)+18,72),i,2,1);
        ninetyMinusHDFR(i) = hdRateMaps(mod(ind(i)+54,72),i,2,1);
    end
end
onAxisAvg = maxHDFR+oppHDFR/2;
offAxisAvg = ninetyPlusHDFR+ninetyMinusHDFR/2;

figure;
scatter(onAxisAvg(resultantHD<0.5),offAxisAvg(resultantHD<0.5));
hold on;
scatter(onAxisAvg(114),offAxisAvg(114),'r','filled')
scatter(onAxisAvg(28),offAxisAvg(28),'r','filled')
scatter(onAxisAvg(124),offAxisAvg(124),'g','filled')
scatter(onAxisAvg(22),offAxisAvg(22),'r','filled')
scatter(onAxisAvg(203),offAxisAvg(203),'r','filled')
scatter(onAxisAvg(121),offAxisAvg(121),'k','filled')
scatter(onAxisAvg(3),offAxisAvg(3),'r','filled')
scatter(onAxisAvg(13),offAxisAvg(13),'r','filled')
scatter(onAxisAvg(13),offAxisAvg(197),'r','filled')
scatter(onAxisAvg(197),offAxisAvg(197),'r','filled')
scatter(onAxisAvg(13),offAxisAvg(197),'w','filled')
scatter(onAxisAvg(206),offAxisAvg(206),'r','filled')


axisWeSay = [114,28,22,203,3,13,197,206];
figure;
scatter(onAxisAvg(~noFireTrack)./offAxisAvg(~noFireTrack),resultantAxis(~noFireTrack),12,log(maxHDFR(~noFireTrack)),'o','filled')
hold on;
scatter(onAxisAvg(axisWeSay)./offAxisAvg(axisWeSay),resultantAxis(axisWeSay),48,log(maxHDFR(axisWeSay)),'o','filled')
axis([0 25 0 1])

figure;
scatter(onAxisAvg(~noFireTrack),offAxisAvg(~noFireTrack),12,1-resultantAxis(~noFireTrack)','filled')
hold on;
scatter(onAxisAvg(axisWeSay),offAxisAvg(axisWeSay),48,'ko','filled')


