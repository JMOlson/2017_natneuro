%axis cell paper figure plots

%% Figure 1
% load('SubPaperMainAxisCellResultsRunning.mat')
% load('SubPaperMainDataStruct.mat')
% load('SubPaperMainNeuronStruct.mat')
% load('2DRMapsPlottingOptions','fullPlatformIsRunning2DRMapsNanConv', 'fullTrackIsRunning2DRMapsNanConv');
 NS = SubPaperNeuronStruct;
 DS = SubPaperDataStruct;
overallFR = SubPaperNeuronStruct.overallMeanFR;
load('figureColormap.mat');
mapToUse = hotMapClipped;

selected = [255,467,423,11];
selected = axisCells;

selected = hdOnTrackNeurons;
% selected = [296,38]
neuronsToPlot = selected;
for i = 1:length(neuronsToPlot)
    maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
    caxisMax = 2*overallFR(neuronsToPlot(i),2);
    
    figure(1)
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));

%     scaleModelVals = maxHDRate/max(modelHDVals(:,3,neuronsToPlot(i)));
%     polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
%         modelHDVals(:,3,neuronsToPlot(i))*scaleModelVals,'k');
    hold off;
    
    figure(2)
    sc(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    
    figure(3)
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),1,1));
    hold off;
    
    figure(4)
    sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    caxisMax
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    disp(neuronsToPlot(i));
    pause;
end




%% Figure 3
% load('SubPaperRotationsFIGURE2DRMAPS.mat')
% NS = SubPaperRotationsNeuronStruct;
% DS = SubPaperRotationsDataStruct;
% overallFR = SubPaperRotationsNeuronStruct.overallMeanFR;
% load('figureColormap.mat');
% mapToUse = hotMapClipped;

selected = [6,16,100,157];

neuronsToPlot = 255;
for i = 1:length(neuronsToPlot)
    maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
    caxisMax = 2*overallFR(neuronsToPlot(i),2);
    
    figure
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
    hold off;
    
    figure
    sc(fullTrackNormalIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackNormalIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    
    figure
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),3,1));
    hold off;
    
    figure
    sc(fullTrackRotatedIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackRotatedIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    caxisMax
    disp(neuronsToPlot(i));
    pause;
end

%% Supplemental Figure 6
% load('D:\AxisCellPaper\FinalAnalysisResults\MainDataset\SubPaperMainTrackRunningCategorizedAxisCells.mat')
axisCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;

% load('twoDPlatformSmoothed.mat')

load('figureColormap.mat');
mapToUse = hotMapClipped;
% downsampled to 1cm by 1cm bins
% Filter settings: 5bins for a width of the filter (11 bins)
% gaussian filter with std dev of 2 bins

neuronsToPlot = axisCells;
 for i = 1:47
cMax = max(max(twoDPlatformSmoothed(6:50,6:50,neuronsToPlot(i))));
    
    figure(4)
    sc(twoDPlatformSmoothed(6:50,6:50,neuronsToPlot(i)),[0,cMax],mapToUse,'w',...
        isnan(twoDPlatformAllData(:,:,neuronsToPlot(i),1)));
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    fprintf(['Neuron %d\nArena Max Firing Rate %4.0f Hz\n\n'],...
        neuronsToPlot(i),cMax)
pause;
end

%% prepping for sup fig 6
for i = 1:47
    maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
    caxisMax = 2*overallFR(neuronsToPlot(i),2);
figure(5)
subplot(7,7,i)
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),1,1));
    hold off;
    
    figure(6)
subplot(7,7,i)
    sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    
    figure(7)
subplot(7,7,i)
    maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),1,1))));
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),1,1));
    hold off;
end

%% could have been part of sup fig 5 
[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainTrackRunning', 2, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
scatter(cumsum(ones(8,542)),sseCrossValRatio);
scatter(cumsum(ones(8,542),1),sseCrossValRatio);
indices = cumsum(ones(8,542));
scatter(indices(:),sseCrossValRatio(:))
scatter(indices(:),sseCrossValDiff(:))
scatter(indices(:),sseCrossValRatopDiff(:))
scatter(indices(:),sseCrossValRatioDiff(:))
indicesAxis = cumsum(ones(8,47));
sseCrossValRatioTrackAxis = sseCrossValRatio(:,jakeSaysAxis);
scatter(indicesAxis(:),sseCrossValRatioTrackAxis(:))
plot(sseCrossValRatioTrackAxis)
plot(sseCrossValRatioTrackAxis,'k')
plot(sseCrossValRatioDiff(:,jakeSaysAxis),'k')
hold on;
plot(sseCrossValRatioDiff,'k')
plot(sseCrossValRatioDiff(:,jakeSaysAxis,'r')
plot(sseCrossValRatioDiff(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatioDiff(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatio,'k')
hold on;
plot(sseCrossValRatio(:,jakeSaysAxis,'r')
plot(sseCrossValRatio(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatio(:,jakeSaysAxis),'r')
dbquit
[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainPlatformRunning', 1, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
figure;
plot(sseCrossValRatioDiff,'k')
plot(sseCrossValRatioDiff(:,jakeSaysAxis,'r')
hold on;
plot(sseCrossValRatioDiff(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatioDiff(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatio,'k')
hold on;
plot(sseCrossValRatio(:,jakeSaysAxis),'r')
figure;
plot(sseCrossValRatio(:,jakeSaysAxis),'r')
