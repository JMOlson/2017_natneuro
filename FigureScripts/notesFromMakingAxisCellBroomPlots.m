clear bigPeakAngle smallPeakAngle offsetAngleFromMax
% allCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;
iTimeSegment = 2;
allCells = [1:542];
nNeurons = length(allCells);
% load('D:\subPaper\FinalAnalysisResults\MainDataset\mixVMMMainPlatformRunning.mat')
axisPeaks = squeeze(thetaEMMain(1:2,3,allCells));
axisPeakVMVarianceVals = squeeze(mEMMain(1:2,3,allCells));
axisPeakMag = squeeze(mixEMMain(1:2,3,allCells));
[~,maxPeakInd] = max(axisPeakMag,[],1);
[~,minPeakInd] = min(axisPeakMag,[],1);
for i = 1:nNeurons
offsetAngleFromMax(i) = axisPeaks(maxPeakInd(i),i)-axisPeaks(minPeakInd(i),i);
% offsetAngleFromMax2(i) =
% circ_dist(axisPeaks(maxPeakInd(i),i),axisPeaks(minPeakInd(i),i)); % same
end
offsetAngleFromMax = mod(offsetAngleFromMax,2*pi);


for i = 1:nNeurons
bigPeakAngle(i) = axisPeaks(maxPeakInd(i),i);
smallPeakAngle(i) = axisPeaks(minPeakInd(i),i);
end
forPlottingMaxes = repmat(bigPeakAngle,2,1);
forPlottingMins = repmat(smallPeakAngle,2,1);
forPlottingMaxes = forPlottingMaxes(:);
forPlottingMins = forPlottingMins(:);

% Visualization of the distribution
% figure(1);
% figure(2);
% close(1);
% close(2);
% load('figureColormap.mat');
% mapToUse = hotMapClipped;
% for i = 1:nNeurons
% if strcmp(input('','s'),'q')
%     break
% else
%     figure(1);
%     normRate = nansum(SubPaperNeuronStruct.hdRMapsRunning(:,axisCells(i),iTimeSegment,1));
%     polar([pi/18:pi/18:2*pi]',squeeze(modelHDVals(:,3,axisCells(i)))*normRate);
%     hold on;
%     rosePolarStyle([pi/18:pi/18:2*pi]',SubPaperNeuronStruct.hdRMapsRunning(:,axisCells(i),iTimeSegment,1));
%     hold off;
%     figure(99)
%     caxisMax = 2*SubPaperNeuronStruct.overallMeanFR(axisCells(i),2);
%     sc(fullTrackIsRunning2DRMapsNanConv(:,:,axisCells(i)),[0,caxisMax],mapToUse,'w',...
%         isnan(fullTrackIsRunning2DRMapsNanConv(:,:,axisCells(i))));
%     figure(2)
%     subplot(221)
%     polar(forPlottingMaxes(2*i-1:2*i),[0;1],'r');
%     hold on;
%     polar(forPlottingMins(2*i-1:2*i),[0;1],'k');
%     subplot(223)
%     polar(forPlottingMins(2*i-1:2*i),[0;1],'k');
%     title('Mins');
%     subplot(224)
%     polar(forPlottingMaxes(2*i-1:2*i),[0;1],'r');
%     title('Maxes')
%     subplot(222)
%     polar(forPlottingOffsetFromMax(2*i-1:2*i),[0;1],'r');
%     hold on;
%     polar([0,0],[0,1],'k')
% end
% end

figure(7);
hold off;
forPlottingOffsetFromMax = repmat(offsetAngleFromMax,2,1);
forPlottingOffsetFromMax = forPlottingOffsetFromMax(:);
polar(forPlottingOffsetFromMax,repmat([0;1],nNeurons,1))
hold on;
polar([0,0],[0,1],'k')

figure(8);
close(8);
figure(8);
subplot(131)
polar(forPlottingMaxes,repmat([0;1],nNeurons,1),'r')
hold on;
polar(forPlottingMins,repmat([0;1],nNeurons,1))
subplot(132)
polar(forPlottingMins,repmat([0;1],nNeurons,1))
title('Mins');
subplot(133)
polar(forPlottingMaxes,repmat([0;1],nNeurons,1),'r')
title('Maxes')
% ADD Thick line for mean on broom plot, put a arc in same color to show std dev.


% CODE that I ran at the command prompt for extra plotting and saving
% variables.

% RUN THIS SECTION WITH PLATFORM MODEL
% platformOffsetAnglesFromMax = offsetAngleFromMax;
% platformBigPeakAngle = bigPeakAngle;
% platformSmallPeakAngle = smallPeakAngle;

% RUN THIS SECTION WITH TRACK MODEL
% trackOffsetAnglesFromMax = offsetAngleFromMax;
% trackBigPeakAngle = bigPeakAngle;
% trackSmallPeakAngle = smallPeakAngle;
% axisCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;

% EXTRA STUFF - DO FOR ROTATIONS TOO - HAVE TO ALTER A BIT.
% bigAngleDifference = circ_dist(trackBigPeakAngle,platformBigPeakAngle);
% toPlot = repmat(bigAngleDifference,2,1);
% toPlot = toPlot(:);
% figure;
% polar(toPlot,repmat([0;1],542,1))
% toPlot = repmat(bigAngleDifference(axisCells),2,1);
% smallAngleDifference = circ_dist(trackSmallPeakAngle,platformSmallPeakAngle);
% bigToEachAngleDifference = ([[circ_dist(trackBigPeakAngle,platformBigPeakAngle)];[circ_dist(trackBigPeakAngle,platformSmallPeakAngle)]]);
% [~,indForCloseAngle] = min(abs(bigToEachAngleDifference),[],1);
% for i = 1:542
% bigToCloseAngleDifference(i) = bigToEachAngleDifference(indForCloseAngle(i),i);
% end
% toPlot = repmat(bigToCloseAngleDifference(axisCells),2,1);
% toPlot = toPlot(:);
% polar(toPlot,repmat([0;1],47,1))
% toPlot = repmat(bigToCloseAngleDifference,2,1);
% toPlot = toPlot(:);
% figure;polar(toPlot,repmat([0;1],542,1))



%% Rotation Broom Plots
rotationAxisCells = union(SubPaperRotationsTrackRotatedRunningCategorizedAxisCells.finalAxisLIPReliableRatio,...
    SubPaperRotationsTrackNormalRunningCategorizedAxisCells.finalAxisLIPReliableRatio);
rotationAxisCells = [1:170];
nNeurons = length(rotationAxisCells);
load('mixVMMRotationsTrackNormalRunning.mat')
axisPeaksNormal = squeeze(thetaEMMain(1:2,3,rotationAxisCells));
axisPeakDispersionNormal = squeeze(mEMMain(1:2,3,rotationAxisCells));
axisPeakMagNormal = squeeze(mixEMMain(1:2,3,rotationAxisCells));
[~,maxPeakIndNormal] = max(axisPeakMagNormal,[],1);
[~,minPeakIndNormal] = min(axisPeakMagNormal,[],1);
for i = 1:nNeurons
offsetAngleFromMaxNormal(i) = axisPeaksNormal(maxPeakIndNormal(i),i)-axisPeaksNormal(minPeakIndNormal(i),i);
end
offsetAngleFromMaxNormal = mod(offsetAngleFromMaxNormal,2*pi);
modelHDValsNormal = modelHDVals(:,3,rotationAxisCells);

load('mixVMMRotationsTrackRotatedRunning.mat')
axisPeaksRotated = squeeze(thetaEMMain(1:2,3,rotationAxisCells));
axisPeakDispersionRotated = squeeze(mEMMain(1:2,3,rotationAxisCells));
axisPeakMagRotated = squeeze(mixEMMain(1:2,3,rotationAxisCells));
[~,maxPeakIndRotated] = max(axisPeakMagRotated,[],1);
[~,minPeakIndRotated] = min(axisPeakMagRotated,[],1);
for i = 1:nNeurons
offsetAngleFromMaxRotated(i) = axisPeaksRotated(maxPeakIndRotated(i),i)-axisPeaksRotated(minPeakIndRotated(i),i);
end
offsetAngleFromMaxRotated = mod(offsetAngleFromMaxRotated,2*pi);
modelHDValsRotated = modelHDVals(:,3,rotationAxisCells);

clear forPlotting* big* small*

for i = 1:nNeurons
bigPeakAngleNormal(i) = axisPeaksNormal(maxPeakIndNormal(i),i);
smallPeakAngleNormal(i) = axisPeaksNormal(minPeakIndNormal(i),i);
bigPeakAngleRotated(i) = axisPeaksRotated(maxPeakIndRotated(i),i);
smallPeakAngleRotated(i) = axisPeaksRotated(minPeakIndRotated(i),i);
end

forPlottingBigPeakAngleNormal = repmat(bigPeakAngleNormal,2,1);
forPlottingSmallPeakAngleNormal = repmat(smallPeakAngleNormal,2,1);
forPlottingBigPeakAngleNormal = forPlottingBigPeakAngleNormal(:);
forPlottingSmallPeakAngleNormal = forPlottingSmallPeakAngleNormal(:);

forPlottingBigPeakAngleRotated = repmat(bigPeakAngleRotated,2,1);
forPlottingSmallPeakAngleRotated = repmat(smallPeakAngleRotated,2,1);
forPlottingBigPeakAngleRotated = forPlottingBigPeakAngleRotated(:);
forPlottingSmallPeakAngleRotated = forPlottingSmallPeakAngleRotated(:);

forPlottingOffsetFromMaxPeakNormal = repmat(offsetAngleFromMaxNormal,2,1);
forPlottingOffsetFromMaxPeakNormal = forPlottingOffsetFromMaxPeakNormal(:);

forPlottingOffsetFromMaxPeakRotated = repmat(offsetAngleFromMaxRotated,2,1);
forPlottingOffsetFromMaxPeakRotated = forPlottingOffsetFromMaxPeakRotated(:);


figure(3);
hold off;
polar(forPlottingOffsetFromMaxPeakNormal,repmat([0;1],nNeurons,1))
hold on;
polar([0,0],[0,1],'k')

figure(4);
close(4);
figure(4);
subplot(131)
polar(forPlottingBigPeakAngleNormal,repmat([0;1],nNeurons,1),'r')
hold on;
polar(forPlottingSmallPeakAngleNormal,repmat([0;1],nNeurons,1))
subplot(132)
polar(forPlottingBigPeakAngleNormal,repmat([0;1],nNeurons,1))
title('Mins');
subplot(133)
polar(forPlottingSmallPeakAngleNormal,repmat([0;1],nNeurons,1),'r')
title('Maxes')


figure(5);
hold off;
polar(forPlottingOffsetFromMaxPeakRotated,repmat([0;1],nNeurons,1))
hold on;
polar([0,0],[0,1],'k')

figure(6);
close(6);
figure(6);
subplot(131)
polar(forPlottingBigPeakAngleRotated,repmat([0;1],nNeurons,1),'r')
hold on;
polar(forPlottingSmallPeakAngleRotated,repmat([0;1],nNeurons,1))
subplot(132)
polar(forPlottingBigPeakAngleRotated,repmat([0;1],nNeurons,1))
title('Mins');
subplot(133)
polar(forPlottingSmallPeakAngleRotated,repmat([0;1],nNeurons,1),'r')
title('Maxes')


% figure;
% subplot(221)
% polar(bigPeakAngleNormal,repmat([0;1],length(rotationAxisCells),1))
% hold on;
% polar(smallPeakAngleNormal,repmat([0;1],length(rotationAxisCells),1),'r')
% 
% subplot(222)
% polar(bigPeakAngleRotated,repmat([0;1],length(rotationAxisCells),1))
% hold on;
% polar(smallPeakAngleRotated,repmat([0;1],length(rotationAxisCells),1),'r')
% 
% subplot(223)
% polar(forPlottingOffsetFromMaxPeakNormal,repmat([0;1],length(rotationAxisCells),1))
% hold on;
% polar([0,0],[0,1],'k')
% 
% subplot(224)
% polar(forPlottingOffsetFromMaxPeakRotated,repmat([0;1],length(rotationAxisCells),1))
% hold on;
% polar([0,0],[0,1],'k')
% 
% for i = 1:nNeurons
% offsetMaxRotatedFromNormal(i) = axisPeaksNormal(maxPeakIndNormal(i),i)-axisPeaksRotated(maxPeakIndRotated(i),i);
% offsetMinRotatedFromNormal(i) = axisPeaksNormal(maxPeakIndNormal(i),i)-axisPeaksRotated(minPeakIndRotated(i),i);
% end
% 
% forPlottingOffsetMaxRotatedFromNormal = repmat(offsetMaxRotatedFromNormal,2,1);
% forPlottingOffsetMaxRotatedFromNormal = forPlottingOffsetMaxRotatedFromNormal(:);
% 
% forPlottingOffsetMinRotatedFromNormal = repmat(offsetMinRotatedFromNormal,2,1);
% forPlottingOffsetMinRotatedFromNormal = forPlottingOffsetMinRotatedFromNormal(:);
% 
% figure;
% polar(forPlottingOffsetMaxRotatedFromNormal,repmat([0;1],length(rotationAxisCells),1))
% hold on;
% polar(forPlottingOffsetMinRotatedFromNormal,repmat([0;1],length(rotationAxisCells),1),'r')
% polar([0,0],[0,1],'k')



% For comparing different model runs:
% clear offsetM*
% j = 0;
% for i = 1:nNeurons
% if ~lowFire(i)
% j = j+1;
% offsetMaxRotatedFromNormal(j) = axisPeaksNormal(maxPeakIndNormal(i),i)-axisPeaksRotated(maxPeakIndRotated(i),i);
% offsetMinRotatedFromNormal(j) = axisPeaksNormal(minPeakIndNormal(i),i)-axisPeaksRotated(minPeakIndRotated(i),i);
% end
% end
% forPlottingOffsetMaxRotatedFromNormal = repmat(offsetMaxRotatedFromNormal,2,1);
% forPlottingOffsetMaxRotatedFromNormal = forPlottingOffsetMaxRotatedFromNormal(:);
% 
% forPlottingOffsetMinRotatedFromNormal = repmat(offsetMinRotatedFromNormal,2,1);
% forPlottingOffsetMinRotatedFromNormal = forPlottingOffsetMinRotatedFromNormal(:);
% 
% figure;
% polar(forPlottingOffsetMaxRotatedFromNormal,repmat([0;1],j,1))
% hold on;
% polar([0,0],[0,1],'k')
% figure;
% polar(forPlottingOffsetMinRotatedFromNormal,repmat([0;1],j,1),'r')
% hold on;
% polar([0,0],[0,1],'k')
% 




