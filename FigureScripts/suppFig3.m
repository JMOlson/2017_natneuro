%axis cell paper figure plots

%% Supp Fig 3 - style of supp fig 6 and 7 - downsampled smoothed maps used
% load('D:\AxisCellPaper\FinalAnalysisResults\MainDataset\SubPaperDataStruct.mat')
% load('D:\AxisCellPaper\FinalAnalysisResults\MainDataset\SubPaperMainTrackRunningCategorizedAxisCells.mat')
% load('D:\AxisCellPaper\FinalAnalysisResults\MainDataset\SubPaperNeuronStruct.mat')
% axisCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;
% load('twoDPlatformAllDataWithOcc.mat')
%  load('twoDPlatformSmoothed.mat')
% [SubPaperDataStruct, SubPaperNeuronStruct] = platform2DSpatialMeasures(SubPaperDataStruct, SubPaperNeuronStruct, twoDPlatformAllData);
spatCoh = SubPaperNeuronStruct.platformSpatialCoherence;
spatInfo = SubPaperNeuronStruct.platformSpatialInfoInBits;
spatInfoPerSpike = SubPaperNeuronStruct.platformSpatialInfoPerSpikeInBits;
spatSpars = SubPaperNeuronStruct.platformSpatialSparsity;
spikeCountPlatform = squeeze(sum(sum(twoDPlatformAllData(:,:,:,2))));
platformMeanFR = spikeCountPlatform./squeeze(sum(sum(twoDPlatformAllData(:,:,:,3)))).*SubPaperDataStruct.sampleRate(1);
enoughSpikes = spikeCountPlatform>250;
% mean(spatCoh(enoughSpikes))
% std(spatCoh(enoughSpikes))
[sortedMat,sortInfoInd] = sortrows([spatInfo',enoughSpikes],1);
sortInfoIndFinal = sortInfoInd(sortedMat(:,2)==1,1);

load('figureColormap.mat');
mapToUse = hotMapClipped;



%% Histogram - part B
figure(1)
histogram(spatInfo(enoughSpikes),0.5:.5:10)

%% Part C - examples
% selected = [255,467,423,11];
% selected = axisCells;
selected = sortInfoIndFinal(end:-1:1); % make it descend
selectedA = selected(17:35:354);
selectedB = selected(18:35:354);

neuronsToPlot = selectedA;
for i = 1:length(neuronsToPlot)
cMax = max(max(twoDPlatformSmoothed(6:50,6:50,neuronsToPlot(i))));
    
    figure(4)
    sc(twoDPlatformSmoothed(6:50,6:50,neuronsToPlot(i)),[0,cMax],mapToUse,'w',...
        isnan(twoDPlatformAllData(:,:,neuronsToPlot(i),1)));
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    fprintf(['Neuron %d\nArena Max Firing Rate %4.0f Hz\nSpatial Info: %4.1f bits\n'...
        'Info/Spike: %4.1f bits\nCoherence: %1.2f\n'],...
        neuronsToPlot(i),cMax,spatInfo(neuronsToPlot(i)),...
        spatInfoPerSpike(neuronsToPlot(i)),spatCoh(neuronsToPlot(i)))

cMax = max(max(twoDPlatformSmoothed(6:50,6:50,selectedB(i))));
    figure(5)
    sc(twoDPlatformSmoothed(6:50,6:50,selectedB(i)),[0,cMax],mapToUse,'w',...
        isnan(twoDPlatformAllData(:,:,selectedB(i),1)));
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    fprintf(['Neuron %d\nArena Max Firing Rate %4.0f Hz\nSpatial Info: %4.1f bits\n'...
        'Info/Spike: %4.1f bits\nCoherence: %1.2f\n\n'],...
        selectedB(i),cMax,spatInfo(selectedB(i)),...
        spatInfoPerSpike(selectedB(i)),spatCoh(selectedB(i)))
    pause;
end

