function [ ExampleHDBySpaceOccMap ] = directionBySpaceOccupancyExample( trackIsRunning2DByHDRMap)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% Calculating average direction and magnitude of direction 
[xRange,yRange,nDirections,~] = size(trackIsRunning2DByHDRMap);
noOcc = trackIsRunning2DByHDRMap(:,:,:,3)==0;
noOccXY = all(noOcc,3);
fixedMap = trackIsRunning2DByHDRMap(:,:,:,3);
magnitude = nan(xRange,yRange);
direction = nan(xRange,yRange);
for i = 1:xRange
for j = 1:yRange
    if ~noOccXY(i,j)
magnitude(i,j) = circ_r([2*pi/nDirections:2*pi/nDirections:2*pi]',squeeze(fixedMap(i,j,:)),2*pi/nDirections);
direction(i,j) = circ_mean([2*pi/nDirections:2*pi/nDirections:2*pi]',squeeze(fixedMap(i,j,:)));
    end
end
end

ExampleHDBySpaceOccMap.noOcc = noOcc;
ExampleHDBySpaceOccMap.noOccXY = noOccXY;
ExampleHDBySpaceOccMap.fixedMap = fixedMap;
ExampleHDBySpaceOccMap.magnitude = magnitude;
ExampleHDBySpaceOccMap.direction = direction;
end

% Lines of code from doing this.

% tmp = [[0:1/254:1]';ones(255*2,1);[1:-1/254:0]';zeros(255*2,1);];
% circleColorMap = [tmp,circshift(tmp,510),circshift(tmp,1020)];

% load('exampleOccHDby2DMap.mat')
% load 'circleColorMap'
% figure(1);
% sc(ExampleOccDirection2DMap.direction,[-pi,pi],circleColorMap*0.5,'w',ExampleOccDirection2DMap.noOccXY);
% figure(2);
% colormap(circleColorMap*0.5)
% scatter(cos(0:pi/200:2*pi),sin(0:pi/200:2*pi),[],0:.0025:1,'filled')
% map180 = ([[[0:1:204]';204*ones(205,1);[204:-1:0]';zeros(205,1)],zeros(820,1),[204*ones(205,1);[204:-1:0]';zeros(205,1);[0:1:204]']])/255;
% map180 = [map180;map180];
% figure(3);
% sc(ExampleOccDirection2DMap.direction,[-pi,pi],map180,'w',ExampleOccDirection2DMap.noOccXY);
% figure(4);
% colormap(map180);
% scatter(cos(0:pi/200:2*pi),sin(0:pi/200:2*pi),[],0:.0025:1,'filled')


% load('exampleOccHDby2DMap.mat')
% load 'circleColorMap'
% figure(61);
% sc(mod(ExampleOccDirection2DMap.direction,2*pi),[0,2*pi],circleColorMap*0.70,'w',ExampleOccDirection2DMap.noOccXY);
% figure(62);
% colormap(circleColorMap*0.70)
% scatter(cos(0:pi/200:2*pi),sin(0:pi/200:2*pi),[],0:.0025:1,'filled')
% xlim([-1.5,1.5]);
% ylim([-1.5,1.5]);
% map180 = ([[[64:1:255]';255*ones(192,1);[255:-1:64]';64*ones(192,1)],zeros(768,1),[255*ones(192,1);[255:-1:64]';64*ones(192,1);[64:1:255]']])/255;
% map180 = [map180;map180];
% figure(63);
% sc(mod(ExampleOccDirection2DMap.direction,2*pi),[0,2*pi],map180,'w',ExampleOccDirection2DMap.noOccXY);
% figure(64);
% colormap(map180);
% scatter(cos(0:pi/200:2*pi),sin(0:pi/200:2*pi),[],0:.0025:1,'filled')
% xlim([-1.5,1.5]);
% ylim([-1.5,1.5]);


