%%axis cell paper figure plots

caxisMax = 35;
iOverallNeuron = 255;
map = 'parula';
%% 2DRMap Styles for Figures 1 and 2
% A little prep:

[platformIsRunning2DRMapsConv2Nan, platformIsRunning2DRMapsNanConv] = filter2DDownSampledMatrices(platformIsRunning2DRMaps);
[trackIsRunning2DRMapsConv2Nan, trackIsRunning2DRMapsNanConv] = filter2DDownSampledMatrices(trackIsRunning2DRMaps);

[SubPaperDataStruct, fullTrackIsRunning2DRMaps, ~] = twoDRateMapper(SubPaperDataStruct, SubPaperNeuronStruct,2,1,1,1,1);
[SubPaperDataStruct, fullTrack2DRMaps] = twoDRateMapper(SubPaperDataStruct, SubPaperNeuronStruct,2,1,1,1,0);
[SubPaperDataStruct, fullPlatform2DRMaps] = twoDRateMapper(SubPaperDataStruct, SubPaperNeuronStruct,1,1,1,1,0);
[SubPaperDataStruct, fullPlatformIsRunning2DRMaps, ~] = twoDRateMapper(SubPaperDataStruct, SubPaperNeuronStruct,1,1,1,1,1);
[FastDS, fullPlatformFastRunning2DRMaps,~] = twoDRateMapper(FastDS, FastNS,1,1,1,1,1);
[FastDS, fullTrackFastRunning2DRMaps,~] = twoDRateMapper(FastDS, FastNS,2,1,1,1,1);
[FastDS, platformFastRunning2DRMaps,~] = twoDRateMapper(FastDS, FastNS,1,6+2/3,1,1,1);
[FastDS, trackFastRunning2DRMaps,~] = twoDRateMapper(FastDS, FastNS,2,6,1,1,1);
[fullPlatformFastRunning2DRMapsConv2Nan, fullPlatformFastRunning2DRMapsNanConv] = filter2DMatrices(fullPlatformFastRunning2DRMaps,0);
[fullTrackFastRunning2DRMapsConv2Nan, fullTrackFastRunning2DRMapsNanConv] = filter2DMatrices(fullTrackFastRunning2DRMaps,0);
[fullPlatformIsRunning2DRMapsConv2Nan, fullPlatformIsRunning2DRMapsNanConv] = filter2DMatrices(fullPlatformIsRunning2DRMaps,0);
[fullTrackIsRunning2DRMapsConv2Nan, fullTrackIsRunning2DRMapsNanConv] = filter2DMatrices(fullTrackIsRunning2DRMaps,0);
[platformFastRunning2DRMapsConv2Nan, platformFastRunning2DRMapsNanConv] = filter2DMatrices(platformFastRunning2DRMaps,1);
[trackFastRunning2DRMapsConv2Nan, trackFastRunning2DRMapsNanConv] = filter2DMatrices(trackFastRunning2DRMaps,1);
[platformIsRunning2DRMapsConv2Nan, platformIsRunning2DRMapsNanConv] = filter2DMatrices(platformIsRunning2DRMaps,1);
[trackIsRunning2DRMapsConv2Nan, trackIsRunning2DRMapsNanConv] = filter2DMatrices(trackIsRunning2DRMaps,1);

%% The examples
% Style 1 - Unsmoothed downsampled running
% figure(50)
% sc(platformIsRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(platformIsRunning2DRMaps(:,:,iOverallNeuron,1)));
% figure(51)
% sc(trackIsRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(trackIsRunning2DRMaps(:,:,iOverallNeuron,1)));
% 
% % Style 2 - Unsmoothed downsampled high velocity running
% figure(52)
% sc(platformFastRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(platformFastRunning2DRMaps(:,:,iOverallNeuron,1)));
% figure(53)
% sc(trackFastRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(trackFastRunning2DRMaps(:,:,iOverallNeuron,1)));

% Style 3 - Unsmoothed downsampled clean runs only




% Style 4 - nanconv smoothed downsampled running
figure(56)
sc(platformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(platformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));
figure(57)
sc(trackIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(trackIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));

% Style 5 - nanconv smoothed downsampled high velocity running
figure(58)
sc(platformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(platformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));
figure(59)
sc(trackIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(trackFastRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));

% Style 6 - convnan smoothed downsampled clean runs only


% % Style 7 - conv2nan smoothed downsampled running
% figure(62)
% sc(platformIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(platformIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));
% figure(63)
% sc(trackIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(trackIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));
% 
% % Style 8 - conv2nan smoothed downsampled high velocity running
% figure(64)
% sc(platformFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(platformFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));
% figure(65)
% sc(trackFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(trackFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));

% Style 9 - conv2nan smoothed downsampled clean runs only



% Style 10 - Unsmoothed running
% figure(68)
% sc(fullPlatformIsRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullPlatformIsRunning2DRMaps(:,:,iOverallNeuron,1)));
% figure(69)
% sc(fullTrackIsRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullTrackIsRunning2DRMaps(:,:,iOverallNeuron,1)));
% 
% % Style 11 - Unsmoothed high velocity running
% figure(70)
% sc(fullPlatformFastRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullPlatformFastRunning2DRMaps(:,:,iOverallNeuron,1)));
% figure(71)
% sc(fullTrackFastRunning2DRMaps(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullTrackFastRunning2DRMaps(:,:,iOverallNeuron,1)));

% Style 12 - Unsmoothed clean runs only




% Style 13 - convnan smoothed running WINNNER WINNER WINNER WINNER
figure(74)
sc(fullPlatformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));
figure(75)
sc(fullTrackIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
    isnan(fullTrackIsRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));

% % Style 14 - convnan smoothed high velocity running
% figure(76)
% sc(fullPlatformFastRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullPlatformFastRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));
% figure(77)
% sc(fullTrackFastRunning2DRMapsNanConv(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullTrackFastRunning2DRMapsNanConv(:,:,iOverallNeuron,1)));

% Style 15 - convnan smoothed clean runs only


% Style 16 - conv2nan smoothed running
% figure(80)
% sc(fullPlatformIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullPlatformIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));
% figure(81)
% sc(fullTrackIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullTrackIsRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));

% Style 17 - conv2nan smoothed high velocity running
% figure(82)
% sc(fullPlatformFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullPlatformFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));
% figure(83)
% sc(fullTrackFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1),[0,caxisMax],colormap(map),'w',...
%     isnan(fullTrackFastRunning2DRMapsConv2Nan(:,:,iOverallNeuron,1)));

% Style 18 - conv2nan smoothed clean runs only



%% HDRMap Styles for Figures 1 and 2


