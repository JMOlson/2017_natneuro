for i = 1:47
    maxHDRate = max(max(squeeze(NS.hdRMaps(:,neuronsToPlot(i),:,1))));
    figure(1);
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMaps(:,neuronsToPlot(i),1,1),'r');
    maxHDRate = max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),2,1)));
    title('platform - red vs track - black')
    hold off;
    
    figure(2)
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMaps(:,neuronsToPlot(i),2,1),'k');
    maxHDRate = max(maxHDRate,max(squeeze(NS.hdRMapsStill(:,neuronsToPlot(i),2,1))));
    title('platform - red vs track - black')
    hold off;
    
    figure(3);
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),2,1),'r');
    title('run - red vs still - black')
    hold off;
    
    figure(4);
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsStill(:,neuronsToPlot(i),2,1),'k');
    title('run - red vs still - black')
    hold off;
    
         caxisMax = 2*overallFR(neuronsToPlot(i),2);
     figure(5);
    sc(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
         figure(6);
    sc(fullTrackStill2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackStill2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    pause;
end

%%
selected = axisCells;

% selected = [296,38]
neuronsToPlot = selected;
overallFR = NS.overallMeanFR;
for i = 1:47
     caxisMax = 2*overallFR(neuronsToPlot(i),2);
     figure(3);
    sc(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
         figure(4);
    sc(fullTrackStill2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackStill2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     for j = 1:36
%     figure(1)
%         sc(trackIsRunning2DByHDRMaps(:,:,j,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(trackIsRunning2DByHDRMaps(:,:,j,neuronsToPlot(i),1)));
%     figure(2)
%         sc(trackStill2DByHDRMaps(:,:,j,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(trackStill2DByHDRMaps(:,:,j,neuronsToPlot(i),1)));
%     pause;
%     end
pause;
end
