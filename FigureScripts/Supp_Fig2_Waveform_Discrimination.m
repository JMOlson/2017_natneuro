%% Supplemental figure 2 - waveform discrimination. 
neuronsToPlot = axisCells([3,4,5,6,7]);
for i= 1:5
NS = SubPaperNeuronStruct;
DS = SubPaperDataStruct;
overallFR = SubPaperNeuronStruct.overallMeanFR;
load('figureColormap.mat');
mapToUse = hotMapClipped;
maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
caxisMax = 2*overallFR(neuronsToPlot(i),2);

figure;
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
set(gca, 'ColorOrder',[204,0,0]/255);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
hold off;
pause;
end

%% 2nd part.
neuronsToPlot = axisCells(24);
overallFR = SubPaperNeuronStruct.overallMeanFR;
mapToUse = hotMapClipped;
maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
caxisMax = 2*overallFR(neuronsToPlot(i),2);

figure;
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
set(gca, 'ColorOrder',[204,0,0]/255);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
hold off;



%% 3rd part.
neuronsToPlot = 422:424;
for i= 1:3
overallFR = SubPaperNeuronStruct.overallMeanFR;
mapToUse = hotMapClipped;
maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
caxisMax = 2*overallFR(neuronsToPlot(i),2);

figure(i);
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
set(gca, 'ColorOrder',[204,0,0]/255);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
hold off;
end



