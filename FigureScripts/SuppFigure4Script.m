% Supplemental Figure 4
neuronToUse = 28;
% Part 1
maxRate = max(max(squeeze(NS.hdRMapsRunningHalves(:,neuronToUse,2,1,1))));
 normConst = sum(NS.hdRMapsRunningHalves(:,neuronToUse,2,1,1));
maxRateAll = max(maxRate/normConst,max(max(modelHDVals(:,:,neuronToUse))));
for i = 1:9 
figure;
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxRateAll*ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0;0,0,204]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunningHalves(:,neuronToUse,2,1,1)./normConst);
    hold on;
    polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        modelHDVals(:,i,neuronToUse));
end

%% Part 2
maxRate = max(max(squeeze(NS.hdRMapsRunningHalves(:,neuronToUse,2,2,1))));
 normConst = sum(NS.hdRMapsRunningHalves(:,neuronToUse,2,2,1));
maxRateAll = max(maxRate/normConst,max(max(modelHDVals(:,:,neuronToUse))));
for i = 1:9 
figure;
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxRateAll*ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0;0,0,204]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunningHalves(:,neuronToUse,2,2,1)./normConst);
    hold on;
    polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        modelHDVals(:,i,neuronToUse));
end