% For figure 2
neuronToUse = 255;
figure;
    maxRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronToUse,2,1))));
 normConst = sum(NS.hdRMapsRunning(:,neuronToUse,2,1));
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxRate/normConst*ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0;0,0,204]/255);
    
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronToUse,2,1)./normConst);
    
    hold on;
    polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        modelHDVals(:,3,neuronToUse));
    
    % Mean axis cell and model figures
    figure;
for i = 1:47
maxHDRate(i) = max(max(squeeze(NS.hdRMapsRunning(:,axisCells(i),:,1))));
end
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedHDVals(:,axisCells(:))./repmat(maxHDRate,36,1)));
mean(alignedHDVals(:,axisCells(:))./repmat(maxHDRate,36,1));
mean(alignedHDVals(:,axisCells(:))./repmat(maxHDRate,36,1),2);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedHDVals(:,axisCells(:))./repmat(maxHDRate,36,1)),2);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedHDVals(:,axisCells(:))./repmat(maxHDRate,36,1),2));
figure;
for i = 1:542
maxHDRateAll(i) = max(max(squeeze(NS.hdRMapsRunning(:,i,:,1))));
end
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedHDVals(:,:)./repmat(maxHDRateAll,36,1),2));
figure;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedModelVals(:,:)./repmat(maxHDRateAll,36,1),2));
figure;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
mean(alignedModelVals(:,axisCells(:))./repmat(maxHDRate,36,1),2));
figure(4);
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
nanmean(alignedModelVals(:,:)./repmat(maxHDRateAll,36,1),2));
figure;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
modelHDVals(:,3,neuronToUse));
    
    
% Stats about our fig 2 example neuron.
trackOffsetAnglesFromMax(255)
trackPeakAngles(255)
trackRatioBetweenPeaks(255)
trackRatioPeaksToMinsMaxNormed
trackRatioPeaksToMinsMaxNormed(255)
trackHDPeaksSpatialReliability(:,255)
trackBigPeakAngle(255)
mEMMain(1:2,3,255)
circVar255 = 1 - besseli(1,ans)./besseli(0,ans);
mixEMMain(1:2,3,255)


% Making figure 2 bar graphs - use previoiusly compiled stats table too.
figure;
bar([47,1])
[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainPlatformRunning', 1, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
dbquit
[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainTrackRunning', 2, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
bar([110,14])
load('D:\subPaper\FinalAnalysisResults\MainDataset\axisCellStatData\trackModelStats.mat')
mean(trackRatioPeaksToMinsMaxNormed(axisCells))
mean(1/(1-trackRatioPeaksToMinsMaxNormed(axisCells)))
mean(1./(1-trackRatioPeaksToMinsMaxNormed(axisCells)))
1/(1-.74516)
std(1./(1-trackRatioPeaksToMinsMaxNormed(axisCells)))
1./(1-trackRatioPeaksToMinsMaxNormed(axisCells))
hist(ans)
trackRatioPeaksToMinsMaxNormed(axisCells(43:end))
std(1./(1-trackRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))))
mean(1./(1-trackRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))))
mean(1./(1-platformRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))))
std(1./(1-platformRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))))
ranksum(1./(1-platformRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))),1./(1-trackRatioPeaksToMinsMaxNormed(axisCells([1:43,45:end]))))
errorbar([4.6521,1.9491],[2.2322,1.2937],'.')
hold on;
bar([1,2],[4.6521,1.9491])
errorbar([4.6521,1.9491],[2.2322,1.2937],'.')
bar([.7389,.6472]); hold on; errorbar([.7389, .6472],[.1246, .2815],'.')
title(p = 0.0729)


load('mixVMMMainPlatformRunning.mat')
platformAxisCircVar = 1 - besseli(1,mEMMain(1:2,3,axisCells))./besseli(0,mEMMain(1:2,3,axisCells));
load('mixVMMMainTrackRunning.mat')
trackAxisCircVar = 1 - besseli(1,mEMMain(1:2,3,axisCells))./besseli(0,mEMMain(1:2,3,axisCells));
[a,b,c] = ranksum(platformAxisCircVar(:),trackAxisCircVar(:))

load('platformModelStats.mat', 'platformHDPeaksSpatialReliability')
load('trackModelStats.mat', 'trackHDPeaksSpatialReliability')
platformAxisSpatialIndependence = platformHDPeaksSpatialReliability(:,axisCells);
trackAxisSpatialIndependence = trackHDPeaksSpatialReliability(:,axisCells);
[a,b,c] = ranksum(platformAxisSpatialIndependence(:),trackAxisSpatialIndependence(:))

load('SubPaperRotationsDataStruct.mat')
load('SubPaperRotationsNeuronStruct.mat')
size(SubPaperRotationsNeuronStruct.hdRMapsRunning)
axisCellNormal = SubPaperRotationsNeuronStruct.hdRMapsRunning(:,rotationAxisCells,2,1);
axisCellRotation = SubPaperRotationsNeuronStruct.hdRMapsRunning(:,rotationAxisCells,3,1);
axisCellRotationShifted = circshift(axisCellRotation,9,1);
help corr
corr(axisCellNormal,axisCellTrack);
normalCorrs = corr(axisCellNormal,axisCellRotation);
rotatedCorrs = corr(axisCellNormal,axisCellRotationShifted);
rotatedCorrs = diag(corr(axisCellNormal,axisCellRotationShifted));
normalCorrs = diag(corr(axisCellNormal,axisCellRotation));
[a,b,c] = ranksum(normalCorrs,rotatedCorrs)
