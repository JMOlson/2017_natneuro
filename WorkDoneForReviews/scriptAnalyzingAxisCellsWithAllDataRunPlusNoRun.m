% NS = SubPaperNeuronStruct;
% DS = SubPaperDataStruct;
overallFR = NS.overallMeanFR;
load('figureColormap.mat');
mapToUse = hotMapClipped;

selected = [255,467,423,11];
selected = axisCells;
% selected = SubPaperMainTrackCategorizedAxisCells.finalAxisMissed;
selected = SubPaperMainTrackCategorizedAxisCells.finalAxisWrong;
% selected = switchers;

neuronsToPlot = selected;
for i = 1:length(neuronsToPlot)
    maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
    caxisMax = 2*overallFR(neuronsToPlot(i),2);
    
    figure(1)
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));

%     scaleModelVals = maxHDRate/max(modelHDVals(:,3,neuronsToPlot(i)));
%     polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
%         modelHDVals(:,3,neuronsToPlot(i))*scaleModelVals,'k');
    hold off;
    
    figure(2)
    sc(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    
    figure(3)
    % Code to control axis size.
    t = 0 : .01 : 2 * pi;
    P = polar(t, maxHDRate * ones(size(t)));
    set(P, 'Visible', 'off')
    hold on;
    set(gca, 'ColorOrder',[204,0,0]/255);
    rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        NS.hdRMaps(:,neuronsToPlot(i),2,1));
    hold off;
    
    figure(4)
    sc(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    
    figure(5)
    sc(diffMaps(:,:,i,1),mapToUse,'w',...
        isnan(fullTrack2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
    caxisMax
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    disp(neuronsToPlot(i));
    pause;
end
