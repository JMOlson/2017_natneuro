%% For Response to Reviews 1.
% [BVCResults,PlaceResults,radii,centers] = bvcCreator(twoDPlatformAllData, 1:542, NS.recCount);
%
% load('twoDPlatformAllDataWithOcc.mat')
% load('twoDPlatformSmoothed.mat')
% load('D:\AxisCellPaper\FinalAnalysisResults\MainDataset\SubPaperMainTrackRunningCategorizedAxisCells.mat')
% axisCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;

%Spike Count On Platform must be at least 250.
spikeCountPlatform = squeeze(sum(sum(twoDPlatformAllData(:,:,:,2))));
enoughSpikes = spikeCountPlatform>250; 

% Ignore neurons with a max Pearson correlation with a template less than r = 0.4
highRValsMask = max(BVCResults.r,PlaceResults.r)>0.4;

% Axis-tuned neurons
isAxis = zeros(542,1);
isAxis(axisCells) = 1;

[~,sortedPlace] = sort(PlaceResults.r,'descend');
[~,sortedBVC] = sort(BVCResults.r,'descend');
[sortedDiff, sortedDiffInd] = sort(BVCResults.r-PlaceResults.r,'descend');

sortedDiffIndEnough = sortedDiffInd(enoughSpikes(sortedDiffInd));
sortedDiffEnough = sortedDiff(enoughSpikes(sortedDiffInd));

sortedDiffIndEnoughHighR = sortedDiffInd(enoughSpikes(sortedDiffInd) & highRValsMask(sortedDiffInd));
sortedDiffEnoughHighR = sortedDiff(enoughSpikes(sortedDiffInd) & highRValsMask(sortedDiffInd));

sortedDiffIndEnoughHighRAxis = sortedDiffInd(enoughSpikes(sortedDiffInd) & highRValsMask(sortedDiffInd) & isAxis(sortedDiffInd));
sortedDiffEnoughHighRAxis = sortedDiff(enoughSpikes(sortedDiffInd) & highRValsMask(sortedDiffInd) & isAxis(sortedDiffInd));


%% Difference histogram between pearson r of BVC and Place
figure;
histogram(sortedDiffEnoughHighR,[-0.5:0.025:0.5]);
hold on
histogram(sortedDiffEnoughHighRAxis,[-0.5:0.025:0.5]);

%% BVC Histogram
figure;
histogram(BVCResults.r(enoughSpikes),[0:0.025:1]);
hold on;
histogram(BVCResults.r(enoughSpikes&isAxis),[0:0.025:1]);
set(gca,'TickLength',[0,0])
title('BVC')


%% Place Histogram
figure;
histogram(PlaceResults.r(enoughSpikes),[0:0.025:1]);
hold on;
histogram(PlaceResults.r(enoughSpikes&isAxis),[0:0.025:1]);
set(gca,'TickLength',[0,0])


%% Exceptional BVCs
sortedVal = sortedDiffIndEnoughHighR;
for i = 1:10
actInd = sortedVal(i);
cMax = max(max(twoDPlatformSmoothed(6:50,6:50,actInd,1)));
figure(4)
sc(twoDPlatformAllData(:,:,actInd,1),parula,[0,20],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(5)
sc(twoDPlatformSmoothed(6:50,6:50,actInd,1),hotMapClipped,[0,cMax],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(6)
a = BVCResults.bestTemplate(:,:,actInd);
sc(a,hotMapClipped,[0,max(a(:))],'w',a<0);
figure(7)
b = PlaceResults.bestTemplate(:,:,actInd);
sc(b,hotMapClipped,[0,max(b(:))],'w',a<0);
disp(['Neuron: ',num2str(actInd)]);
disp(['BVC R: ',num2str(BVCResults.r(actInd))])
disp(['Place R: ',num2str(PlaceResults.r(actInd))])
disp(['MaxFiringRate: ',num2str(cMax)])
pause;
end

%% Exceptional Places
sortedVal = sortedDiffIndEnoughHighR(end:-1:1);
for i = 1:15 % 11 and 13 are from NS16, 15 is from NS14
actInd = sortedVal(i);
cMax = max(max(twoDPlatformSmoothed(6:50,6:50,actInd,1)));
figure(4)
sc(twoDPlatformAllData(:,:,actInd,1),parula,[0,20],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(5)
sc(twoDPlatformSmoothed(6:50,6:50,actInd,1),hotMapClipped,[0,cMax],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(6)
a = BVCResults.bestTemplate(:,:,actInd);
sc(a,hotMapClipped,[0,max(a(:))],'w',a<0);
figure(7)
b = PlaceResults.bestTemplate(:,:,actInd);
sc(b,hotMapClipped,[0,max(b(:))],'w',a<0);
disp(['Neuron: ',num2str(actInd)]);
disp(['BVC R: ',num2str(BVCResults.r(actInd))])
disp(['Place R: ',num2str(PlaceResults.r(actInd))])
disp(['MaxFiringRate: ',num2str(cMax)])
pause;
end

%% The borderline
sortedVal = sortedDiffIndEnoughHighR;
for i = 41:53 %excluding neurons firing w/ means over X
actInd = sortedVal(i);
cMax = max(max(twoDPlatformSmoothed(6:50,6:50,actInd,1)));
meanFire = nanmean(nanmean(twoDPlatformAllData(:,:,actInd,1)));
figure(5)
sc(twoDPlatformSmoothed(6:50,6:50,actInd,1),hotMapClipped,[0,cMax],'w',twoDPlatformAllData(:,:,actInd,3)==0);
disp(['Neuron: ',num2str(actInd)]);
disp(['BVC R: ',num2str(BVCResults.r(actInd))])
disp(['Place R: ',num2str(PlaceResults.r(actInd))])
disp(['MaxFiringRate: ',num2str(cMax)])
disp(['MeanFiringRate: ',num2str(meanFire)])
pause;
end

%%
% sortedVal = sortedBVC(~isnan(BVCResults.r(sortedBVC)));
% sortedVal = sortedPlace(~isnan(PlaceResults.r(sortedPlace)));
% sortedVal = [sortedVal(round(length(sortedVal)./2)),sortedVal(floor(length(sortedVal)./4)),sortedVal(1)];
for i = 1:542
actInd = sortedVal(i);
figure(4)
sc(twoDPlatformAllData(:,:,actInd,1),parula,[0,20],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(5)
sc(twoDPlatformSmoothed(6:50,6:50,actInd,1),parula,[0,20],'w',twoDPlatformAllData(:,:,actInd,3)==0);
figure(6)
a = BVCResults.bestTemplate(:,:,actInd);
sc(a,parula,[0,max(a(:))],'w',a<0);
figure(7)
b = PlaceResults.bestTemplate(:,:,actInd);
sc(b,parula,[0,max(b(:))],'w',a<0);
figure(8)
a = BVCResults.offBestTemplate(:,:,actInd);
sc(a,parula,[0,max(a(:))],'w',a<0);
figure(9);
surf(BVCResults.rsAll(:,:,1,actInd))
hold on;
surf(BVCResults.rsAll(:,:,2,actInd))
surf(BVCResults.rsAll(:,:,3,actInd))
surf(BVCResults.rsAll(:,:,4,actInd))
hold off;
figure(10);
surf(PlaceResults.rsAll(:,:,1,actInd))
hold on;
surf(PlaceResults.rsAll(:,:,2,actInd))
surf(PlaceResults.rsAll(:,:,3,actInd))
surf(PlaceResults.rsAll(:,:,4,actInd))
hold off;
disp(['Neuron ',num2str(actInd)]);
disp(['BVC R - ',num2str(BVCResults.r(actInd))])
disp(['Place R - ',num2str(PlaceResults.r(actInd))])
disp(['BVCoff R - ',num2str(BVCResults.offR(actInd))])
pause;
end



spikeCountPlatform = squeeze(sum(sum(twoDPlatformAllData(:,:,:,2))));
[sortedDiff, sortedDiffInd] = sort(BVCResults.r-PlaceResults.r,'descend');
enoughSpikes = spikeCountPlatform(sortedDiffInd)>250;
sortedDiffIndEnough = sortedDiffInd(enoughSpikes);
sortedDiffEnough = sortedDiff(enoughSpikes);
sortedDiffIndEnoughHighR = sortedDiffIndEnough(...
    max(BVCResults.r(sortedDiffIndEnough),PlaceResults.r(sortedDiffIndEnough))>0.4);
sortedDiffEnoughHighR = sortedDiffEnough(...
    max(BVCResults.r(sortedDiffIndEnough),PlaceResults.r(sortedDiffIndEnough))>0.4);
figure(11);
histogram(sortedDiffEnough);

sortedDiffIndEnoughHighRAxis = sortedDiffIndEnoughHighR(ismember(sortedDiffIndEnoughHighR,axisCells));

