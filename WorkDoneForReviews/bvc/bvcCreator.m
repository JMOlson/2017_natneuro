function [ BVC, Place, radii, centers] =  bvcCreator( twoDRMaps, neuronsToEval, recCount)
%BVCCREATOR Analyzses the fit of a neuron to the BVC model.
%   Detailed explanation goes here

%% Initialize parameters - these are all the decisions that can be made.
nVariances = 4; % I do the same for both bvc and place - 4 field sizes
% For the place cell templates, need varying field sizes - control with
% variances - in pixels.
% Chosen to represent a wide range of field sizes:
% Standard deviations of ~5-11cm - trials used: 1:4, 2.5:5.5(paper - covers reasonable sizes best), 2.5:7
varianceVals = (2.5:1:5.5).^2; % this vector must be length nVariances.
angleBinSize = 5; % in degrees
distanceBinSize = 2; % in bins. I use 2cm x 2cm bins for my sub data, so 4cm separation of means in BVC model positions.
nEdgeSamples = 5000; % arbitrary - want enough to get samples for all direction bins.
nShuffles = 1000; % arbitrary - our r distribution for each neuron.

% Hartley 2000
% Free variables - dPref, phiPref, sigmaAng, beta, sigmaZero
% Parameters from the Hartley paper:
sigmaAng = 0.2; % paper says 0.2 rad for sigmaRad but doesn't give sigmaAng - I think it is a typo.
beta = 10; % if true to their paper should be 91.5. doesn't work. 183cm - my bins are ~ 2cm x 2cm bins.
sigmaZero = [0.25:0.25:1]; % if true to their paper should be 6.1. doesn't work. 12.2cm - see above.
% sigmaZero also must be length nVariances.

%% Useful constants and initializations based on input variables and parameters.
xSize = size(twoDRMaps,1);
ySize = size(twoDRMaps,1);
nRecs = length(unique(recCount(neuronsToEval)));

% Creating a mask for where there is occupancy.
occAllOrNone = zeros(size(twoDRMaps(:,:,:,1)));
occAllOrNone(twoDRMaps(:,:,:,3) > 0 ) = 1;

nDirections = 360/angleBinSize;
nNeurons = length(neuronsToEval);

% Create all XY coordinates.
makeXsMat = repmat(1:xSize,xSize,1)';
makeYsMat = repmat(1:ySize,ySize,1);
allXYCoords = [makeXsMat(:),makeYsMat(:)];

% Create 2D gaussian fields - then just recenter for templates.
placeFakeFields = nan(2*xSize-1,2*ySize-1,nVariances);
for iVar = 1:nVariances
    placeFakeFields(:,:,iVar) = fspecial('gaussian',[2*xSize-1,2*ySize-1],sqrt(varianceVals(iVar)));
end

%% Output Inits - All are useful for single neuron or small batch 
% outputs to see what is happening
% approxNumberOfPixelsOnPlatform = 1200;
% placeRs = nan(length(neuronsToEval),approxNumberOfPixelsOnPlatform,nVariances);
% placeRandRs = nan(length(neuronsToEval),approxNumberOfPixelsOnPlatform,nVariances,nShuffles);
%
maxDistance = 20; % roughly should be radius of the platform, slightly larger.
% BVCRs = nan(length(neuronsToEval), maxDistance/distanceBinSize,nDirections,nVariances);
% BVCRandRs = nan(length(neuronsToEval), maxDistance/distanceBinSize,nDirections,nVariances,nShuffles);
%
% meanBVCRandRs = nan(length(neuronsToEval));
% stdBVCRandRs = nan(length(neuronsToEval));
% meanPlaceRandRs = nan(length(neuronsToEval));
% stdPlaceRandRs = nan(length(neuronsToEval));

BVC = struct('r',{nan(length(neuronsToEval),1)},...
    'rsAll',{nan(maxDistance/distanceBinSize,nDirections,nVariances,length(neuronsToEval))},...
    'bestTemplate',{nan(xSize,ySize,length(neuronsToEval))},...
    'offR',{nan(length(neuronsToEval),1)},...
    'offBestTemplate',{nan(xSize,ySize,length(neuronsToEval))},...
    'meanRandRs',{nan(length(neuronsToEval),1)},...
    'stdRandRs',{nan(length(neuronsToEval),1)});

Place = struct('r',{nan(length(neuronsToEval),1)},...
    'rsAll',{nan(xSize,ySize,nVariances,length(neuronsToEval))},...
    'bestTemplate',{nan(xSize,ySize,length(neuronsToEval))},...
    'meanRandRs',{nan(length(neuronsToEval),1)},...
    'stdRandRs',{nan(length(neuronsToEval),1)});

radii = zeros(nRecs,1);
centers = nan(nRecs,2);

currRec = 0;
iRec = 0;
for iNeuron = 1:nNeurons;
    currNeuron = neuronsToEval(iNeuron);
    % if the first neuron on this recording, calculate the platform
    % location and the distance of each point to each edge. If not, we'll
    % just use the values from the last neuron.
    if currRec ~= recCount(currNeuron)
        currRec = recCount(currNeuron);
        iRec = iRec+1;
        %%  Create the edge of the platform.
        % Create contour lines separating where there is and is not occupancy.
        c = contourc(occAllOrNone(:,:,currNeuron),1);
        
        % Grab the longest one (the outside of the platform)
        contourBreaks = find(sum(abs(diff(c,2)),1)>1);
        contourStarts = contourBreaks(1:2:end);
        contourLengths = c(2,contourStarts);
        [~,outsideCircleContourStart] = max(contourLengths);
        myContour = c(:,contourStarts(outsideCircleContourStart)+1:contourStarts(outsideCircleContourStart+1)-1);
        % Create a 2D image with the contour filled.
        myFilledContour = poly2mask(myContour(1,:),myContour(2,:),size(occAllOrNone,1),size(occAllOrNone,2));
        % Fit a circle to the contour.
        circleStats = regionprops('table',myFilledContour,'Centroid', ...
            'MajorAxisLength','MinorAxisLength');
        center = circleStats.Centroid;
        diameter = mean([circleStats.MajorAxisLength circleStats.MinorAxisLength],2);
        % We use the circle radius to mark the edge points of the graph. I want
        % the edges marked just outside the graph, not on the edge, so I am
        % adding 1 to the radius.
        radius = 1+diameter/2;
        nDistBins = ceil(radius/distanceBinSize);
        % Get edge XY coordinates.
        edgeXYCoordFineGrain = [cos(2*pi*(0:1/nEdgeSamples:1-1/nEdgeSamples))',...
            sin(2*pi*(0:1/nEdgeSamples:1-1/nEdgeSamples))']*radius+repmat(center,nEdgeSamples,1);
        
        
        %% Calculate the distance and angle from each XY point to each edge sample.
        distancesFromEdges = reshape(pdist2(allXYCoords,edgeXYCoordFineGrain),...
            xSize,ySize,length(edgeXYCoordFineGrain));
        yDiffs = -1*bsxfun(@minus,allXYCoords(:,2),edgeXYCoordFineGrain(:,2)');
        xDiffs = -1*bsxfun(@minus,allXYCoords(:,1),edgeXYCoordFineGrain(:,1)');
        anglesFromEdges = reshape(atan2d(yDiffs(:),xDiffs(:)),xSize,ySize,length(edgeXYCoordFineGrain));
        % Inside the circle - firing when extended over the edge may lead to
        % spurious results.
        inTheCircle = poly2mask(edgeXYCoordFineGrain(:,1),edgeXYCoordFineGrain(:,2),xSize,ySize);
        % Bin to whole pixel distances and whole degree angles
        anglesFromEdgesBinned = round(anglesFromEdges/angleBinSize);
        anglesFromEdgesBinned(anglesFromEdgesBinned == -180/angleBinSize) = 180/angleBinSize;
        
        distancesFromEdgeAtAngleBinned = -1*ones(xSize,ySize,nDirections);
        parfor x = 1:xSize
            for y = 1:ySize
                if inTheCircle(x,y)
                    for iDir = 1:nDirections
                        distancesFromEdgeAtAngleBinned(x,y,iDir) = ...
                            round(mean(distancesFromEdges(x,y,...
                            anglesFromEdgesBinned(x,y,:) == ...
                            iDir-180/angleBinSize))/distanceBinSize);
                    end
                end
            end
        end
        inTheCircle(any(isnan(distancesFromEdgeAtAngleBinned),3)) = 0; % Truly in the circle - hits an edge in all directions.
        distancesFromEdgeAtAngleBinned(repmat(any(isnan(distancesFromEdgeAtAngleBinned),3),[1,1,nDirections])) = -1;
        inCircleAndOccMask = find(inTheCircle & occAllOrNone(:,:,currNeuron));
        [inCircleX,inCircleY] = ind2sub([xSize,ySize],find(inTheCircle));
        
        % Create BVC maps for all distances.
        bvcFakeMaps = nan(xSize,ySize,nDistBins,nDirections,nVariances);
        circleDistances = circ_dist2(...
            deg2rad([-180+angleBinSize:angleBinSize:180]),...
            deg2rad([-180+angleBinSize:angleBinSize:180]));
        parfor dPref = 1:nDistBins
            for iVar = 1:nVariances
                sigmaRad = (dPref/beta + 1)*sigmaZero(iVar);
                for phiPref = 1:nDirections
                    activityValue = -99*ones(xSize,ySize,nDirections);
                    for x = 1:xSize
                        for y = 1:ySize
                            if inTheCircle(x,y)
                                for iDir = 1:nDirections
                                    activityValue(x,y,iDir) = ...
                                        exp(-(distancesFromEdgeAtAngleBinned(x,y,iDir)-dPref).^2/(2*sigmaRad))./...
                                        (2*pi*sigmaRad).^0.5 .*...
                                        exp(-(circleDistances(iDir,phiPref)).^2/(2*sigmaAng))./...
                                        (2*pi*sigmaAng).^0.5;
                                end
                            end
                        end
                    end
                    bvcFakeMaps(:,:,dPref,phiPref,iVar) = sum(activityValue,3);
                end
            end
        end
        % If you want to output the different radii of the platform - shows
        % they are consistent - same with the centers - position under
        % camera.
        radii(iRec) = radius; 
        centers(iRec,:) = center;
    end
    
    %% evaluate templates vs real and shuffled data.
    thisNeuron = twoDRMaps(:,:,currNeuron,1);
    thisNeuronToCorr = thisNeuron(inCircleAndOccMask);
    thisRandNeurons = nan(length(thisNeuronToCorr),nShuffles);
    nToShuffle = length(thisNeuronToCorr);
if nToShuffle > 10 % if < 10 there isn't really any data - less than 10 pixels of occupancy.
    for iShuffle = 1:nShuffles
        thisRandNeurons(:,iShuffle) =...
            thisNeuronToCorr(randperm(nToShuffle));
    end
    
    rBVC = nan(nDistBins,nDirections,nVariances);
    rRandosBVC = nan(nDistBins,nDirections,nVariances,nShuffles);
    parfor dPref = 1:nDistBins
        for phiPref = 1:nDirections
            for iVar = 1:nVariances
                thisBVC = bvcFakeMaps(:,:,dPref,phiPref,iVar);
                [rTMP] = corrcoef(thisNeuronToCorr,...
                    thisBVC(inCircleAndOccMask));
                rBVC(dPref,phiPref,iVar) = rTMP(2);
                rRandosBVC(dPref,phiPref,iVar,:) = corr(thisRandNeurons,...
                    thisBVC(inCircleAndOccMask));
            end
        end
    end
    % For outputting all Rs and RandRs, not just the best and summary
    % statistics. Useful for seeing how it works on a small batch of
    % neurons.
%     BVCRs(iNeuron,1:nDistBins,:,:) = RBVC;
%     BVCRandRs(iNeuron,1:nDistBins,:,:,:) = RRandosBVC;
    
    [xLocsToCorr,yLocsToCorr] = find(inTheCircle);
    nPixelsInCircle = sum(inTheCircle(:));
    rPlace = nan(nPixelsInCircle,nVariances);
    rPlaceGrid = nan(xSize,ySize,nVariances);
    rRandosPlace = nan(nPixelsInCircle,nVariances,nShuffles);
    placeFieldMaps = nan(xSize,ySize,nPixelsInCircle,nVariances);
    parfor iVar = 1:nVariances
        for iValidPlaceFieldLoc = 1:nPixelsInCircle
            xOffset = xSize-inCircleX(iValidPlaceFieldLoc)+1;
            yOffset = ySize-inCircleY(iValidPlaceFieldLoc)+1;
            placeFieldMap = placeFakeFields(xOffset:xSize+xOffset-1,yOffset:ySize+yOffset-1,iVar);
            [rTMP] = corrcoef(thisNeuronToCorr,...
                placeFieldMap(inCircleAndOccMask));
            rPlace(iValidPlaceFieldLoc,iVar) = rTMP(2);
            rRandosPlace(iValidPlaceFieldLoc,iVar,:) = corr(thisRandNeurons,...
                placeFieldMap(inCircleAndOccMask));
            placeFieldMaps(:,:,iValidPlaceFieldLoc,iVar) = placeFieldMap;
        end
    end
    for iValidPlaceFieldLoc = 1:nPixelsInCircle
        rPlaceGrid(xLocsToCorr(iValidPlaceFieldLoc),yLocsToCorr(iValidPlaceFieldLoc),:) =...
            rPlace(iValidPlaceFieldLoc,:);
    end
    
    % For outputting all Rs and RandRs, not just the best and summary
    % statistics. Useful for seeing how it works on a small batch of
    % neurons.
%     placeRs(iNeuron,1:nPixelsInCircle,:) = rPlace;
%     placeRandRs(iNeuron,1:nPixelsInCircle,:,:) = rRandosPlace;
    
    [maxRBVC, maxRBVCInd] = max(rBVC(:));
    [a,b,c] = ind2sub(size(rBVC),maxRBVCInd);
    
    [offRBVC, offRBVCInd] = min(rBVC(:));
    [d,e,f] = ind2sub(size(rBVC),offRBVCInd);

    BVC.r(iNeuron) = maxRBVC;
    BVC.rsAll(1:nDistBins,:,:,iNeuron) = rBVC;
    BVC.bestTemplate(:,:,iNeuron) = bvcFakeMaps(:,:,a,b,c);
    BVC.offR(iNeuron) = offRBVC;
    BVC.offBestTemplate(:,:,iNeuron) = bvcFakeMaps(:,:,d,e,f);
    BVC.meanRandRs(iNeuron) = mean(rRandosBVC(:));
    BVC.stdRandRs(iNeuron) = std(rRandosBVC(:));
    
    [maxRPlace, maxRPlaceInd] = max(rPlace(:));
    [a,b] = ind2sub(size(rPlace),maxRPlaceInd);
    
    Place.r(iNeuron) = maxRPlace;
    Place.rsAll(:,:,:,iNeuron) = rPlaceGrid;
    Place.bestTemplate(:,:,iNeuron) = placeFieldMaps(:,:,a,b);
    Place.meanRandRs(iNeuron) = mean(rRandosPlace(:));
    Place.stdRandRs(iNeuron) = std(rRandosPlace(:));
end
    disp(['Finished Neuron: ',num2str(iNeuron)]);
end

end