function [ bvcMaps ] = bvcScorer( twoDRMaps, recCount, sampleRate, neuronsToMap)
%BVCSCORER Analyzses the fit of a neuron to the BVC model.
%   Detailed explanation goes here

xSize = size(twoDRMaps,1);
ySize = size(twoDRMaps,1);
angleBinSize = 5; % in degrees
distanceBinSize = 2; % in degrees
maxDistance = 40; % roughly should be diameter of the platform.
% Note: Distances should range from 0-max,. I added 1 to the circle radius 
% when finding the edge, so the bins are all effectively 1 closer to the 
% edge than the distance calculated - so we go use the distances 1-max+1.
nDirections = 360/angleBinSize;
nNeurons = length(neuronsToMap);
nEdgeSamples = 5000; % arbitrary - want enough to get samples for all direction bins. 

% Create all XY coordinates.
allXs = repmat(1:xSize,xSize,1);
allYs = repmat(1:ySize,ySize,1)';
allXYCoords = [allXs(:),allYs(:)];

occAllOrNone = zeros(size(twoDRMaps(:,:,:,1)));
occAllOrNone(twoDRMaps(:,:,:,3) > 0 ) = 1;

bvcMaps = nan(nDirections,ceil(maxDistance/distanceBinSize)+1,nNeurons,3);

currRec = 0;
for iNeuron = 1:nNeurons;
    currNeuron = neuronsToMap(iNeuron);
    % if the first neuron on this recording, calculate the platform
    % location and the distance of each point to each edge. If not, we'll
    % just use the values from the last neuron.
    if currRec ~= recCount(currNeuron)
        currRec = recCount(currNeuron);
        %%  Create the edge of the platform.
        % Create contour lines separating where there is and is not occupancy.
        c = contourc(occAllOrNone(:,:,currNeuron),1);
        
        % Grab the longest one (the outside of the platform)
        contourBreaks = find(sum(abs(diff(c,2)),1)>1);
        contourStarts = contourBreaks(1:2:end);
        contourLengths = c(2,contourStarts);
        [~,outsideCircleContourStart] = max(contourLengths);
        myContour = c(:,contourStarts(outsideCircleContourStart)+1:contourStarts(outsideCircleContourStart+1)-1);
        % Create a 2D image with the contour filled.
        myFilledContour = poly2mask(myContour(1,:),myContour(2,:),size(occAllOrNone,1),size(occAllOrNone,2));
        % Fit a circle to the contour.
        circleStats = regionprops('table',myFilledContour,'Centroid', ...
            'MajorAxisLength','MinorAxisLength');
        center = circleStats.Centroid;
        diameter = mean([circleStats.MajorAxisLength circleStats.MinorAxisLength],2);
        % We use the circle radius to mark the edge points of the graph. I want
        % the edges marked just outside the graph, not on the edge, so I am
        % adding 1 to the radius.
        radius = 1+diameter/2;

        % Get edge XY coordinates. Start with too many then eliminate
        % duplicates due to limitation of pixel sizes.
        edgeXYCoordFineGrain = [cos(2*pi*(0:1/nEdgeSamples:1-1/nEdgeSamples))',...
            sin(2*pi*(0:1/nEdgeSamples:1-1/nEdgeSamples))']*radius+repmat(center,nEdgeSamples,1);

        %% Plot the platform and data
        %     figure(241);
        %     imagesc(twoDPlatformAllData(:,:,i,3));
        %     caxis([0 120]);
        %     hold on
        %     viscircles(center,radius);
        %     hold off
        %     figure(242);
        %     imagesc(twoDPlatformAllData(:,:,i,1));
        %     caxis([0 30]);
        %     hold on
        %     viscircles(center,radius);
        %     plot(edgeXYCoord(:,1),edgeXYCoord(:,2),'k');
        %     hold off
        %     figure(243);
        %     imagesc(myFilledContour);
        %     hold on
        %     viscircles(center,radius);
        %     plot(edgeXYCoord(:,1),edgeXYCoord(:,2),'k');
        %     hold off
        %     figure(244);
        %     imagesc(twoDPlatformSmoothed(5:50,5:50,i,1));
        %     hold on
        %     viscircles(center,radius);
        %     hold off
        %     pause
    
        %% Calculate the distance and angle from each XY point to each edge.
        distancesFromEdges = reshape(pdist2(allXYCoords,edgeXYCoordFineGrain),...
            xSize,ySize,length(edgeXYCoordFineGrain));
        yDiffs = bsxfun(@minus,allXYCoords(:,2),edgeXYCoordFineGrain(:,2)');
        xDiffs = bsxfun(@minus,allXYCoords(:,1),edgeXYCoordFineGrain(:,1)');
        anglesFromEdges = reshape(atan2d(yDiffs(:),xDiffs(:)),xSize,ySize,length(edgeXYCoordFineGrain));
        % Inside the circle - firing when extended over the edge may lead to
        % spurious results.
        inTheCircle = poly2mask(edgeXYCoordFineGrain(:,1),edgeXYCoordFineGrain(:,2),xSize,ySize);
        % Bin to whole pixel distances and whole degree angles
        anglesFromEdgesBinned = round(anglesFromEdges/angleBinSize);
        anglesFromEdgesBinned(anglesFromEdgesBinned == -180/angleBinSize) = 180/angleBinSize;
        
        distancesFromEdgeAtAngleBinned = -1*ones(xSize,ySize,nDirections);
        parfor x = 1:xSize
            for y = 1:ySize
                if inTheCircle(x,y)
                    for iDir = 1:nDirections
                        distancesFromEdgeAtAngleBinned(x,y,iDir) = ...
                            round(mean(distancesFromEdges(x,y,...
                            anglesFromEdgesBinned(x,y,:) == iDir-180/angleBinSize))/distanceBinSize);
                    end
                end
            end
        end
        distancesFromEdgeAtAngleBinned(isnan(distancesFromEdgeAtAngleBinned)) = -1;

        %% Debugging/sanity plots
        %     for iEdgePoint = 1:length(edgeXYCoord)
        %         thisDistanceMap = distancesFromEdgesBinned(:,:,iEdgePoint);
        %         thisDistanceMap(~myFilledContour | ~inTheCircle ) = -1;
        %         figure(44);
        %         imagesc(thisDistanceMap);
        % %         imagesc(distancesFromEdges(:,:,iEdgePoint));
        %         colorbar;
        %         figure(45);
        %         thisAngleMap = anglesFromEdgesBinned(:,:,iEdgePoint);
        %         thisAngleMap(~myFilledContour | ~inTheCircle ) = -2*180/angleBinSize;
        %         imagesc(thisAngleMap);
        %         caxis([-2*180/angleBinSize,180/angleBinSize]);
        % %         imagesc(anglesFromEdges(:,:,iEdgePoint));
        %         colorbar;
        %         pause(0.01);
        %     end

    end
    
    %% Construct distance by direction ratemap
    bvcOccs = zeros(nDirections,ceil(maxDistance/distanceBinSize)+1);
    bvcSpikes = zeros(nDirections,ceil(maxDistance/distanceBinSize)+1);
    bvcRates = zeros(nDirections,ceil(maxDistance/distanceBinSize)+1);
    
    xySpikes = twoDRMaps(:,:,currNeuron,2);
    xyOccs = twoDRMaps(:,:,currNeuron,3);
    
    %% Debugging - enable to help when also using the image inside the loop below.
    %     % Gets the bins on the more fine grain circle.
    %     edgeXYCoord = zeros(0,2);
    %     edgeXYCoord(end+1,:) = round(edgeXYCoordFineGrain(1,:));
    %     for iEdgePoint = 2:nEdgeSamples
    %         if any(edgeXYCoord(end,:) ~= round(edgeXYCoordFineGrain(iEdgePoint,:)));
    %             edgeXYCoord(end+1,:) = round(edgeXYCoordFineGrain(iEdgePoint,:));
    %         end
    %     end
    %     % Make the binned circle - mostly for plotting.
    %     myJaggedCircle = zeros(size(inTheCircle));
    %     for i = 1:length(edgeXYCoord)
    %         myJaggedCircle(edgeXYCoord(i,1),edgeXYCoord(i,2)) = 1;
    %     end
    %     figure(81)
    %     imagesc(inTheCircle);
    %     pause(0.25)
    %     hold on;
    %     viscircles(center,radius);
    %     figure(82)
    %     imagesc(myFilledContour);
    %     hold on;
    %     viscircles(center,radius);
    %     figure(83)
    %     imagesc(myJaggedCircle);
    %     hold on;
    %     viscircles(center,radius);
    

    %% Actual ratemap creation.
    for iDirectionInBins = 1:nDirections
        for iDistanceInBins = 1:ceil(maxDistance/distanceBinSize)+1 % round(sqrt(xSize^2+ySize^2))-1
            binMask = distancesFromEdgeAtAngleBinned(:,:,iDirectionInBins) == iDistanceInBins;
            bvcSpikes(iDirectionInBins,iDistanceInBins) = sum(sum(xySpikes(binMask)));
            bvcOccs(iDirectionInBins,iDistanceInBins) = sum(sum(xyOccs(binMask)));
            
            % Previous way - more intuitive but binning breaks down.
            %         binMask = any(anglesFromEdgesBinned == (iDirectionInBins-180/angleBinSize) &...
            %             distancesFromEdgesBinned == iDistanceInBins,3) &...
            %             myFilledContour & inTheCircle;
            %             bvcSpikes(iDirectionInBins,iDistanceInBins+1) = sum(sum(xySpikes(binMask)));
            %             bvcOccs(iDirectionInBins,iDistanceInBins+1) = sum(sum(xyOccs(binMask)));
            
            % Debugging images - waves of pixels
            %             figure(80)
            %             imagesc(binMask);
            %             title([num2str(iDirectionInBins*angleBinSize-180),' angle in degrees to edge, '...
            %                 num2str(iDistanceInBins),' bins from edge']);
            %             hold on
            %             viscircles(center,radius);
            %             pause(0.1);
        end
    end
    
    isOcc = bvcOccs>0;
    bvcRates(isOcc) = (bvcSpikes(isOcc)./bvcOccs(isOcc))*sampleRate(currRec);
    bvcRates(~isOcc) = NaN;
    
    bvcMaps(:,:,iNeuron,1)=bvcRates;
    bvcMaps(:,:,iNeuron,2)=bvcSpikes;
    bvcMaps(:,:,iNeuron,3)=bvcOccs;  
    radiusValues(iNeuron) = radius;
    centers(:,iNeuron) = center;
end
end

