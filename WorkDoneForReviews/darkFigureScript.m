% Dark Supplemental Figure Script
% Created a neuron list and processed dvt list as with the main datasets.
% Only consists of one recording: NS14 Rec 23. Worth noting that notes in
% notebook state the axis cells stayed with the room when the track turned,
% even in the dark.
subPaperDarkStruct = fileListParser('subPaperDarkNeuronList.txt','subPaperDarkProcessedDVTList.txt');
subPaperDarkStruct = ratTrueNorthCalculator(subPaperDarkStruct);
[SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = addMeanLinearRates(subPaperDarkStruct);
[SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = pathCorrelations(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct);
[SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] =  mapSpikesAndLocsToSamples(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct);
[SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] =  isRunningAnalysis(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, 3);
DS = SubPaperDarkDataStruct;
NS = SubPaperDarkNeuronStruct;
DS.sampleRate = 60;
NS.recCount = ones(11,1);
[DS, twoDPlatformDarkControlAllData] = twoDRateMapper(DS,NS,1,6+2/3,300,300,0);
[DS, twoDTrackDarkControlAllData] = twoDRateMapper(DS,NS,2,6,1,1,0);
[DS, twoDTrackDarkActualAllData] = twoDRateMapper(DS,NS,3,6,1,1,0);
for i = 1:11
figure(1);
imagesc(twoDPlatformDarkControlAllData(:,:,i));
figure(2);
imagesc(twoDTrackDarkControlAllData(:,:,i));
figure(3);
imagesc(twoDTrackDarkActualAllData(:,:,i));
pause;
end
twoDPlatformDarkControlAllDataSmoothed = filter2DMatrices(twoDPlatformDarkControlAllData,1);
twoDTrackDarkControlAllDataSmoothed = filter2DMatrices(twoDTrackDarkControlAllData,1);
twoDTrackDarkActualAllDataSmoothed = filter2DMatrices(twoDTrackDarkActualAllData,1);
load('figureColormap.mat');
for i = 1:11
platCMax = 2*nanmean(nanmean(twoDPlatformDarkControlAllDataSmoothed(:,:,i)));
trackCMax = nanmean(nanmean(twoDTrackDarkControlAllDataSmoothed(:,:,i)))+nanmean(nanmean(twoDTrackDarkActualAllDataSmoothed(:,:,i)));
figure(1)
sc(twoDPlatformDarkControlAllDataSmoothed(:,:,i),hotMapClipped,[0,platCMax],'w',isnan(twoDPlatformDarkControlAllDataSmoothed(:,:,i)));
figure(2)
sc(twoDTrackDarkControlAllDataSmoothed(:,:,i),hotMapClipped,[0,trackCMax],'w',isnan(twoDTrackDarkControlAllDataSmoothed(:,:,i)));
figure(3)
sc(twoDTrackDarkActualAllDataSmoothed(:,:,i),hotMapClipped,[0,trackCMax],'w',isnan(twoDTrackDarkActualAllDataSmoothed(:,:,i)));
pause;
end
[SubPaperDarkDataStruct, platformIsRunning2DByHDRMapsDark, platformStill2DByHDRMapsDark] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,1,6+2/3,300,300,1);
[SubPaperDarkDataStruct, platform2DByHDRMapsDark] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,1,6+2/3,300,300,0);
[SubPaperDarkDataStruct, trackIsRunning2DByHDRMapsDarkControl, trackStill2DByHDRMapsDarkControl] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,2,6,1,1,1);
[SubPaperDarkDataStruct, track2DByHDRMapsDarkControl] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,2,6,1,1,0);
[SubPaperDarkDataStruct, trackIsRunning2DByHDRMapsDarkActual, trackStill2DByHDRMapsDarkActual] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,3,6,1,1,1);
[SubPaperDarkDataStruct, track2DByHDRMapsDarkActual] = hdBy2DBinsRateMapper(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct,3,6,1,1,0);
platform2DRMapsDark = twoDFromHDBy2DRMaps(platform2DByHDRMapsDark,SubPaperDarkDataStruct.sampleRate(1));
platformIsRunning2DRMapsDark = twoDFromHDBy2DRMaps(platformIsRunning2DByHDRMapsDark,SubPaperDarkDataStruct.sampleRate(1));
platformStill2DRMapsDark = twoDFromHDBy2DRMaps(platformStill2DByHDRMapsDark,SubPaperDarkDataStruct.sampleRate(1));
track2DRMapsDarkControl = twoDFromHDBy2DRMaps(track2DByHDRMapsDarkControl,SubPaperDarkDataStruct.sampleRate(1));
trackIsRunning2DRMapsDarkControl = twoDFromHDBy2DRMaps(trackIsRunning2DByHDRMapsDarkControl,SubPaperDarkDataStruct.sampleRate(1));
trackStill2DRMapsDarkControl = twoDFromHDBy2DRMaps(trackStill2DByHDRMapsDarkControl,SubPaperDarkDataStruct.sampleRate(1));
track2DRMapsDarkActual = twoDFromHDBy2DRMaps(track2DByHDRMapsDarkActual,SubPaperDarkDataStruct.sampleRate(1));
trackIsRunning2DRMapsDarkActual = twoDFromHDBy2DRMaps(trackIsRunning2DByHDRMapsDarkActual,SubPaperDarkDataStruct.sampleRate(1));
trackStill2DRMapsDarkActual = twoDFromHDBy2DRMaps(trackStill2DByHDRMapsDarkActual,SubPaperDarkDataStruct.sampleRate(1));
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, platform2DByHDRMapsDark, 1, 0, 0);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, platformIsRunning2DByHDRMapsDark, 1, 1, 1);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, platformStill2DByHDRMapsDark, 1, 1, 0);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, track2DByHDRMapsDarkControl, 2, 0, 0);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, trackIsRunning2DByHDRMapsDarkControl, 2, 1, 1);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, trackStill2DByHDRMapsDarkControl, 2, 1, 0);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, track2DByHDRMapsDarkActual, 3, 0, 0);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, trackIsRunning2DByHDRMapsDarkActual, 3, 1, 1);
[ SubPaperDarkDataStruct, SubPaperDarkNeuronStruct] = hdBy2DSpatialReliabilityCalculator(SubPaperDarkDataStruct, SubPaperDarkNeuronStruct, trackStill2DByHDRMapsDarkActual, 3, 1, 0);
[ SubPaperDarkControlRunningCategorizedAxisCells ] = categorizeAxisCells( SubPaperDarkDataStruct,SubPaperDarkNeuronStruct, [], SubPaperDarkNeuronStruct.hdRMapsRunningHalves, 0, 'mixVMMDarkControlTrackRunning', 2, SubPaperDarkNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperDarkNeuronStruct.hdRMapsRunning, SubPaperDarkNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
for i = [2,7,9]
platCMax = 2*nanmean(nanmean(twoDPlatformDarkControlAllDataSmoothed(:,:,i)));
trackCMax = nanmean(nanmean(twoDTrackDarkControlAllDataSmoothed(:,:,i)))+nanmean(nanmean(twoDTrackDarkActualAllDataSmoothed(:,:,i)));
figure(1)
sc(twoDPlatformDarkControlAllDataSmoothed(:,:,i),hotMapClipped,[0,platCMax],'w',isnan(twoDPlatformDarkControlAllDataSmoothed(:,:,i)));
figure(2)
sc(twoDTrackDarkControlAllDataSmoothed(:,:,i),hotMapClipped,[0,trackCMax],'w',isnan(twoDTrackDarkControlAllDataSmoothed(:,:,i)));
figure(3)
sc(twoDTrackDarkActualAllDataSmoothed(:,:,i),hotMapClipped,[0,trackCMax],'w',isnan(twoDTrackDarkActualAllDataSmoothed(:,:,i)));
pause;
end

[DS, twoDTrackDarkControlAllDataSuppFig] = twoDRateMapper(DS,NS,2,1,1,1,0);
[DS, twoDTrackDarkActualAllDataSuppFig] = twoDRateMapper(DS,NS,3,1,1,1,0);
twoDTrackDarkControlAllDataSuppFigSmoothed = filter2DMatrices(twoDTrackDarkControlAllDataSuppFig,0);
twoDTrackDarkActualAllDataSuppFigSmoothed = filter2DMatrices(twoDTrackDarkActualAllDataSuppFig,0);

[DS,NS] = addMeanFR(DS,NS);

%% Actual Figures - in exact style of figures 1 and 3
overallFR = NS.overallMeanFR;
load('figureColormap.mat');
mapToUse = hotMapClipped;
selected = SubPaperDarkControlRunningCategorizedAxisCells.finalAxisLIPReliableRatio; % axis cells.
neuronsToPlot = selected;
for i = 1:length(neuronsToPlot)
%     maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
    caxisMax = 2*overallFR(neuronsToPlot(i),2);
    
%     figure(1)
%     % Code to control axis size.
%     t = 0 : .01 : 2 * pi;
%     P = polar(t, maxHDRate * ones(size(t)));
%     set(P, 'Visible', 'off')
%     hold on;
%     set(gca, 'ColorOrder',[204,0,0]/255);
%     rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
%         NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
%     hold off;
    
    figure(2)
    sc(twoDTrackDarkControlAllDataSuppFigSmoothed(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(twoDTrackDarkControlAllDataSuppFigSmoothed(:,:,neuronsToPlot(i),1)));
    
%     figure(3)
%     % Code to control axis size.
%     t = 0 : .01 : 2 * pi;
%     P = polar(t, maxHDRate * ones(size(t)));
%     set(P, 'Visible', 'off')
%     hold on;
%     set(gca, 'ColorOrder',[204,0,0]/255);
%     rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
%         NS.hdRMapsRunning(:,neuronsToPlot(i),1,1));
%     hold off;
    
    figure(4)
    sc(twoDTrackDarkActualAllDataSuppFigSmoothed(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
        isnan(twoDTrackDarkActualAllDataSuppFigSmoothed(:,:,neuronsToPlot(i),1)));
    caxisMax
%     rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%         isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
%     alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
%     image(rmapImage,'alphaData',alphamap);
    disp(neuronsToPlot(i));
    pause;
end


