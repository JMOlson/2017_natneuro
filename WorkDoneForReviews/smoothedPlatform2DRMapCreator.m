%If on my mac:
load('/Volumes/JMODataDisk/AxisCellPaper/FinalAnalysisResults/MainDataset/SubPaperDataStruct.mat')
load('/Volumes/JMODataDisk/AxisCellPaper/FinalAnalysisResults/MainDataset/SubPaperNeuronStruct.mat')
NS = SubPaperNeuronStruct;
DS = SubPaperDataStruct;
for i = 1:85
DS.recFile{i}([3,14,23]) = '/';
DS.recFile{i} = ['/Volumes/JMODataDisk',DS.recFile{i}(3:end)];
end

% ORRRR if on PC
load('D:/AxisCellPaper/FinalAnalysisResults/MainDataset/SubPaperDataStruct.mat')
load('D:/AxisCellPaper/FinalAnalysisResults/MainDataset/SubPaperNeuronStruct.mat')
NS = SubPaperNeuronStruct;
DS = SubPaperDataStruct;

[DS, twoDPlatformAllData] = twoDRateMapper(DS,NS,1,6+2/3,300,300,0);
twoDPlatformSmoothed = filter2DMatrices(twoDPlatformAllData,1);
for i = 1:542
imagesc(twoDPlatformSmoothed(:,:,i));
disp(i)
pause;
end