%% Entire History from day of looking at single head direction neurons on track - response to review 1

load('SubPaperDataStruct.mat')
load('SubPaperNeuronStruct.mat')
load('SubPaperMainTrackRunningCategorizedAxisCells.mat')
axisCells = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;
[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, axisCells, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainTrackRunning', 2, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
headDirectionOnTrack = baloney;
load('FIGURE_2DRMAPS.mat')
sc(fullTrackIsRunning2DRMaps(:,:,38),[0,20],parula,'w',isnan(fullTrackIsRunning2DRMaps(:,:,38)));
hdOnTrackNeurons = baloney.finalAxisLIPReliableRatio;
for i = 1:length(hdOnTrackNeurons)
maxFR = max(max(fullTrackIsRunning2DRMapsNanConv(:,:,hdOnTrackNeurons(i))));
figure(1)
sc(fullTrackIsRunning2DRMapsNanConv(:,:,hdOnTrackNeurons(i)),[0,0.75*maxFR],parula,'w',isnan(fullTrackIsRunning2DRMapsNanConv(:,:,hdOnTrackNeurons(i))));
figure(2)
imagesc(fullPlatformIsRunning2DRMapsNanConv(:,:,hdOnTrackNeurons(i)));
pause;
end
load('twoDPlatformSmoothed.mat')
NS = SubPaperNeuronStruct;
DS = SubPaperDataStruct;

overallFR = SubPaperNeuronStruct.overallMeanFR;
load('figureColormap.mat');
mapToUse = hotMapClipped;

[ baloney ] = categorizeAxisCells( SubPaperDataStruct,SubPaperNeuronStruct, axisCells, SubPaperNeuronStruct.hdRMapsRunningHalves, 1, 'mixVMMMainTrackRunning', 2, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunning, SubPaperNeuronStruct.hdRMapsRunning, SubPaperNeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling);
axisLIP = baloney.axisLIP;

circVarAxisOnTrack = 1 - besseli(1,mEMMain(1:2,3,axisCells))./besseli(0,mEMMain(1:2,3,axisCells));
circVarHDOnTrack = 1 - besseli(1,mEMMain(1,2,hdOnTrackNeurons))./besseli(0,mEMMain(1,2,hdOnTrackNeurons));
circVarAxisDeg = rad2deg(circVarAxisOnTrack(:));
circVarHDDeg = rad2deg(circVarHDOnTrack(:));
hist(circVarAxisDeg)
title('axis')
hist(circVarHDDeg)
title('HD')

diag(corr(NS.hdRMapsRunning(:,hdOnTrackNeurons,1,1),NS.hdRMapsRunning(:,hdOnTrackNeurons,1,1)));
corrValsHDTrackVsPlatform = diag(corr(NS.hdRMapsRunning(:,hdOnTrackNeurons,1,1),NS.hdRMapsRunning(:,hdOnTrackNeurons,1,1)));
figure;
hist(corrValsHDTrackVsPlatform)
corrValsHDTrackVsPlatform = diag(corr(NS.hdRMapsRunning(:,hdOnTrackNeurons,1,1),NS.hdRMapsRunning(:,hdOnTrackNeurons,2,1)));
hist(corrValsHDTrackVsPlatform)
figure
scatter(circVarHDDeg,corrValsHDTrackVsPlatform);

corrValsAxisTrackVsPlatform = diag(corr(NS.hdRMapsRunning(:,axisCells,1,1),NS.hdRMapsRunning(:,axisCells,2,1)));
hold on;
circVarAxisDeg2 = rad2deg(mean(circVarAxisOnTrack));
scatter(circVarAxisDeg2,corrValsAxisTrackVsPlatform);

sum(circVarHDDeg > mean(circVarAxisDeg)+2*stdDevAxisVar)
mean(circVarAxisDeg)
stdDevAxisVar = std(circVarAxisDeg)
mean(circVarAxisDeg)+2*stdDevAxisVar
mean(circVarAxisDeg)-2*stdDevAxisVar
mean(circVarHDDeg)
std(circVarHDDeg)