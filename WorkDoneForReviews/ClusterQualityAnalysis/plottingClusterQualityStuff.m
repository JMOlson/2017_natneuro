figure;
scatter(axisCellsClusterQualityScores(:,1),axisCellsClusterQualityScores(:,2),16,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),16,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),32,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),64,'.')
scatter(log(ClusterQualitySubPaperMainDataset.lRatio),log(ClusterQualitySubPaperMainDataset.isolationDistance),64,'.')
hold on;
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),64,'.')
title('log(lRatio) vs log(isoDistance)')
a = sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,2);
figure;
hist(ClusterQualitySubPaperMainDataset.lRatio)
hist(log(ClusterQualitySubPaperMainDataset.lRatio))
hist(ClusterQualitySubPaperMainDataset.lRatio,100)
sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,2);
a = sortrows(axisCellsClusterQualityScores,1);

%% For Supplemental Figure 2
% figure;
% histogram(log10(ClusterQualitySubPaperMainDataset.isolationDistance),100,'FaceColor',[0,0,1],'EdgeColor',[0,0,1]);
% hold on;
% histogram(log10(axisCellsClusterQualityScores(:,2)),25,'FaceColor','Red');
% legend('All Subiculum Neurons','Axis-Tuned Neurons')
% title('Histogram of Isolation Distances')

figure;
histogram(ClusterQualitySubPaperMainDataset.isolationDistance,10.^(0.1:0.1:4))
set(gca,'xScale','log')
hold on;
histogram(axisCellsClusterQualityScores(:,2),10.^(0.1:0.1:4))
set(gca,'xTickLabel',[1,10,100,1000,10000])

%% Get Iso Distances for figure neurons
ratRecWireNumber = [14,3,46,1];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [14,3,46,2];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [14,3,46,3];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [14,3,46,4];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [14,3,46,7];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [15,19,5,1];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [16,14,41,1];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [16,14,41,2];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))
ratRecWireNumber = [16,14,41,3];
ClusterQualitySubPaperMainDataset.isolationDistance(find(ismember(neuronsAnalyzedLabels,ratRecWireNumber,'rows')))