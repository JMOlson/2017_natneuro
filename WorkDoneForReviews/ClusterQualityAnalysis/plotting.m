figure;
scatter(axisCellsClusterQualityScores(:,1),axisCellsClusterQualityScores(:,2),16,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),16,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),32,'.')
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),64,'.')
scatter(log(ClusterQualitySubPaperMainDataset.lRatio),log(ClusterQualitySubPaperMainDataset.isolationDistance),64,'.')
hold on;
scatter(log(axisCellsClusterQualityScores(:,1)),log(axisCellsClusterQualityScores(:,2)),64,'.')
title('log(lRatio) vs log(isoDistance)')
a = sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,2);
figure;
hist(ClusterQualitySubPaperMainDataset.lRatio)
hist(log(ClusterQualitySubPaperMainDataset.lRatio))
hist(ClusterQualitySubPaperMainDataset.lRatio,100)
sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,1);
a = sortrows(axisCellsClusterQualityScores,2);
a = sortrows(axisCellsClusterQualityScores,1);
