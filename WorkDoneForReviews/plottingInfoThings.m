% Plotting info things.
figure;
hist(NS.platformMeanFR,0:2:50)
hist(NS.platformMeanFR(axisCells),0:2:50)
hold on;
scatter(NS.platformSpatialInfoPerSpikeInBits(axisCells),NS.platformSpatialCoherence(axisCells),16,'x');
hold off;
scatter(NS.platformSpatialInfoInBits(axisCells),NS.platformSpatialCoherence(axisCells),16,'x');
scatter(NS.platformSpatialInfoInBits,NS.platformSpatialCoherence,16);
hold on;
scatter(NS.platformSpatialInfoInBits(axisCells),NS.platformSpatialCoherence(axisCells),16,'x');
title('Info Vs Coh')
hold on;
scatter(NS.platformSpatialInfoPerSpikeInBits(axisCells),NS.platformSpatialCoherence(axisCells),16,'x');
title('InfoPerSpike vs Coh')
hold on;
scatter(NS.platformSpatialSparsity(axisCells),NS.platformSpatialCoherence(axisCells),16,'x');
title('Sparsity vs Coh')
title('Info vs InfoPerSpike')
figure;
scatter(NS.platformSpatialSparsity,NS.platformSpatialInfoInBits);
hold on;
scatter(NS.platformSpatialSparsity(axisCells),NS.platformSpatialInfoInBits(axisCells),16,'x');
title('Sparsity vs Info')
NS.platformSpatialCoherence;
platformSpatialCoherence = NS.platformSpatialCoherence;
platformInfoInBits = NS.platformSpatialInfoInBits;
platformInfoPerSpikeInBits = NS.platformSpatialInfoPerSpikeInBits;
platformSparsity = NS.platformSpatialSparsity;
platformMeanFR = NS.platformMeanFR;
platformMaxFR = NS.platformMaxFR;