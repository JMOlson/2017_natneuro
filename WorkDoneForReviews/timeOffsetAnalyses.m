function [ DataStruct, NeuronStruct ] = timeOffsetAnalyses( DataStruct, NeuronStruct, nTimeSegments )
%NAMEHERE Calculates 
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, July 2016
%   Last updated by Jake Olson, July 2016.

% Define binning choices here - all in pixels. ~ 3 pixels to the cm.
DataStruct.velBinSize = 6; % log bins would probably be better...
DataStruct.velMaxBinCenter = 297; % 6 and 297 give 50 2 cm/s bins. (3:6:297)
DataStruct.accBinSize = 6; % log bins would probably be better...
DataStruct.accMaxBinCenter =447; % 6 and 447 give 75 2 cm/s bins. (3:6:447)
DataStruct.angVelBinSize = pi; % log bins would probably be better...
DataStruct.angVelMaxBinCenter = 10*pi; % pi and 10*pi gives 21 bins (-10*pi:pi:10*pi)


binSizeDegrees = DataStruct.binSizeDegrees;
nHDBins = 360/binSizeDegrees;
nRecs = DataStruct.nRecs;
iOverallNeuron = 1;
for iRec = 1:nRecs
    subNeuronInds = DataStruct.neuronList{iRec};
    sampleRate = DataStruct.sampleRate(iRec);
    timeStamps = DataStruct.timeStamps{iRec};
    nCells = length(subNeuronInds);
    processedDvtFile = DataStruct.dvtFile{iRec};
    load(processedDvtFile,'acc','vel','processedDVT');
    
    %% (Re)Create basic velocity and acceleration variables and store in
    % data struct
    timeToOffsetDueToSmoothingAcc = round((length(processedDVT)-length(acc))/2);
    timeToOffsetDueToSmoothingVel = round((length(processedDVT)-length(vel))/2);
    velAvg = nanmean(vel(:,1,:),3); % vel is in pixels/seconds. angle is in radians. Combines the two lights.
    % vel is calculated over 1/10 of a second (6 samples apart).
    accAvg = nanmean(acc(:,1,:),3); % acc is in pixels/seconds^2. angle is in radians. Combines the two lights.
    % acc is calculated over the instantaneous change of the 1/10 sec
    % vel.
    
    % velDirections is in: -pi : pi - this calculates angular velocity.
    velDir = circ_mean(squeeze(vel(:,2,:)),[],2);
    accDir = circ_mean(squeeze(acc(:,2,:)),[],2);
    velDir2 = velDir;
    velDir2(velDir2<0) = velDir2(velDir<0)+2*pi;
    angVelTmp = [NaN;diff(velDir)];
    angVelTmp2 = [NaN;diff(velDir2)];
    angVel = angVelTmp;
    tmp2RightAngVel = abs(angVelTmp) > abs(angVelTmp2);
    angVel(tmp2RightAngVel) = angVelTmp2(tmp2RightAngVel);
    
    % Convert to radians per second - calculated at sampling rate (60hz)
    % but direction is calculated over vector calculated from samples 1/10
    % of a second apart - like the velocity.
    angVel = angVel*60;
    
    % Smoothing over 1/10 of a second + 1 sample (to be odd, it is the 7, 2nd argument). 
    % Redundant? Maybe helps individual
    % outlier points. I did it in isRunningAnalysis to vel and angVel.
    velAvgSmo = smooth(velAvg(:,1),7,'sgolay');
    accAvgSmo = smooth(accAvg(:,1),7,'sgolay');
    angVelSmo = smooth(angVel(:,1),7,'sgolay');
    
    for i = 1:nTimeSegments
        velAvgTimeSeg = velAvgSmo(timeStamps(2*i-1)-timeToOffsetDueToSmoothingVel:...
            timeStamps(2*i)-timeToOffsetDueToSmoothingVel,1);
        accAvgTimeSeg = accAvgSmo(timeStamps(2*i-1)-timeToOffsetDueToSmoothingAcc:...
            timeStamps(2*i)-timeToOffsetDueToSmoothingAcc,1);
        angVelTimeSeg = angVelSmo(timeStamps(2*i-1)-timeToOffsetDueToSmoothingVel:...
            timeStamps(2*i)-timeToOffsetDueToSmoothingVel);
        
        velDirAvgTimeSeg = velDir(timeStamps(2*i-1)-timeToOffsetDueToSmoothingVel:...
            timeStamps(2*i)-timeToOffsetDueToSmoothingVel,1);
        accDirAvgTimeSeg = accDir(timeStamps(2*i-1)-timeToOffsetDueToSmoothingAcc:...
            timeStamps(2*i)-timeToOffsetDueToSmoothingAcc,1);
        
        % Overwrite vel and angVel from isRunning because I switched the
        % variables when writing them to the struct!
        DataStruct.vel{i,iRec} = velAvgTimeSeg;
        DataStruct.acc{i,iRec} = accAvgTimeSeg;
        DataStruct.angVel{i,iRec} = angVelTimeSeg;
        DataStruct.velAngle{i,iRec} = velDirAvgTimeSeg;
        DataStruct.accAngle{i,iRec} = accDirAvgTimeSeg;
    end    
       
    %% Now actually calculate values for ideothetic time offset analyses. 
    % Now going analyze vel, ang vel, acc, and HD vs spiking at different
    % time offsets a la Whitlock - although he is using space movement not
    % vel/acc movement...
    for iNeuron = 1:nCells
        iNeuNSpikes = NeuronStruct.posNSpikesAll{iOverallNeuron+iNeuron-1};

        iNeuHDRadians = NeuronStruct.posHDRadiansAllCorrected{iOverallNeuron+iNeuron-1};

        for iTimeSegment = 1:nTimeSegments
            % if statement is there to ignore time periods with no data
            % (literally less than 6 samples).
            if timeStamps(iTimeSegment*2)-timeStamps(iTimeSegment*2-1) > 5
                % Get data for the time period.
                hdThisTimeSeg = iNeuHDRadians(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                spikeCountsThisTimeSeg = iNeuNSpikes(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                velThisTimeSeg = DataStruct.vel{iTimeSegment,iRec};
                accThisTimeSeg = DataStruct.acc{iTimeSegment,iRec};
                angVelThisTimeSeg = DataStruct.angVel{iTimeSegment,iRec};
                
                
                % Bin all data types appropriately.
                
                % HD binning
                % mod to get all positive values.
                % round to nearest bin center. 
                hdThisTimeSegBinned =...
                    round(mod(radtodeg(hdThisTimeSeg),360)/binSizeDegrees);
                % put all values from bin 0 into the last bin (same bin, circle).
                hdThisTimeSegBinned(hdThisTimeSegBinned==0) = nHDBins;
                hdThisTimeSegBinned(isnan(hdThisTimeSeg)) = NaN;
                
                
                % velBinning
                velThisTimeSegBinned = round((velThisTimeSeg+DataStruct.velBinSize/2)...
                    /DataStruct.velBinSize);
                velThisTimeSegBinned(isnan(velThisTimeSeg)) = NaN;
                nVelBins = length(DataStruct.velBinSize/2:DataStruct.velBinSize:DataStruct.velMaxBinCenter);
                
                % acc Binning
                
                
                % angVelBinning
                
                
                

                nTimeOffsets = length(-sampleRate:sampleRate); % 1sec past:1sec future
                
                hdOccs=zeros(nHDBins,nTimeOffsets);
                hdSpikes=zeros(nHDBins,nTimeOffsets);
                hdRates=zeros(nHDBins,nTimeOffsets);
                
                velOccs=zeros(nVelBins,nTimeOffsets);
                velSpikes=zeros(nVelBins,nTimeOffsets);
                velRates=zeros(nVelBins,nTimeOffsets);
                
%                 accOccs=zeros(nAccBins,nTimeOffsets);
%                 accSpikes=zeros(nAccBins,nTimeOffsets);
%                 accRates=zeros(nAccBins,nTimeOffsets);
%                 
%                 angVelOccs=zeros(nAngVelBins,nTimeOffsets);
%                 angVelSpikes=zeros(nAngVelBins,nTimeOffsets);
%                 angVelRates=zeros(nAngVelBins,nTimeOffsets);
                
                for iOffset = -sampleRate:sampleRate % Go past->future
                    % shift the spike train
                    if iOffset < 0 % spikes aligned to past behavior.
                        spikesOffset = spikeCountsThisTimeSeg(-iOffset+1:end);
                        hdOffset = hdThisTimeSegBinned(1:end+iOffset);
                        velOffset = velThisTimeSegBinned(1:end+iOffset);
                    elseif iOffset > 0 % spikes aligned to future behavior.
                        spikesOffset = spikeCountsThisTimeSeg(1:end-iOffset);
                        hdOffset = hdThisTimeSegBinned(iOffset+1:end);
                        velOffset = velThisTimeSegBinned(iOffset+1:end);
                    else
                        spikesOffset = spikeCountsThisTimeSeg;
                        hdOffset = hdThisTimeSegBinned;
                        velOffset = velThisTimeSegBinned;
                    end
                    
                    for iHDBins = 1:nHDBins
                        hdSpikes(iHDBins,iOffset+sampleRate+1) = sum(spikesOffset(hdOffset == iHDBins));
                        hdOccs(iHDBins,iOffset+sampleRate+1) = sum(hdOffset == iHDBins);
                    end
                    for iVelBins = 1:nVelBins
                        velSpikes(iVelBins,iOffset+sampleRate+1) = sum(spikesOffset(velOffset == iVelBins));
                        velOccs(iVelBins,iOffset+sampleRate+1) = sum(velOffset == iVelBins);
                    end
                    
                end
                    
                isOcc = hdOccs>0;
                hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*sampleRate;
                hdRates(~isOcc) = NaN;
                
                NeuronStruct.hdTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=hdRates;
                NeuronStruct.hdTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=hdSpikes;
                NeuronStruct.hdTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=hdOccs;
         
                
                isOcc = velOccs>0;
                velRates(isOcc) = (velSpikes(isOcc)./velOccs(isOcc))*sampleRate;
                velRates(~isOcc) = NaN;
                
                NeuronStruct.velTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=velRates;
                NeuronStruct.velTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=velSpikes;
                NeuronStruct.velTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=velOccs;
                  
            else
                % set Nan values for output variables for nonexistant time
                % segments.
                NeuronStruct.hdTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,:)=NaN;
                
                NeuronStruct.velTimeOffsetRMaps(:,:,iOverallNeuron+iNeuron-1,iTimeSegment,:)=NaN;
            end
        end
    end
    iOverallNeuron = iOverallNeuron + nCells;
end
end       