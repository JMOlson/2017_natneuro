function [ posNSpikes, posPathAndBin] = mapSpikesAndLocsToSamples( dataFile, processedDvtFile, neuronNumber)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015
%   Edited by Jake Olson, May 2016


% Create a dvt w/ nSpikes at that sample and the path and bin of the
% sample. Can then get HD, vel, etc, for each sample from the vectors of
% corresponding length already made.

load(dataFile,'pos','allTfiles','sampleRate','nRuns',...
    'runPosMarkers','pathList','PathTemplate'); %,'filterValuesNormed','filterMedian'); % last 2 for linear rmaps
load(processedDvtFile,'processedDVT','HDRadians');

%% Check that both processedDvt and the spikes are all sorted ascending and
% pos and processedDvt are equal.
if any(pos(:,1) ~= processedDVT(:,2))
    disp('Pos times doesn''t equal processedDvt times. Yikes!');
    return
end
if any(diff(processedDVT(:,2))<0)
    disp(['Processed dvt is not in chronological order.']);
    return
end
if any(diff(allTfiles{neuronNumber})<0)
    disp(['Neuron ', num2str(neuronNumber),' spikes are not in chronological order.']);
    return;
end

%% Calc nSpikes for each sample.
posNSpikes = zeros(size(pos,1),1);
nSamples = size(pos,1);
tfile = allTfiles{neuronNumber};
posIndex = 1;
for iSpike =1:length(tfile)
    % Cycle through dvt until you hit the time that this spike goes to.
    while tfile(iSpike) > (processedDVT(posIndex,2)+(1/(2*sampleRate))) &&...
            posIndex <= nSamples
        posIndex = posIndex+1;
    end
    if posIndex > nSamples
        disp('Spikes exist after the last sample!');
        return
    else
        posNSpikes(posIndex) = posNSpikes(posIndex) + 1;
    end
end

    %% Find the closest bin for each path
    posPathAndBin = nan(size(pos,1),3);
    posPathAndBin(:,1) = pos(:,1);
    for iPath = 1:length(pathList)
        template = PathTemplate{iPath};
        thisPathPosMarkers = [runPosMarkers{iPath,1},runPosMarkers{iPath,2}];
        for iRun=1:length(thisPathPosMarkers(:,1))  % each outbound run
            for iPoint = thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2)  % each time-point during this run
                if pos(iPoint,2)>1 && pos(iPoint,3)>1  % if we have valid tracking
                    
                    % Find the closest bin for the current path.
                    templateDist = dist(pos(iPoint,2:3),template(:,2:3)');
                    [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                    posPathAndBin(iPoint,2:3) = [pathList(iPath),nearestPointInd(1)];
                end
            end
        end
    end
end
