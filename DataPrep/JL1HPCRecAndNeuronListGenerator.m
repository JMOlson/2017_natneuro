jl1HPCRecListFID = fopen('jl1HPCRecList','w');
jl1HPCNeuronListFID = fopen('jl1HPCNeuronList','w');
path = 'D:\allSubData\JL1HPCData\';
writePath = 'D:\\allSubData\\JL1HPCData\\';
allFileNames = ls(path);
for i = 1:size(allFileNames,1)
    if ~isempty(strfind(allFileNames(i,:),'JL1'))&&~isempty(strfind(allFileNames(i,:),'rmap'))
        clear tfileList tFileNumbers
        load([path,deblank(allFileNames(i,:))],'tfileList');
        % Get channel numbers of neurons.
        for iT = 1:length(tfileList)
            tFileNumbers(iT) = str2double(tfileList{iT}(4:6));
        end
        % Get the recording number
        if strcmp(allFileNames(i,9),'_')
            recNum = str2double(allFileNames(i,8));
        else
            recNum = str2double(allFileNames(i,8:9));
        end
        
        % Choose the channels in HPC for this recording.
        if recNum >= 31
            neuronList = find(tFileNumbers>=33);
            recFile = deblank(allFileNames(i,:));
            fprintf(jl1HPCRecListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
        elseif recNum >= 27
            neuronList = find(tFileNumbers>=33 & (tFileNumbers < 41 | tFileNumbers > 44));
            recFile = deblank(allFileNames(i,:));
            fprintf(jl1HPCRecListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
        elseif recNum >= 23
            neuronList = find(tFileNumbers>=33 & tFileNumbers<41);
            recFile = deblank(allFileNames(i,:));
            fprintf(jl1HPCRecListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,[writePath,recFile,'\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
            fprintf(jl1HPCNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
            
        else
            disp('Something is wrong!!')
        end
        clear recList neuronList
    end
end