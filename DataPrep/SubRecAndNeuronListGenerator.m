%% Import excel spreadsheet and convert recording drive and hookup info
%  into structs - a useable form so we can create lists of all relevant
%  neurons from it.

% % Step 1: Import data from excel sheet as cell arrays.
% % Step 2: Convert to structs.
% NS14RecDriveHookupLog = cell2struct(RatRecordingDriveAndHookupLogS1(2:end,:),RatRecordingDriveAndHookupLogS1(1,:),2);
% NS15RecDriveHookupLog = cell2struct(RatRecordingDriveAndHookupLogS2(2:end,:),RatRecordingDriveAndHookupLogS2(1,:),2);
% NS16RecDriveHookupLog = cell2struct(RatRecordingDriveAndHookupLogS3(2:end,:),RatRecordingDriveAndHookupLogS3(1,:),2);
% allRecs = [NS14RecDriveHookupLog;NS15RecDriveHookupLog;NS16RecDriveHookupLog];
% DriveToChannelMapping = cell2struct(RatRecordingDriveAndHookupLog(2:end,:),RatRecordingDriveAndHookupLog(1,:),2);


%% Hookup Locations of each recording.
ns14RSubHookup = [];
ns15LBRFRHookupOrder = [];
ns16LBRFRHookupOrder = [];
i14Recs = 0;
i15Recs = 0;
i16Recs = 0;
for iRec = 1:length(allRecs)
    if strcmp(allRecs(iRec).rat,'NS14')
        %Finding R-Sub hookup
        if strcmp(allRecs(iRec).hookupA,'R-Sub')
            i14Recs = i14Recs+1;
            ns14RSubHookup(i14Recs,:) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,1];
        end
        if strcmp(allRecs(iRec).hookupB,'R-Sub')
            i14Recs = i14Recs+1;
            ns14RSubHookup(i14Recs,:) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,2];
        end
        if strcmp(allRecs(iRec).hookupC,'R-Sub')
            i14Recs = i14Recs+1;
            ns14RSubHookup(i14Recs,:) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,3];
        end
    elseif strcmp(allRecs(iRec).rat,'NS15')
        i15Recs = i15Recs+1;
        %Finding L Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'L-Sub')
            ns15LBRFRHookupOrder(i15Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,1];
        elseif strcmp(allRecs(iRec).hookupB,'L-Sub')
            ns15LBRFRHookupOrder(i15Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,2];
        elseif strcmp(allRecs(iRec).hookupC,'L-Sub')
            ns15LBRFRHookupOrder(i15Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,3];
        end
        %Finding Back Right Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'RB-Sub')
            ns15LBRFRHookupOrder(i15Recs,4) = 1;
        elseif strcmp(allRecs(iRec).hookupB,'RB-Sub')
            ns15LBRFRHookupOrder(i15Recs,4) = 2;
        elseif strcmp(allRecs(iRec).hookupC,'RB-Sub')
            ns15LBRFRHookupOrder(i15Recs,4) = 3;
        end
        %Finding Front Right Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'RF-Sub')
            ns15LBRFRHookupOrder(i15Recs,5) = 1;
        elseif strcmp(allRecs(iRec).hookupB,'RF-Sub')
            ns15LBRFRHookupOrder(i15Recs,5) = 2;
        elseif strcmp(allRecs(iRec).hookupC,'RF-Sub')
            ns15LBRFRHookupOrder(i15Recs,5) = 3;
        end
    elseif strcmp(allRecs(iRec).rat,'NS16')
        %Finding L Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'L-Sub')
            i16Recs = i16Recs+1;
            ns16LBRFRHookupOrder(i16Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,1];
        elseif strcmp(allRecs(iRec).hookupB,'L-Sub')
            i16Recs = i16Recs+1;
            ns16LBRFRHookupOrder(i16Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,2];
        elseif strcmp(allRecs(iRec).hookupC,'L-Sub')
            i16Recs = i16Recs+1;
            ns16LBRFRHookupOrder(i16Recs,1:3) = [str2double(allRecs(iRec).rat(3:4)),allRecs(iRec).recording,3];
        end
        %Finding Back Right Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'RB-Sub')
            ns16LBRFRHookupOrder(i16Recs,4) = 1;
        elseif strcmp(allRecs(iRec).hookupB,'RB-Sub')
            ns16LBRFRHookupOrder(i16Recs,4) = 2;
        elseif strcmp(allRecs(iRec).hookupC,'RB-Sub')
            ns16LBRFRHookupOrder(i16Recs,4) = 3;
        end
        %Finding Front Right Subiculum hookup
        if strcmp(allRecs(iRec).hookupA,'RF-Sub')
            ns16LBRFRHookupOrder(i16Recs,5) = 1;
        elseif strcmp(allRecs(iRec).hookupB,'RF-Sub')
            ns16LBRFRHookupOrder(i16Recs,5) = 2;
        elseif strcmp(allRecs(iRec).hookupC,'RF-Sub')
            ns16LBRFRHookupOrder(i16Recs,5) = 3;
        end
    end
end

%% Plotting the results of above
figure;
subplot(311)
plot(ns14RSubHookup(:,3),'b')
subplot(312)
plot(ns15LBRFRHookupOrder(:,3:5))
subplot(313)
plot(ns16LBRFRHookupOrder(:,3:5))

%% Map out neurons to drives.
%NS15 & 16 canula Mapping, based on drive to channel mapping data.
clear ns15map ns16map
ns15map(:,1) = ones(16,1); %Left drive
ns15map(:,2) = [2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3]; % Back right drive
ns15map(:,3) = [4;4;4;4;3;3;3;3;4;4;4;4;4;4;4;4]; % Front right drive
ns16map(:,1) = ones(16,1); %Left drive
ns16map(:,2) = [2;2;2;2;2;2;2;2;3;3;3;3;3;3;3;3]; % Back right drive
ns16map(:,3) = [4;4;4;4;4;4;4;4;4;4;4;4;3;3;3;3]; % Front right drive
channels = cumsum(ones(48,1));

ns14RSubRecListFID = fopen('ns14RSubRecList','w');
ns14RSubNeuronListFID = fopen('ns14RSubNeuronList','w');
% ns15LSubRecListFID = fopen('ns15LSubRecList','w');
% ns15LSubNeuronListFID = fopen('ns15LSubNeuronList','w');
% ns15MBRSubRecListFID = fopen('ns15MBRSubRecList','w');
% ns15MBRSubNeuronListFID = fopen('ns15MBRSubNeuronList','w');
% ns15FMRSubRecListFID = fopen('ns15FMRSubRecList','w');
% ns15FMRSubNeuronListFID = fopen('ns15FMRSubNeuronList','w');
% ns15FLRSubRecListFID = fopen('ns15FLRSubRecList','w');
% ns15FLRSubNeuronListFID = fopen('ns15FLRSubNeuronList','w');
% ns16LSubRecListFID = fopen('ns16LSubRecList','w');
% ns16LSubNeuronListFID = fopen('ns16LSubNeuronList','w');
% ns16MBRSubRecListFID = fopen('ns16MBRSubRecList','w');
% ns16MBRSubNeuronListFID = fopen('ns16MBRSubNeuronList','w');
% ns16FMRSubRecListFID = fopen('ns16FMRSubRecList','w');
% ns16FMRSubNeuronListFID = fopen('ns16FMRSubNeuronList','w');
% ns16FLRSubRecListFID = fopen('ns16FLRSubRecList','w');
% ns16FLRSubNeuronListFID = fopen('ns16FLRSubNeuronList','w');

% Where the data currently resides - may need to change.
path = 'D:\allSubData\NS14Data\';
writePath = 'D:\\allSubData\\NS14Data\\';
allFileNames = ls(path);
% allFileNames = [allFileNames; ls('D:\allSubData\NS15Data')];
% allFileNames = [allFileNames; ls('D:\allSubData\NS16Data')];

for i = 1:size(allFileNames,1)
    if ~isempty(strfind(allFileNames(i,:),'NS14'))&&~isempty(strfind(allFileNames(i,:),'rmap'))
        clear tfileList tFileNumbers
        load([path,deblank(allFileNames(i,:))],'tfileList');
        % Get channel numbers of neurons.
        for iT = 1:length(tfileList)
            tFileNumbers(iT) = str2double(tfileList{iT}(4:6));
        end
        % Get the recording number
        if strcmp(allFileNames(i,10),'_')
            recNum = str2double(allFileNames(i,9));
        else
            recNum = str2double(allFileNames(i,9:10));
        end
        
        % Find the appropriate cannula mapping for this recording's hookup
        % configuration.
        rowToUse = find(ns14RSubHookup(:,2)==recNum);
        if ns14RSubHookup(rowToUse,3) == 3
            neuronList = find(tFileNumbers>=33);
            recFile = deblank(allFileNames(i,:));
            fprintf(ns14RSubRecListFID,[writePath,recFile,'\n']);
            fprintf(ns14RSubNeuronListFID,[writePath,recFile,'\n']);
            fprintf(ns14RSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
            fprintf(ns14RSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
            clear recList neuronList
        else
            disp('Something is wrong!!')
        end
        
    elseif ~isempty(strfind(allFileNames(i,:),'NS15'))&&~isempty(strfind(allFileNames(i,:),'rmap'))
        clear tfileList tFileNumbers
        load([path,deblank(allFileNames(i,:))],'tfileList');
        % Get channel numbers of neurons.
        for iT = 1:length(tfileList)
            tFileNumbers(iT) = str2double(tfileList{iT}(4:6));
        end
        % Get the recording number
        if strcmp(allFileNames(i,10),'_')
            recNum = str2double(allFileNames(i,9));
        else
            recNum = str2double(allFileNames(i,9:10));
        end
        
        % Find the appropriate cannula mapping for this recording's hookup
        % configuration.
        rowToUse = find(ns15LBRFRHookupOrder(:,2)==recNum);
        hookupOrder = ns15LBRFRHookupOrder(rowToUse,3:5);
        canulaMap = [ns15map(:,find(hookupOrder==1));ns15map(:,find(hookupOrder==2));ns15map(:,find(hookupOrder==3))];
        
        recFile = deblank(allFileNames(i,:));
        for iCanula = 1:4
            channelList = channels(canulaMap == iCanula);
            neuronList = [];
            for iT = 1:length(tFileNumbers)
                if any(tFileNumbers(iT) == channelList)
                    neuronList = [neuronList, iT];
                end
            end
            if ~isempty(neuronList)
                switch iCanula
                    case 1
                        fprintf(ns15LSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns15LSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns15LSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns15LSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 2
                        fprintf(ns15MBRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns15MBRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns15MBRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns15MBRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 3
                        fprintf(ns15FMRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns15FMRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns15FMRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns15FMRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 4
                        fprintf(ns15FLRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns15FLRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns15FLRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns15FLRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                end
            end
        end
        clear recList neuronList
        
        
        
        
    elseif ~isempty(strfind(allFileNames(i,:),'NS16'))&&~isempty(strfind(allFileNames(i,:),'rmap'))
        clear tfileList tFileNumbers
        load([path,deblank(allFileNames(i,:))],'tfileList');
        % Get channel numbers of neurons.
        for iT = 1:length(tfileList)
            tFileNumbers(iT) = str2double(tfileList{iT}(4:6));
        end
        % Get the recording number
        if strcmp(allFileNames(i,10),'_')
            recNum = str2double(allFileNames(i,9));
        else
            recNum = str2double(allFileNames(i,9:10));
        end
        
        % Find the appropriate cannula mapping for this recording's hookup
        % configuration.
        rowToUse = find(ns16LBRFRHookupOrder(:,2)==recNum);
        hookupOrder = ns16LBRFRHookupOrder(rowToUse,3:5);
        canulaMap = [ns16map(:,find(hookupOrder==1));ns16map(:,find(hookupOrder==2));ns16map(:,find(hookupOrder==3))];
        
        recFile = deblank(allFileNames(i,:));
        for iCanula = 1:4
            channelList = channels(canulaMap == iCanula);
            neuronList = [];
            for iT = 1:length(tFileNumbers)
                if any(tFileNumbers(iT) == channelList)
                    neuronList = [neuronList, iT];
                end
            end
            if ~isempty(neuronList)
                switch iCanula
                    case 1
                        fprintf(ns16LSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns16LSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns16LSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns16LSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 2
                        fprintf(ns16MBRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns16MBRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns16MBRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns16MBRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 3
                        fprintf(ns16FMRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns16FMRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns16FMRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns16FMRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                    case 4
                        fprintf(ns16FLRSubRecListFID,[writePath,recFile,'\n']);
                        fprintf(ns16FLRSubNeuronListFID,[writePath,recFile,'\n']);
                        fprintf(ns16FLRSubNeuronListFID,['[ ', int2str(neuronList),' ]\n']);
                        fprintf(ns16FLRSubNeuronListFID,['[ ', int2str(tFileNumbers(neuronList)),' ]\n']);
                end
            end
        end
        clear recList neuronList
    end
end
fclose('all');




