function [ outputStruct ] = fileListParser( neuronListFileName, dvtListFileName )
%FILELISTPARSER Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015

% Process Dvt List
% Spaces between letters? Make sure to delete any leading blank lines and
% then open the file in notepad, go to save as, and change the encoding to
% UTF-8. Then you can run it in this.
dvtListFID = fopen(dvtListFileName,'r');
tmp = fgetl(dvtListFID);
i = 0;
while tmp ~= -1
    if ~isempty(deblank(tmp))
        i = i+1;
        dvtFileFromList{i} = tmp;
    end
    tmp = fgetl(dvtListFID);
end
fclose(dvtListFID);

% Process Neuron/Rec List
recNeuronListFID = fopen(neuronListFileName);
nRecs = 0;
tmp = fgetl(recNeuronListFID);
i = 0;
while tmp ~= -1
    i = i+1;
    recFile{i} = tmp;
    neuronList{i} = str2num(fgetl(recNeuronListFID));
    channelList{i} = str2num(fgetl(recNeuronListFID));
    nRecs = nRecs+1;
    
    % Get the dvt file to go with the recFile
    % Recording identifier part of name is 19-20 characters long - NSXX_recXX_MMDDYY###
    % Offset is 23 characters: - D:\allSubData\NS14Data\
    % Find the dvt file with the same recording identifier.
    % NOTE 18-19 long for JL1 (shorter name) switch 1 thing on each of next
    % 3 lines.
    fileSubstring = recFile{i}(24:43); %Name is 20 characters long
    if strcmp(fileSubstring(20),'_')
        fileSubstring = fileSubstring(1:19);
    end
    dvtListIndex = -1;
    for line=1:size(dvtFileFromList,2)
        if ~isempty(strfind(dvtFileFromList{line},fileSubstring))
            if dvtListIndex==-1
                dvtListIndex = line;
            else
                disp('multiple dvt files found');
                return;
            end
        end
    end
    if dvtListIndex == -1
        disp('no dvt file found');
        return;
    end
    dvtFile{i} = dvtFileFromList{dvtListIndex};
    
    tmp = fgetl(recNeuronListFID);
end
fclose(recNeuronListFID);

outputStruct.recAndNeuronList = neuronListFileName;
outputStruct.dvtList = dvtListFileName;
outputStruct.recFile = recFile;
outputStruct.dvtFile = dvtFile;
outputStruct.neuronList = neuronList;
outputStruct.channelList = channelList;
outputStruct.nRecs = nRecs;
end

