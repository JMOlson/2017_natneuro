function [ neuronIndex ] = createNeuronIndex(DataStruct, NeuronStruct)
% createNeuronIndex - Spits out lists of neurons sorted by mutual and
% positional information.
%    This function takes in the finished info analysis data and spits out
%    one lookup table to find cells by their recording/number in the lists.
%    
%
%    [lookupTable] = INFOCELLSORTER(recList, neuronList)
%     takes two arguments, the ....TODO output from the
%    infoAnalyses analyses.
%    
%    OUTPUT: 1 matrix.
%    
%    TOUPDATE - kind of right.
%    lookupTable is a matrix of size nCells x 6. The first column is the
%    complete recording code, the second is the rat, and the third is the
%    recording number. These are useful for looking up cells by rat and
%    recording. The fourth column corresponds to the third in
%    mutualInfoList and posInfoList, and columns 5 and 6 of lookupTable
%    correspond to 2 and 3 of those lists. By utilizing a find command on
%    the values in the 5th and 6th columns in the lookup table, one can
%    match up cells from the sorted lists to the correct recording.
%
%    Sample usage for the results.
%    sortedMIbyTurn1 = sortrows(mutInfoList,-4);
%    sortedMIbyTurn2 = sortrows(mutInfoList,-5);
%    sortedMIbyTurn3 = sortrows(mutInfoList,-6);
%    sortedMIbyAllTurns = sortrows(mutInfoList,-7);
%    
%    Non-built-in functions called:
%    
%    Written by Jake Olson, October 2015
%    Last modified by Jake Olson, December 2015

% Create a list of the identifying info for the cells. (Lookup Table)
recIndex = [];
for iRec = 1:DataStruct.nRecs
recIndex = [recIndex; repmat(iRec,length(DataStruct.neuronList{iRec}),1)];
end
for iOverallNeuron = 1:length(NeuronStruct.neuronIndex)
    neuronIndex(iOverallNeuron,:) = [iOverallNeuron,...
        recIndex(iOverallNeuron),...
        str2double(NeuronStruct.rat{iOverallNeuron}(3:end)),...
        NeuronStruct.rec(iOverallNeuron),...
        NeuronStruct.neuronIndex(iOverallNeuron)];
end
end
