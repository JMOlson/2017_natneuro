%% Rotation Figure Neurons
% mapToUse = colormap('hot');
% mapToUse = mapToUse(1:180,:);
load('figureColormap.mat');
mapToUse = hotMapClipped;

% mapToUse = colormap('parula');

rotationAxisCells = union(SubPaperRotationsTrackNormalRunningCategorizedAxisCells.finalAxisLIPReliableRatio,SubPaperRotationsTrackRotatedRunningCategorizedAxisCells.finalAxisLIPReliableRatio);

sampleAxisRotation = [6,12,16,30,100,113,152,155,157,161];
iffyAxisRotation = [11,26,27,28,38,41,153,158];

selected = [6,16,100,157];

neuronsToPlot = selected;
for i = 1:length(neuronsToPlot)
maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
caxisMax = 2*overallFR(neuronsToPlot(i),2);

figure
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
hold off;
figure
sc(fullTrackNormalIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
    isnan(fullTrackNormalIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
figure
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),3,1));
hold off;
figure
sc(fullTrackRotatedIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
    isnan(fullTrackRotatedIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
disp(neuronsToPlot(i));
pause;
end

%% Platform/Normal Figure Neurons
mapToUse = colormap('hot');
mapToUse = mapToUse(1:45,:);
% mapToUse = repmat(0:.8/255:0.8,3,1)'; % Raiders Colorsse
% mapToUse = brewermap(9,'Blues');
% mapToUse = mapToUse(3:9,:);

% caxisMax = 35;
sampleAxis = [11,12,13,14,28,38,48,50,57,58,61,63,71,74,198,255,265,273,296,415,421,423,443,446,450,452,456,457,467,521,529];
iffyAxis = [3,17,22,23,27,45,66,68,69,72,422,440];

figureNeuronCandidates = [443,446,467,529];

selected = [255,467,423,11];

% neuronsToPlot = SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio;
neuronsToPlot = selected;
for i = 1:length(neuronsToPlot)
maxHDRate = max(max(squeeze(NS.hdRMapsRunning(:,neuronsToPlot(i),:,1))));
caxisMax = 2*overallFR(neuronsToPlot(i),2);
figure(101)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),2,1));
hold off;
figure(102)
sc(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
    isnan(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
% rmapImage = sc(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
%     isnan(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
% alphamap = rmapImage(fullTrackIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
% image(rmapImage,'alphaData',alphamap);
figure(103)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    NS.hdRMapsRunning(:,neuronsToPlot(i),1,1));
hold off;
figure(104)
rmapImage = sc(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1),[0,caxisMax],mapToUse,'w',...
    isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1)));
alphamap = ~isnan(fullPlatformIsRunning2DRMapsNanConv(:,:,neuronsToPlot(i),1));
image(rmapImage,'alphaData',alphamap);
disp(neuronsToPlot(i));
pause;
end