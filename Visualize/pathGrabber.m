function [ trimmedPixelDvt ] = pathGrabber(rmapFile,pathsToGrab)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
load(rmapFile);

% Additional state info.
changeStarts = [find(diff(pixelDvt(:,end)))];
changeEnds = [changeStarts(2:end);length(pixelDvt)];

wantedStarts = false(length(changeStarts),1); 
for iPathToPlot = 1:length(pathsToGrab)
    wantedStarts = (pixelDvt(changeEnds,end)==pathsToGrab(iPathToPlot))|...
        wantedStarts;
end
pathsWanted = find(wantedStarts);
changeStarts = changeStarts(pathsWanted);
changeEnds = changeEnds(pathsWanted);
trimmedPixelDvt = [];
for iSegment = 1:length(changeStarts);
    trimmedPixelDvt = [trimmedPixelDvt;...
        pixelDvt(changeStarts(iSegment):changeEnds(iSegment),:)];
end
end

