%% As one model, not components

listToShow = axisRatioLIP;
% twoDRMapsToShow = SubPaperFull2DRMapsPlatform;
NS = SubPaperNeuronStruct;
DS = SubPaperDataStruct;
hdRMapsToShow = NS.hdRMapsHalves;
iTimeSegment = 1;

for i = 1:length(listToShow)
    iRecOverall = NS.recCount(listToShow(i));
    offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180;
    
    figure(98);
    subplot(121)
    hold off;
    polar(degtorad(10:DS.binSizeDegrees:360)',...
        circshift(hdRMapsToShow(:,listToShow(i),iTimeSegment,1,1)...
        ./nansum(hdRMapsToShow(:,listToShow(i),iTimeSegment,1,1)),...
        floor(offset/DS.binSizeDegrees)));
    hold on;
    polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        circshift(modelHDVals(:,3,listToShow(i)),...
        floor(offset/DS.binSizeDegrees)),'k');
    
    subplot(122)
    hold off;
    polar(degtorad(10:DS.binSizeDegrees:360)',...
        circshift(hdRMapsToShow(:,listToShow(i),iTimeSegment,2,1)...
        ./nansum(hdRMapsToShow(:,listToShow(i),iTimeSegment,2,1)),...
        floor(offset/DS.binSizeDegrees)));
    hold on;
    polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
        circshift(modelHDVals(:,3,listToShow(i)),...
        floor(offset/DS.binSizeDegrees)),'k');
    
%     figure(99);
%     imagesc(twoDRMapsToShow(:,:,listToShow(i),1));
    
    disp(listToShow(i));
    pause;
end
clear offset iRecOverall iTimeSegment i

%% one off of the above code.
figure;
neuronToShow = 479;
polar(degtorad(10:DS.binSizeDegrees:360)',...
    NS.hdRMapsCleanRunsOnly(:,neuronToShow,2,1)./sum(NS.hdRMapsCleanRunsOnly(:,neuronToShow,2,1)));
hold on;
polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    modelHDVals(:,3,neuronToShow),'k')

%% Compare two rotations data.
listToShow = rotationAxis10;
twoDRMapsToShow1 = SubPaperRotations2DRMapsTrackCleanRunsOnly;
twoDRMapsToShow2 = SubPaperRotations2DRMapsRotationsCleanRunsOnly;

iTimeSegment = [2,3];

for i = 1:length(listToShow)
    iRecOverall = NS.recCount(listToShow(i));
    offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180;
    
    figure(98);
    hold off;
    polar(degtorad(10:DS.binSizeDegrees:360)',...
        circshift(NS.hdRMapsCleanRunsOnly(:,listToShow(i),iTimeSegment(1),1)...
        ./sum(NS.hdRMapsCleanRunsOnly(:,listToShow(i),iTimeSegment(1),1)),...
        floor(offset/DS.binSizeDegrees)));
    
    figure(100)
    hold off;
    polar(degtorad(10:DS.binSizeDegrees:360)',...
        circshift(NS.hdRMapsCleanRunsOnly(:,listToShow(i),iTimeSegment(2),1)...
        ./sum(NS.hdRMapsCleanRunsOnly(:,listToShow(i),iTimeSegment(2),1)),...
        floor(offset/DS.binSizeDegrees)));
    
    figure(99);
    imagesc(twoDRMapsToShow1(:,:,listToShow(i),1));
    
    figure(101);
    imagesc(twoDRMapsToShow2(:,:,listToShow(i),1));
        
    disp(listToShow(i));
    pause;
end
clear offset iRecOverall iTimeSegment i

%% Basic 2Dist
for i = 1:length(thetaEMMain)
    subplot(121)
    hold off;
    polar(degtorad(0:NS1.HDMapsBinSizeDegrees:350)',...
        NS1.hdRMapsCleanRunsOnly(:,i,2,1,1)./sum(NS1.hdRMapsCleanRunsOnly(:,i,2,1,1)));
    hold on;
    polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
        circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(1,3,i),mEMMain(1,3,i))*mixEMMain(1,3,i)*pi/18,'k')
    polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
        circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(2,3,i),mEMMain(2,3,i))*mixEMMain(2,3,i)*pi/18,'r')
    subplot(122)
    hold off;
    polar(degtorad(0:NS1.HDMapsBinSizeDegrees:350)',...
        NS1.hdRMapsCleanRunsOnly(:,i,2,2,1)./sum(NS1.hdRMapsCleanRunsOnly(:,i,2,2,1)));
    hold on;
    polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
        circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(1,3,i),mEMMain(1,3,i))*mixEMMain(1,3,i)*pi/18,'k')
    polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
        circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(2,3,i),mEMMain(2,3,i))*mixEMMain(2,3,i)*pi/18,'r')
    disp(i);
    pause(1.5);
end

%% 9 Dist
figure;
subplot(121)
hold off;
polar(degtorad(0:NS1.HDMapsBinSizeDegrees:350)',...
    NS1.hdRMapsCleanRunsOnly(:,i,2,1,1)./sum(NS1.hdRMapsCleanRunsOnly(:,i,2,1,1)));
hold on;
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(1,9,i),mEMMain(1,9,i))*mixEMMain(1,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(2,9,i),mEMMain(2,9,i))*mixEMMain(2,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(3,9,i),mEMMain(3,9,i))*mixEMMain(3,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(4,9,i),mEMMain(4,9,i))*mixEMMain(4,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(5,9,i),mEMMain(5,9,i))*mixEMMain(5,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(6,9,i),mEMMain(6,9,i))*mixEMMain(6,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(7,9,i),mEMMain(7,9,i))*mixEMMain(7,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(8,9,i),mEMMain(8,9,i))*mixEMMain(8,9,i)*pi/18,'k')

subplot(122)
hold off;
polar(degtorad(0:NS1.HDMapsBinSizeDegrees:350)',...
    NS1.hdRMapsCleanRunsOnly(:,i,2,2,1)./sum(NS1.hdRMapsCleanRunsOnly(:,i,2,2,1)));
hold on;
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(1,9,i),mEMMain(1,9,i))*mixEMMain(1,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(2,9,i),mEMMain(2,9,i))*mixEMMain(2,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(3,9,i),mEMMain(3,9,i))*mixEMMain(3,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(4,9,i),mEMMain(4,9,i))*mixEMMain(4,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(5,9,i),mEMMain(5,9,i))*mixEMMain(5,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(6,9,i),mEMMain(6,9,i))*mixEMMain(6,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(7,9,i),mEMMain(7,9,i))*mixEMMain(7,9,i)*pi/18,'k')
polar(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',...
    circ_vmpdf(degtorad(NS1.HDMapsBinSizeDegrees:NS1.HDMapsBinSizeDegrees:360)',thetaEMMain(8,9,i),mEMMain(8,9,i))*mixEMMain(8,9,i)*pi/18,'k')
disp(i);
