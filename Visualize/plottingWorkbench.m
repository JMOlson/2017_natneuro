%% Plotting Workbench


%% HD Reliability (Sparsity) Analysis Plotting
%         hdPathOnlySparsityRates(maxInd(iOverallNeuron),iOverallNeuron,2)
%         hdPathOnlySparsityAllOrNone(maxInd(iOverallNeuron),iOverallNeuron,2)
%         figure(9);
%         plot(hdPathOnlySparsityRates(:,iOverallNeuron,2));
%         disp(['Max angle: ', maxInd(iOverallNeuron)]);


%% RUN CORRELATIONS: Pairwise correlations lumped together for a histogram - not very useful.
% An average of each pair for all neurons is the last line - more useful.
toPlotCorr = [squeeze(NS.corrValsRuns1stTurnOn(1,2,:)),...
    squeeze(NS.corrValsRuns1stTurnOn(3,4,:)),...
    squeeze(NS.corrValsRuns1stTurnOn(1,4,:)),...
    squeeze(NS.corrValsRuns1stTurnOn(2,3,:)),...
    squeeze(NS.corrValsRuns1stTurnOn(1,3,:)),...
    squeeze(NS.corrValsRuns1stTurnOn(2,4,:)),...
    squeeze(NS.corrValsReturns(1,2,:))];
figure;
hist(mean(toPlotCorr,2))
nanmean(toPlotCorr)

%% POPULATION RUN CORRELATION MATRICES - Create Vars.
FRMat1 = squeeze(NS.MeanLinearRatesCompiled{1}(:,:,1));
FRMat2 = squeeze(NS.MeanLinearRatesCompiled{2}(:,:,1));
FRMat3 = squeeze(NS.MeanLinearRatesCompiled{3}(:,:,1));
FRMat4 = squeeze(NS.MeanLinearRatesCompiled{4}(:,:,1));
FRMat5 = squeeze(NS.MeanLinearRatesCompiled{9}(:,:,1));
FRMat6 = squeeze(NS.MeanLinearRatesCompiled{10}(:,:,1));
FRMat1R = squeeze(NS.MeanLinearRatesCompiled{11}(:,:,1));
FRMat2R = squeeze(NS.MeanLinearRatesCompiled{12}(:,:,1));
FRMat3R = squeeze(NS.MeanLinearRatesCompiled{13}(:,:,1));
FRMat4R = squeeze(NS.MeanLinearRatesCompiled{14}(:,:,1));
FRMat5R = squeeze(NS.MeanLinearRatesCompiled{19}(:,:,1));
FRMat6R = squeeze(NS.MeanLinearRatesCompiled{20}(:,:,1));
%% Standard Corr Matrix Plots
figure(1001)
imagesc(corr(FRMat1'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1011)
imagesc(corr(FRMat1',FRMat2'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1012)
imagesc(corr(FRMat2'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1021)
imagesc(corr(FRMat1',FRMat3'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1022)
imagesc(corr(FRMat2',FRMat3'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1023)
imagesc(corr(FRMat3'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1031)
imagesc(corr(FRMat1',FRMat4'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1032)
imagesc(corr(FRMat2',FRMat4'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1033)
imagesc(corr(FRMat3',FRMat4'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1034)
imagesc(corr(FRMat4'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1045)
imagesc(corr(FRMat5'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1055)
imagesc(corr(FRMat5',FRMat6'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(1056)
imagesc(corr(FRMat6'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
%% Rotation Corr Matrix Plots
figure(2001)
imagesc(corr(FRMat1R'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2011)
imagesc(corr(FRMat1R',FRMat2R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2012)
imagesc(corr(FRMat2R'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2021)
imagesc(corr(FRMat1R',FRMat3R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2022)
imagesc(corr(FRMat2R',FRMat3R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2023)
imagesc(corr(FRMat3R'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2031)
imagesc(corr(FRMat1R',FRMat4R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2032)
imagesc(corr(FRMat2R',FRMat4R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2033)
imagesc(corr(FRMat3R',FRMat4R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2034)
imagesc(corr(FRMat4R'));
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2045)
imagesc(corr(FRMat5R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2055)
imagesc(corr(FRMat5R',FRMat6R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])
figure(2056)
imagesc(corr(FRMat6R'),[0.5,1])
colormap('parula')
set(gca,'xtick',[],'ytick',[0.5,1])


%% Looking at the population 2D spatial measures.
% figure();
for iPath = 1:6
%     goodSpotsPath = pathSpatialCoherence(:,iPath)~=0 &...
%         ~isnan(pathSpatialCoherence(:,iPath));
%     subplot(3,6,iPath)
%     scatter(pathSpatialCoherence(goodSpotsPath,iPath),pathSpatialInfoInBits(goodSpotsPath,iPath),...
%         markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
%     xlabel('pathSpatialCoherence')
%     ylabel('pathSpatialInfoInBits')
%     title(['Path' num2str(iPath)])
%     subplot(3,6,iPath+6)
%     scatter(pathSpatialCoherence(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),...
%         markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
%     xlabel('pathSpatialCoherence')
%     ylabel('pathSpatialSparsity')
%     subplot(3,6,iPath+12)
%     scatter(pathSpatialInfoInBits(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),...
%     markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
%     xlabel('pathSpatialInfoInBits')
%     ylabel('pathSpatialSparsity')
% 
% %     
% %     subplot(6,6,6*(iPath-1)+4)
% %     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialInfoInBits(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath))
% %     xlabel('pathMeanFiringRate')
% %     ylabel('pathSpatialInfoInBits')
% %     subplot(6,6,6*(iPath-1)+5)
% %     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath))
% %     xlabel('pathMeanFiringRate')
% %     ylabel('pathSpatialSparsity')
% %     subplot(6,6,6*(iPath-1)+6)
% %     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialCoherence(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath),'fill')
% %     xlabel('pathMeanFiringRate')
% %     ylabel('pathSpatialCoherence')
end
% suptitle('All neurons')

figure();
for iPath = 1:6
    goodSpotsPath = pathSpatialCoherence(:,iPath)~=0 &...
        ~isnan(pathSpatialCoherence(:,iPath)) &...
        ~noFire';
    subplot(3,6,iPath)
    scatter(pathSpatialCoherence(goodSpotsPath,iPath),pathSpatialInfoInBits(goodSpotsPath,iPath),...
        markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
    xlabel('pathSpatialCoherence')
    ylabel('pathSpatialInfoInBits')
    title(['Path' num2str(iPath)])
    axis([0.9 1 0 10]);
    subplot(3,6,iPath+6)
    scatter(pathSpatialCoherence(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),...
        markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
    xlabel('pathSpatialCoherence')
    ylabel('pathSpatialSparsity')
    axis([0.9 1 0 1]);
    subplot(3,6,iPath+12)
    scatter(pathSpatialInfoInBits(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),...
    markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
    xlabel('pathSpatialInfoInBits')
    ylabel('pathSpatialSparsity')  
    axis([0 10 0 1]);
%     
%     subplot(6,6,6*(iPath-1)+4)
%     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialInfoInBits(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath))
%     xlabel('pathMeanFiringRate')
%     ylabel('pathSpatialInfoInBits')
%     subplot(6,6,6*(iPath-1)+5)
%     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialSparsity(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath))
%     xlabel('pathMeanFiringRate')
%     ylabel('pathSpatialSparsity')
%     subplot(6,6,6*(iPath-1)+6)
%     scatter(pathMeanFiringRate(goodSpotsPath,iPath),pathSpatialCoherence(goodSpotsPath,iPath),markerSize,pathMeanFiringRate(goodSpotsPath,iPath),'fill')
%     xlabel('pathMeanFiringRate')
%     ylabel('pathSpatialCoherence')
end
suptitle('Only neurons with maxfiringrates > 5 on either platform or track')

% Just zooming in on a few figures.
% figure();
 for iPath = 1:6
%         goodSpotsPath = pathSpatialCoherence(:,iPath)~=0 &...
%         ~isnan(pathSpatialCoherence(:,iPath)) &...
%         ~noFire';
%     subplot(1,6,iPath)
%     scatter(pathSpatialCoherence(goodSpotsPath,iPath),pathSpatialInfoInBits(goodSpotsPath,iPath),...
%         markerSize,log(pathMeanFiringRate(goodSpotsPath,iPath)),'fill')
%     xlabel('pathSpatialCoherence')
%     ylabel('pathSpatialInfoInBits')
%     title(['Path' num2str(iPath)])
 end

%% whole track spatial numbers
figure();
subplot(1,6,1)
scatter(trackSpatialCoherence(goodSpots),trackSpatialInfoInBits(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('trackSpatialCoherence')
ylabel('trackSpatialInfoInBits')
axis([0.9 1 0 10]);

subplot(1,6,2)
scatter(trackSpatialCoherence(goodSpots),trackSpatialSparsity(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('trackSpatialCoherence')
ylabel('trackSpatialSparsity')
axis([0.9 1 0 1]);

subplot(1,6,3)
scatter(trackSpatialInfoInBits(goodSpots),trackSpatialSparsity(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('trackSpatialInfoInBits')
ylabel('trackSpatialSparsity')
axis([0 10 0 1]);

subplot(1,6,4)
scatter(pathNFieldsStandard(goodSpots),trackSpatialCoherence(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialCoherence')
axis([0 25 0.9 1]);

subplot(1,6,5)
scatter(pathNFieldsStandard(goodSpots),trackSpatialSparsity(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialSparsity')
axis([0 25 0 1]);

subplot(1,6,6)
scatter(pathNFieldsStandard(goodSpots),trackSpatialInfoInBits(goodSpots),...
    markerSize,log(trackMeanFiringRate(goodSpots)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialInfoInBits')
axis([0 25 0 10]);

%% Sparse vs Not Sparse
figure();
% Sparse neurons
subplot(2,6,1)
scatter(trackSpatialCoherence(sparse),trackSpatialInfoInBits(sparse),...
    markerSize,log(trackMeanFiringRate(sparse)),'fill')
xlabel('trackSpatialCoherence')
ylabel('trackSpatialInfoInBits')
axis([0.9 1 0 10]);
title('Sparsity < 0.4 - sparse')
% subplot(2,6,2)
% scatter(trackSpatialCoherence(sparse),trackSpatialSparsity(sparse),...
%     markerSize,log(trackMeanFiringRate(sparse)),'fill')
% xlabel('trackSpatialCoherence')
% ylabel('trackSpatialSparsity')
% axis([0.9 1 0 1]);
% title('Sparsity < 0.4 - sparse')
% subplot(2,6,3)
% scatter(trackSpatialInfoInBits(sparse),trackSpatialSparsity(sparse),...
%     markerSize,log(trackMeanFiringRate(sparse)),'fill')
% xlabel('trackSpatialInfoInBits')
% ylabel('trackSpatialSparsity')
% axis([0 10 0 1]);
% title('Sparsity < 0.4 - sparse')
subplot(2,6,4)
scatter(pathNFieldsStandard(sparse),trackSpatialCoherence(sparse),...
    markerSize,log(trackMeanFiringRate(sparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialCoherence')
axis([0 25 0.9 1]);
title('Sparsity < 0.4 - sparse')
subplot(2,6,5)
scatter(pathNFieldsStandard(sparse),trackSpatialSparsity(sparse),...
    markerSize,log(trackMeanFiringRate(sparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialSparsity')
axis([0 25 0 1]);
title('Sparsity < 0.4 - sparse')
subplot(2,6,6)
scatter(pathNFieldsStandard(sparse),trackSpatialInfoInBits(sparse),...
    markerSize,log(trackMeanFiringRate(sparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialInfoInBits')
axis([0 25 0 10]);
title('Sparsity < 0.4 - sparse')

% Not Sparse Neurons Plot
subplot(2,6,7)
scatter(trackSpatialCoherence(notSparse),trackSpatialInfoInBits(notSparse),...
    markerSize,log(trackMeanFiringRate(notSparse)),'fill')
xlabel('trackSpatialCoherence')
ylabel('trackSpatialInfoInBits')
axis([0.9 1 0 10]);
title('Sparsity > 0.4 - Not sparse')
% subplot(2,6,8)
% scatter(trackSpatialCoherence(notSparse),trackSpatialSparsity(notSparse),...
%     markerSize,log(trackMeanFiringRate(notSparse)),'fill')
% xlabel('trackSpatialCoherence')
% ylabel('trackSpatialSparsity')
% axis([0.9 1 0 1]);
% title('Sparsity > 0.4 - Not sparse')
% subplot(2,6,9)
% scatter(trackSpatialInfoInBits(notSparse),trackSpatialSparsity(notSparse),...
%     markerSize,log(trackMeanFiringRate(notSparse)),'fill')
% xlabel('trackSpatialInfoInBits')
% ylabel('trackSpatialSparsity')
% axis([0 10 0 1]);
title('Sparsity > 0.4 - Not sparse')
subplot(2,6,10)
scatter(pathNFieldsStandard(notSparse),trackSpatialCoherence(notSparse),...
    markerSize,log(trackMeanFiringRate(notSparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialCoherence')
axis([0 25 0.9 1]);
title('Sparsity > 0.4 - Not sparse')
subplot(2,6,11)
scatter(pathNFieldsStandard(notSparse),trackSpatialSparsity(notSparse),...
    markerSize,log(trackMeanFiringRate(notSparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialSparsity')
axis([0 25 0 1]);
title('Sparsity > 0.4 - Not sparse')
subplot(2,6,12)
scatter(pathNFieldsStandard(notSparse),trackSpatialInfoInBits(notSparse),...
    markerSize,log(trackMeanFiringRate(notSparse)),'fill')
xlabel('pathNFieldsStandard - sum all paths')
ylabel('trackSpatialInfoInBits')
axis([0 25 0 10]);
title('Sparsity > 0.4 - Not sparse')

%% hist of firing rates
figure();
subplot(421)
hist(platformMeanFiringRate,0:1:50)
title('Mean FR Platform - All')
subplot(423)
hist(trackMeanFiringRate,0:1:50)
title('Mean FR Track - All')
subplot(425)
hist(platformMaxFiringRate,0:1:100)
title('Max FR Platform - All')
subplot(427)
hist(trackMaxFiringRate,0:1:100)
title('Max FR Track - All')

subplot(422)
hist(platformMeanFiringRate,0:1:50)
title('Mean FR Platform - Included')
subplot(424)
hist(trackMeanFiringRate,0:1:50)
title('Mean FR Track - Included')
subplot(426)
hist(platformMaxFiringRate,0:1:100)
title('Max FR Platform - Included')
subplot(428)
hist(trackMaxFiringRate,0:1:100)
title('Max FR Track - Included')

%% Corr of spatial measures across paths
corrSpCoh = corr(pathSpatialCoherence,'rows','complete','type','Spearman');
corrSpSpar = corr(pathSpatialSparsity,'rows','complete','type','Spearman');
corrSpInfo = corr(pathSpatialInfoInBits,'rows','complete','type','Spearman');

figure()
subplot(131)
imagesc(corrSpCoh,[0:1]);
subplot(132)
imagesc(corrSpSpar,[0:1]);
subplot(133)
imagesc(corrSpInfo,[0:1]);

%% 
