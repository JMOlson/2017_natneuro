%% Select your dataset.
% Full
DS = SubPaperDataStruct;
NS = SubPaperNeuronStruct;
% twoDPlatform = SubPaperFull2DRMapsPlatform;
% % twoDTrack = SubPaperFull2DRMapsTrack;
% twoDTrack = SubPaperFull2DRMapsTrackCleanRunsOnly;
% twoDPlatform = platformIsRunning2DRMaps;
% twoDTrack = trackIsRunning2DRMaps;


% % Rotations
% DS = SubPaperRotationsDataStruct;
% NS = SubPaperRotationsNeuronStruct;
% twoDPlatform = platformRunning2DRMaps;
% twoDTrack = trackNormalRunning2DRMaps;
% twoDTrackRotated = trackRotatedRunning2DRMaps;

% CA1
% DS = JL1HPCDataStruct;
% NS = JL1HPCNeuronStruct;

%% General useful variables
trackMaxFR = reshape(max(max(twoDClean(:,:,:,1))),size(NS.platformMaxFR));
trackMeanFR = reshape(nanmean(nanmean(twoDClean(:,:,:,1))),size(NS.platformMeanFR));
noFire = NS.platformMaxFR < 5 & trackMaxFR < 5;
noFireTrack = trackMaxFR < 5;
noFirePlatform = NS.platformMaxFR < 5;
% sparse = ~noFire & NS.trackSpatialSparsity < 0.4;
% notSparse = ~noFire & NS.trackSpatialSparsity > 0.4;

% neuronIndex: NeuronNumberDataSet, RecNumberDataset, Rat, Rec, NeuronIndexInRec;
neuronIndex = createNeuronIndex(DS, NS);
nTotNeurons = length(neuronIndex);

%% Snippet to run to sort individual neurons based on different stats.
% [~,sortTrackSparsityInd] = sort(NS.trackSpatialSparsity,'descend');
% [~,sortTrackCoherenceInd] = sort(NS.trackSpatialCoherence,'descend'); 
% [~,sortTrackInfoInBitsInd] = sort(NS.trackSpatialInfoInBits,'descend'); 
[~,sortPlatformSparsityInd] = sort(NS.platformSpatialSparsity,'descend');
[~,sortPlatformCoherenceInd] = sort(NS.platformSpatialCoherence,'descend'); 
[~,sortPlatformInfoInBitsInd] = sort(NS.platformSpatialInfoInBits,'descend');
% [~,sortTrackHDReliabilityCleanRunsOnlyInd] = sort(max(NS.hdReliabilitySparsityCalcCleanRunsOnly(:,:,2),[],1),'descend');


% percentileTrackSpatialSparsity(sortTrackSparsityInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
% percentileTrackSpatialCoherence(sortTrackCoherenceInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
% percentileTrackSpatialInfoInBits(sortTrackInfoInBitsInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
percentilePlatformSpatialSparsity(sortPlatformSparsityInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
percentilePlatformSpatialCoherence(sortPlatformCoherenceInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
percentilePlatformSpatialInfoInBits(sortPlatformInfoInBitsInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);
% percentileTrackHDReliabilityCleanRunsOnly(sortTrackHDReliabilityCleanRunsOnlyInd) = round(100*(nTotNeurons:-1:1)/nTotNeurons);

% [~,sortPValTrack] = sort(pValHDoTest,'ascend');
% [~,sortPValTrackAxis] = sort(pValHDAxisrTest,'ascend');

%% Cell Looper portion - uses ONECELLALLPLOTS
ourNeuronList = SubPaperMainCategorizedAxisCells.finalAxisLIPReliableRatio(end:-1:1); % Change this to change neuron order
% [maxVal, maxInd] = max(hdCrossCorr,[],1);
for iSorted = 1:length(ourNeuronList)
    iOverallNeuron = ourNeuronList(iSorted);
%     if uniformP(iOverallNeuron) >= 0.05
%     if ~noFireTrack(iOverallNeuron) && sigPosAt1802(iOverallNeuron,2) && sigNegAt90(iOverallNeuron,2)
        %     if ~noFireTrack(iOverallNeuron) & maxVal(iOverallNeuron) > 0.4 &&...
        %             hdCorr(37,iOverallNeuron) > .25 && hdCorrPlatform(37,iOverallNeuron) > .25
        %     if ~noFireTrack(iOverallNeuron) && ~isnan(trackSpatialSparsity(iOverallNeuron))
        %     if ~noFireTrack(iOverallNeuron) && hdPathOnlySparsityRates(maxInd(iOverallNeuron),iOverallNeuron,2) > 0.3
        display(iSorted);
        warning off;
        %         display(hdCorr(37,iOverallNeuron));
        %         figure(10);
        %         plot(hdCrossCorr(:,iOverallNeuron));
        oneCellAllPlots;
%         figure(10);plot(hdCorrTrackMain(:,iOverallNeuron));
        pause;
%     end
end
