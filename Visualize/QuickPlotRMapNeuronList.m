recNeuronListFID = fopen('ns14RSubNeuronList');
nRecs = 0;
while fgetl(recNeuronListFID) ~= -1
    fgetl(recNeuronListFID);
    fgetl(recNeuronListFID);
    nRecs = nRecs+1;
end
fclose(recNeuronListFID);
recNeuronListFID = fopen('ns14RSubNeuronList');

% % To skip recordings:
for i=1:8
    fgetl(recNeuronListFID);
    fgetl(recNeuronListFID);
    fgetl(recNeuronListFID);
end
    
for iRec = 1:nRecs

    dataFile = fgetl(recNeuronListFID);
    recList{iRec} = dataFile;
    subNeuronInds = str2num(fgetl(recNeuronListFID));
    recNeuronList{iRec} = subNeuronInds;
    load(dataFile);
    wire = str2num(fgetl(recNeuronListFID));
    
    for iCell = 1:length(subNeuronInds)
        actCell = subNeuronInds(iCell);
        disp(dataFile);
        disp(['Cell ', num2str(actCell)]);
        disp(['Wire ',num2str(wire(iCell))]);
        
        whichPathPlotMapping = [1,2,3,4,0,0,0,0,5,6,7,8,9,10,0,0,0,0,11,12];
        for iPath = 1:nPaths
            maxRate(iPath) = max(MeanLinearRates{iPath}(actCell,:,1));
            nClicks(iPath) = size(Clicks{iPath},1)-1; %cumSumNBins ignores 1st click.
        end
        maxY = max(max(maxRate),40);
        
        figure(1);
        clf;
        for iPath = 1:nPaths
            whichPath = pathList(iPath);
            if find([1:4,9:14,19:20]==whichPath) %Only plot these runs.
                
                subplot(6,2,whichPathPlotMapping(whichPath)); % Might change 3 to 6.
                bar(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(actCell,:,1));
                hold on;
                plot(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(actCell,:,2),'g');
                if find([1:4,11:14]==whichPath)
                    nClicksToPlot = 3;
                    whichClicks = [3,5,7];
                else
                    nClicksToPlot = 2;
                    whichClicks = [2,4];
                end
                for iClick = 1:nClicksToPlot
                    plot([cumSumNBins{iPath}(whichClicks(iClick)-1), cumSumNBins{iPath}(whichClicks(iClick)-1)],[0, maxY+1],'r')
                end
                axis([0 length(MeanLinearRates{iPath}) 0 maxY+1])
            end
        end
        
        figure(2);
        clf;
        bar(pathList,nRuns,'k');
        set(gca,'Xtick',pathList,'XTickLabel',{'LRL','LRR','RLL','RLR','LL','RR'});
        
%         figure(3);
%         clf;
%         nTimeSegments = size(timeSegments,1);
%         maxHDRate = max(max(squeeze(hdRateMaps(:,iCell,:,1))));
%         for iTimeSegment = 1:nTimeSegments
%             subplot(1, nTimeSegments, iTimeSegment)
%             % Code to control axis size.
%             t = 0 : .01 : 2 * pi;
%             P = polar(t, maxHDRate * ones(size(t)));
%             set(P, 'Visible', 'off')
%             
%             %Our HD is normally about 60 degrees out of phase with our 2D ratemaps, so
%             %I am shifting the values that much in the polar plot.
%             hold on;
%             polar(degtorad(binSizeDegrees:binSizeDegrees:360)',...
%                 circshift(squeeze(hdRateMaps(:,iCell,iTimeSegment,1)),-floor(60/binSizeDegrees)));
%         end
        
        figure(4)
        imagesc(squeeze(TwoDRateMaps(actCell,:,:,1)));
        colorbar;
        
        figure(5)
        imagesc(squeeze(TwoDRateMaps(actCell,:,:,3)));
        caxis([0 20])
        
        clear nCells actCell iTimeSegment nTimeSegments t P maxHDRate
        clear iCell iPath maxY iClick maxRate nClicks whichPath whichClicks nClicksToPlot whichPathPlotMapping
        
        pause;
    end
end
