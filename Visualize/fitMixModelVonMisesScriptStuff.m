%% Run all neurons all versions 1-4 mix of vonmises
for i = 1:170
[loglikelihood, mix, angles, concentration] = fitMixOfVonMisesEMAlgorithm(NS.hdRMapsCleanRunsOnly(:,i,2,1),1,50);
loglikeMat(i,1) = loglikelihood(end);
mix1(i) = mix;
angles1(i) = angles;
concentration1(i) = concentration;
[loglikelihood, mix, angles, concentration] = fitMixOfVonMisesEMAlgorithm(NS.hdRMapsCleanRunsOnly(:,i,2,1),2,50);
loglikeMat(i,2) = loglikelihood(end);
mix2(i,:) = mix;
angles2(i,:) = angles;
concentration2(i,:) = concentration;
[loglikelihood, mix, angles, concentration] = fitMixOfVonMisesEMAlgorithm(NS.hdRMapsCleanRunsOnly(:,i,2,1),3,50);
loglikeMat(i,3) = loglikelihood(end);
mix3(i,:) = mix;
angles3(i,:) = angles;
concentration3(i,:) = concentration;
[loglikelihood, mix, angles, concentration] = fitMixOfVonMisesEMAlgorithm(NS.hdRMapsCleanRunsOnly(:,i,2,1),4,50);
loglikeMat(i,4) = loglikelihood(end);
mix4(i,:) = mix;
angles4(i,:) = angles;
concentration4(i,:) = concentration;
disp(i);
end
%% Stupid VonMisesFit Plotting
for i = 1:170
figure(1);plot(diff(loglikeMat(i,:)));
figure(2);
hold off;
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
NS.hdRMapsCleanRunsOnly(:,i,2,1),'k');
hold on;
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles1(i,1),concentration1(i,1))*40,'r')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles2(i,1),concentration2(i,1))*40*mix2(i,1),'b')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles2(i,2),concentration2(i,2))*40*mix2(i,2),'b')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles3(i,1),concentration3(i,1))*40*mix3(i,1),'c')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles3(i,2),concentration3(i,2))*40*mix3(i,2),'c')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles3(i,3),concentration3(i,3))*40*mix3(i,3),'c')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles4(i,1),concentration4(i,1))*40*mix4(i,1),'g')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles4(i,2),concentration4(i,2))*40*mix4(i,2),'g')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles4(i,3),concentration4(i,3))*40*mix4(i,3),'g')
polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',angles4(i,4),concentration4(i,4))*40*mix4(i,4),'g')
pause;
end