%ONECELLALLPLOTS plots multiple plots for one cell at a time to provide the
%gist of cell activity. Utilized by visualization plots.
%
% Created by Jake Olson Fall 2014.
% Last update by Jake Olson December 2015

%% Parameters to tweak and plot style settings.
colormapVar = brewermap(9,'*PuBu');
colormapVar = colormap('jet');
% colormapVar = colormapVar(end:-1:1,:);
colormapVar = [ones(63,1),(0:1/62:1)',zeros(63,1)];
colormapVar = [1,1,1;colormapVar];
colormap2 = othercolor('BuOrR_14');
% colormapVar = colormapVar([15:-2:9,8:-1:1],:);
% colormapVar = parula;
% colormapVar = [0:1/255:1;0:1/255:1;1:-1/255:0]';

markerSize = 12;
colorScaleMin = 0.0;
colorScaleMax = 0.6;

lineColors = distinguishable_colors(6);
lineColors(9:10,:) = lineColors(5:6,:);
lineColors(11:20,:) = lineColors;

set(0,'defaultLineLineWidth',2.25) % Sets the matlab default line width to 2.
set(0,'defaultAxesFontSize',10) % Sets the matlab default.
set(0,'DefaultAxesColorOrder',lineColors);
set(0,'DefaultFigureColormap',colormapVar)
turnIndices = [51,51;87,87;118,118]';
pathLabels = ['LRL';'LRR';'RLL';'RLR';'   ';'   ';'   ';'   ';'LL ';'RR '];
pathLabels(11:20,:) = pathLabels; 

iRecOverall = neuronIndex(iOverallNeuron,2);
% iOverallNeuron is the index of the neuron.
rat = neuronIndex(iOverallNeuron,3);
rec = neuronIndex(iOverallNeuron,4);
neuron = neuronIndex(iOverallNeuron,5);
pathList = DS.pathList{iRecOverall};
nPaths = length(pathList);

% Determine y axis limit for 2D plots.
imageSCMax = max(max(max(squeeze(twoDClean(:,:,iOverallNeuron,:,1)))));

%% FIGURE 1: HISTOGRAM - # of runs to each path
figure(1);
clf;
for i = 1:nPaths
    if any(pathList(i) == [9,10,19,20])
        bar(pathList(i)-3,DS.nRunsEachPath{iRecOverall}(i),'FaceColor',lineColors(pathList(i),:));
    else
        bar(pathList(i),DS.nRunsEachPath{iRecOverall}(i),'FaceColor',lineColors(pathList(i),:));
    end
    hold on;
end
title('Run Count per path');
% set(gca,'XTickLabel',num2str([pathList(pathList<=10)';pathList(pathList>10)']));

%% FIGURE 2: BAR - mean/max firing rates for platform/track
figure(2);
clf;
bar([NS.platformMeanFR(iOverallNeuron),trackMeanFR(iOverallNeuron);...
    NS.platformMaxFR(iOverallNeuron),trackMaxFR(iOverallNeuron)]);
ylabel('Firing Rates (Hz)')
set(gca,'XTickLabel',['mean';'max ']);
legend('Platform','Track');

%% FIGURE 3: LINE SUBPLOTS - mean linear firing rates & std deviations
% figure(3);
% clf;
% clear maxFiring
% if size(NS.hdRMaps,3) == 3 % Rotation Data
%     nPlotRows = 6;
%     nPathsUsed = nPaths;
%     nPathsToCheck = 20;
% else
%     nPlotRows = 3;
%     nPathsUsed = min(nPaths,6);
%     nPathsToCheck = 10;
% end
% for iPath = 1:nPathsToCheck
%     if ~isempty(NS.MeanLinearRatesCompiled{iPath})
%         maxFiring(iPath) =  max(NS.MeanLinearRatesCompiled{iPath}(:,iOverallNeuron,1));
%     else
%         maxFiring(iPath) = 0;
%     end
% end
% %Scales each plot to the firing of the individual cell.
% maxFiring = ceil(max(maxFiring));
% for iPath = 1:nPathsUsed
%     hold on;
%     pathIndex = pathList(iPath);
%     rightSubplot = [1,2,3,4,0,0,0,0,5,6,7,8,9,10,0,0,0,0,11,12];
%     pathLength = size(NS.MeanLinearRatesCompiled{pathIndex},1);
%     subplot(nPlotRows,2,rightSubplot(pathIndex))
%     if any(isnan(NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,3)))
%         plot(NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,1),'k');
%     else     
%     boundedline(1:pathLength,NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,1),...
%         NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,3),'k');
%     end
%     if any(rightSubplot(pathIndex) == [5,6,11,12])
%     plot([15,15;127,127]',repmat([0;maxFiring],1,2),'r','LineWidth',2);
%     else
%     plot(turnIndices,repmat([0;maxFiring],1,3),'r','LineWidth',2);
%     end
%     xlabel(pathLabels(pathIndex,:));
%     axis([0,pathLength,0,max(maxFiring,1)]);
%     set(gca,'xtick',[],'ytick',[maxFiring])
% end
% suptitle(['rat ',num2str(rat),' recording ', num2str(rec), ' cell ',...
%     int2str(neuron), 'Firing Rates and SEM by path']);

%% FIGURE 8: OVERLAYED LINES - mean linear firing rates & std deviations
% figure(8)
% clf;
% lineflag(1:4) = false;
% if size(NS.hdRMaps,4) == 3 % Rotation Data
%     nPlotRows = 2;
% else
%     nPlotRows = 1;
%     nPathsUsed = min(nPaths,6);
% end
% for iPath = 1:nPathsUsed
%     hold on;
%     pathIndex = pathList(iPath);
%     rightSubplot = [1,1,1,1,0,0,0,0,2,2,3,3,3,3,0,0,0,0,4,4];
%     pathLength = size(NS.MeanLinearRatesCompiled{pathIndex},1);
%     subplot(nPlotRows,2,rightSubplot(pathIndex))
%     if any(isnan(NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,3)))
%         plot(NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,1),'color',lineColors(pathIndex,:));
%     else
%         [a,~] = boundedline(1:pathLength,NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,1),...
%             NS.MeanLinearRatesCompiled{pathIndex}(:,iOverallNeuron,3));
%         set(a,'color',lineColors(pathIndex,:));
%     end
%     if any(rightSubplot(pathIndex) == [2,2,4,4]) && ~lineflag(rightSubplot(pathIndex))
%         plot([15,15;127,127]',repmat([0;maxFiring],1,2),'r','LineWidth',2);
%         lineflag(rightSubplot(pathIndex)) = true;
%     elseif ~lineflag(rightSubplot(pathIndex))
%         plot(turnIndices,repmat([0;maxFiring],1,3),'r','LineWidth',2);
%         lineflag(rightSubplot(pathIndex)) = true;
%     end
%     axis([0,pathLength,0,max(maxFiring,1)]);
%     set(gca,'xtick',[],'ytick',[maxFiring])
% end

%% FIGURE 4: COLORMAP - 2D & HD RMaps of each time segment
% Our HD is normally about -60 degrees out of phase with our 2D ratemaps for 
% NS 15 and 16 and 180 degrees for NS14.

offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180; % Transform to get calculations in line with plots.
maxHDRate = max(max(squeeze(NS.hdRMaps(:,iOverallNeuron,:,1))));
if size(NS.hdRMaps,3) == 3 % Rotation Data
    nPlotCols = 3;
else
    nPlotCols = 2;
end
figure(4);
clf;
subplot(2,nPlotCols,1)
imagesc(twoDPlatform(:,:,iOverallNeuron,1,1));
caxis([colorScaleMin*imageSCMax,colorScaleMax*imageSCMax]);
subplot(2,nPlotCols,nPlotCols+1)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMaps(:,iOverallNeuron,1,1),floor(offset/DS.binSizeDegrees)));
subplot(2,nPlotCols,2)
imagesc(twoDTrack(:,:,iOverallNeuron,1));
caxis([colorScaleMin*imageSCMax,colorScaleMax*imageSCMax]);
subplot(2,nPlotCols,nPlotCols+2)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMaps(:,iOverallNeuron,2,1),floor(offset/DS.binSizeDegrees)));
if size(NS.hdRMaps,3) == 3 % Rotation Data
subplot(2,nPlotCols,3)
imagesc(twoDRotation(:,:,iOverallNeuron,1));
caxis([colorScaleMin*imageSCMax,colorScaleMax*imageSCMax]);
subplot(2,nPlotCols,6)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMaps(:,iOverallNeuron,3,1),floor(offset/DS.binSizeDegrees)));
end
suptitle('Whole Track TwoD Ratemaps')

%% FIGURE 5: COLORMAP:WHITE BKGND - 2D RMaps clean runs only of each time segment 
offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180; % Transform to get calculations in line with plots.
maxHDRate = max(max(squeeze(NS.hdRMaps(:,iOverallNeuron,:,1))));

if size(NS.hdRMaps) == 3 % Rotation Data
    nPlotCols = 3;
else
    nPlotCols = 2;
end
figure(5);
clf;
subplot(1,nPlotCols,1)
sc(NS.platformSpatialDownSampleRMap(:,:,iOverallNeuron,1),...
    [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
    colormapVar,'w',isnan(NS.platformSpatialDownSampleRMap(:,:,iOverallNeuron,1)));
set(gca, 'DataAspectRatioMode', 'auto')
subplot(1,nPlotCols,2)
sc(twoDClean(:,:,iOverallNeuron,1),...
    [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
    colormapVar,'w',isnan(twoDClean(:,:,iOverallNeuron,1)));
set(gca, 'DataAspectRatioMode', 'auto')
if size(NS.hdRMaps,3) == 3 % Rotation Data
    subplot(1,nPlotCols,3)
    sc(twoDRotationClean(:,:,iOverallNeuron,2),...
        [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
        colormapVar,'w',isnan(twoDClean(:,:,iOverallNeuron,2)));
    set(gca, 'DataAspectRatioMode', 'auto')
end
suptitle('Clean paths HD Ratemaps - White background')
% [0.6,0,0;1,1,0] - alternate colormap

%% FIGURE 6: COLORMAP - 2D & HD RMaps clean runs only of each time segment 
offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180; % Transform to get calculations in line with plots.
maxHDRate = max(max(squeeze(NS.hdRMaps(:,iOverallNeuron,:,1))));
if size(NS.hdRMaps,3) == 3 % Rotation Data
    nPlotCols = 3;
else
    nPlotCols = 2;
end

figure(6);
clf;
subplot(2,nPlotCols,1)
sc(NS.platformSpatialDownSampleRMap(:,:,iOverallNeuron,1),...
    [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
    colormapVar);
set(gca, 'DataAspectRatioMode', 'auto')
subplot(2,nPlotCols,nPlotCols+1)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMaps(:,iOverallNeuron,1,1),floor(offset/DS.binSizeDegrees)));
polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMaps(:,iOverallNeuron,1,1),floor(offset/DS.binSizeDegrees)),'k');
subplot(2,nPlotCols,2)
sc(twoDClean(:,:,iOverallNeuron,1),...
    [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
    colormapVar);
set(gca, 'DataAspectRatioMode', 'auto')
subplot(2,nPlotCols,nPlotCols+2)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
rosePolarStyle(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMapsCleanRunsOnly(:,iOverallNeuron,2,1),floor(offset/DS.binSizeDegrees)));
polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMapsCleanRunsOnly(:,iOverallNeuron,2,1),floor(offset/DS.binSizeDegrees)),'k');
if size(NS.hdRMaps,3) == 3 % Rotation Data
    subplot(2,nPlotCols,3)
sc(twoDRotationClean(:,:,iOverallNeuron,2),...
    [colorScaleMin*imageSCMax,colorScaleMax*imageSCMax],...
    colormapVar);
set(gca, 'DataAspectRatioMode', 'auto')
subplot(2,nPlotCols,6)
% Code to control axis size.
t = 0 : .01 : 2 * pi;
P = polar(t, maxHDRate * ones(size(t)));
set(P, 'Visible', 'off')
hold on;
% Actually plot
polar(degtorad(DS.binSizeDegrees:DS.binSizeDegrees:360)',...
    circshift(NS.hdRMapsCleanRunsOnly(:,iOverallNeuron,3,1),floor(offset/DS.binSizeDegrees)));
end
suptitle('Clean paths TwoD and HD Ratemaps')

%% FIGURE 7: BAR - run pair correlation values
% figure(7);
% clf;
% toPlotCorr = [squeeze(NS.corrValsRuns1stTurnOn(1,2,iOverallNeuron)),...
%     squeeze(NS.corrValsRuns1stTurnOn(3,4,iOverallNeuron)),...
%     squeeze(NS.corrValsRuns1stTurnOn(1,4,iOverallNeuron)),...
%     squeeze(NS.corrValsRuns1stTurnOn(2,3,iOverallNeuron)),...
%     squeeze(NS.corrValsRuns1stTurnOn(1,3,iOverallNeuron)),...
%     squeeze(NS.corrValsRuns1stTurnOn(2,4,iOverallNeuron)),...
%     squeeze(NS.corrValsReturns(1,2,iOverallNeuron))];
% indices = [1,2,4,5,7,8,10];
% bar(indices,toPlotCorr);
% title('Correlations - Sides, Out vs In, endL v endR, returns');
% % disp(toPlotCorr); % Prints out corr vals to screen.
% % legend('1v2','3v4','1v4','2v3','1v3','2v4','5v6');

%% FIGURE 9: HD Reliability (Sparsity)
% offset = -1*rad2deg(DS.trueNorthRadians(iRecOverall))-180; % Transform to get calculations in line with plots.
% maxHDRate = max(max(squeeze(NS.hdRMaps(:,iOverallNeuron,:,1))));
% 
% figure(9)
% subplot(211)
% plot(circshift(NS.hdRMapsCleanRunsOnly(:,iOverallNeuron,2,1),floor(offset/DS.binSizeDegrees)));
% axis([0,72,0,maxHDRate]);
% subplot(212)
% plot(circshift(NS.hdReliabilityPercentTimeFiredCleanRunsOnly(:,iOverallNeuron,2),floor(offset/DS.binSizeDegrees)));
% axis([0,72,0,1]);
% suptitle('HD Reliability Values - FR top, Reliability bottom');

%% Display spatial property values
disp('********************************************************************');
disp(['Rat:', int2str(rat), ' Recording:', int2str(rec), ' Neuron:', int2str(neuron),...
    ' RecIndex:', int2str(iRecOverall), ' NeuronIndex:',int2str(iOverallNeuron)]);
disp('___________________________Platform Stats____________________________');
disp(['Sparsity: ', num2str(NS.platformSpatialSparsity(iOverallNeuron)),...
    ' Percentile:', num2str(percentilePlatformSpatialSparsity(iOverallNeuron))]);
disp(['Coherence: ', num2str(NS.platformSpatialCoherence(iOverallNeuron)),...
    ' Percentile:', num2str(percentilePlatformSpatialCoherence(iOverallNeuron))]);
disp(['Info (Bits/Spike): ', num2str(NS.platformSpatialInfoInBits(iOverallNeuron)),...
    ' Percentile:', num2str(percentilePlatformSpatialInfoInBits(iOverallNeuron))]);
% disp('___________________________Track Stats____________________________');
% disp(['Sparsity: ', num2str(NS.trackSpatialSparsity(iOverallNeuron)),...
%     ' Percentile:', num2str(percentileTrackSpatialSparsity(iOverallNeuron))]);
% disp(['Coherence: ', num2str(NS.trackSpatialCoherence(iOverallNeuron)),...
%     ' Percentile:', num2str(percentileTrackSpatialCoherence(iOverallNeuron))]);
% disp(['Info (Bits/Spike): ', num2str(NS.trackSpatialInfoInBits(iOverallNeuron)),...
%     ' Percentile:', num2str(percentileTrackSpatialInfoInBits(iOverallNeuron))]);
% 
% disp(['Number of fields on the track:',num2str(sum(NS.pathFieldsN(iOverallNeuron,1:6)))]);

% disp(['Platform PVal:' num2str(pValHDoTestPlat(iOverallNeuron)),...
%     ' Track PVal: ', num2str(pValHDoTest(iOverallNeuron)),...
%     ' Track Axis PVal: ', num2str(pValHDAxisrTest(iOverallNeuron))]);
