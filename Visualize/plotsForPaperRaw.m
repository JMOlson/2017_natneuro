figure;
hist(peakAngles(1,:)-peakAngles(2,:),[0:pi/18:2*pi])
hist(peakAngles(1,axisLIPReliableRatioFinal)-peakAngles(2,axisLIPReliableRatioFinal),[0:pi/18:2*pi])
hist(mod(peakAngles(1,axisLIPReliableRatioFinal)-peakAngles(2,axisLIPReliableRatioFinal),2*i),[0:pi/18:2*pi])
hist(mod(peakAngles(1,axisLIPReliableRatioFinal)-peakAngles(2,axisLIPReliableRatioFinal),2*pi),[0:pi/18:2*pi])
mean(mod(peakAngles(1,axisLIPReliableRatioFinal)-peakAngles(2,axisLIPReliableRatioFinal),2*pi))
mean(mod(peakAngles(1,:)-peakAngles(2,:),2*pi))
hist(peakAngles(:,axisLIPReliableRatioFinal),[0:pi/18:2*pi])
hist(reshape(mod(peakAngles(:,axisLIPReliableRatioFinal),2*pi),100,1),[0:pi/18:2*pi])
hist(reshape(mod(peakAngles(:,axisLIPReliableRatioFinal),2*pi),100,1),[0:pi/9:2*pi])
hist(reshape(mod(peakAngles(:,axisLIPReliableRatioFinal),pi),100,1),[0:pi/9:2*pi])


%% Rotation Stuff

% Our neurons
rotationAxis = union(SubPaperRotationsCategorizedAxisCellsNormalTrack.finalAxisLIPReliableRatio,...
    SubPaperRotationsCategorizedAxisCellsRotatedTrack.finalAxisLIPReliableRatio);

axisCells = union(SubPaperRotationsCategorizedAxisCellsNormalTrack.finalAxisLIPReliableRatio,SubPaperRotationsCategorizedAxisCellsRotatedTrack.finalAxisLIPReliableRatio);
makeTheCut2X = intersect(SubPaperRotationsCategorizedAxisCellsNormalTrack.finalAxisLIPReliableRatio,SubPaperRotationsCategorizedAxisCellsRotatedTrack.finalAxisLIPReliableRatio);

% Ran code from xcovORxcorstuff.m

% Just ran shifting through with circshift - track and platformAll + crossCorr
nBins = NS.HDMapsNBins;
nNeurons = length(rotationAxis);
hdRMaps2CorrPlatform = NS.hdRMapsCleanRunsOnly(:,rotationAxis,3,1);
hdRMaps2CorrTrack = NS.hdRMapsCleanRunsOnly(:,rotationAxis,2,1);
for iNeuron = 1:nNeurons
    for iShift = 1:nBins
        hdCorrPlatform(iShift,iNeuron) = corr(hdRMaps2CorrPlatform(:,iNeuron),...
            circshift(hdRMaps2CorrPlatform(:,iNeuron),iShift));
        hdCorr(iShift,iNeuron) = corr(hdRMaps2CorrTrack(:,iNeuron),...
            circshift(hdRMaps2CorrTrack(:,iNeuron),iShift));
        hdCrossCorr(iShift,iNeuron) = corr(hdRMaps2CorrPlatform(:,iNeuron),...
            circshift(hdRMaps2CorrTrack(:,iNeuron),iShift));
    end
end
hdCorrForFig = [hdCorr(nBins,:);hdCorr]';
hdCorrForFigPlatform = [hdCorrPlatform(nBins,:);hdCorrPlatform]';
hdCrossCorr = circshift(hdCrossCorr,18);
[maxCrossCorrVals, maxCrossCorrValInds] = max(hdCrossCorr,[],1);
[~,axisCellRotationPlotInOrder] = sort(maxCrossCorrValInds,'ascend');
set(0,'DefaultFigureColormap',jet)
figure;
subplot(121)
imagesc(hdCrossCorr');
subplot(122)
imagesc(hdCrossCorr(:,axisCellRotationPlotInOrder)')

% difference in peak locations from normal to rotated models.
load('mixVMMRotationTrackNormal.mat')
normalPeaks = thetaEMMain(1:2,3,rotationAxis);
load('mixVMMRotationTrackRotated.mat')
rotatedPeaks = thetaEMMain(1:2,3,rotationAxis);

figure;
bar([-pi/2:pi/18:pi/2],circshift(hist(mod(normalPeaks(:)-rotatedPeaks(:),pi),[0:pi/18:pi])',floor(length([0:pi/18:pi])/2)));
figure;
hist(normalPeaks(:),18)








