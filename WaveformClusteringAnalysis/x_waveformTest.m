figure;
i=1;
while i<=size(spikeFormData.spikeAvg,1)
    clf;
    for j = 1:20
        if i<=size(spikeFormData.spikeAvg,1)
            subplot(5, 4, j);
            plot(spikeFormData.spikeAvg(i,:)')
            hold on;
            plot(spikeFormData.spikeAvg(i,:)'+spikeFormData.spikeStdDev(i,:)','r')
            plot(spikeFormData.spikeAvg(i,:)'-spikeFormData.spikeStdDev(i,:)','r')
            title(sprintf('N%d - width=%d (ch %d)', i, ...
                spikeFormData.spikeWidthRaw(1,i), ...
                spikeFormData.spikeWidthTetrodeWireIndex(1,i)));
            i = i+1;
        end
    end
end