%this script is for plotting output of NatEmKmeanCluster4CenFunction in
%customizable ways. Load a complete spikeformdata before running.

clusterMatrix = spikeFormData.clusterMatrix;
centers = spikeFormData.clusterCenters;
pointIDs = spikeFormData.clusterPointIDs;
%jakeSaysAxis =

figure();
dim1 = 1; %index of the column you want to plot, i.e. PC1
dim2 = 2;
dim3 = 6;
scatter3(clusterMatrix(pointIDs == 1,dim1), ...
    clusterMatrix(pointIDs == 1,dim2),clusterMatrix(pointIDs == 1,dim3),20,'r','filled');
hold on; 
scatter3(centers(1,dim1),centers(1,dim2),centers(1,dim3), ...
    80,'Marker','x','MarkerEdgeColor',[0.6 0 0],'LineWidth',4);
scatter3(clusterMatrix(pointIDs == 2,dim1), ...
    clusterMatrix(pointIDs == 2,dim2),clusterMatrix(pointIDs == 2,dim3),20,'b','filled');
scatter3(centers(2,dim1),centers(2,dim2),centers(2,dim3),80, ...
    'Marker','x','MarkerEdgeColor',[0 0 0.6],'LineWidth',4);
scatter3(clusterMatrix(pointIDs == 3,dim1), ...
    clusterMatrix(pointIDs == 3,dim2),clusterMatrix(pointIDs == 3,dim3),20,'g','filled');
scatter3(centers(3,dim1),centers(3,dim2),centers(3,dim3),80, ...
    'Marker','x','MarkerEdgeColor',[0 0.6 0],'LineWidth',4);
scatter3(clusterMatrix(pointIDs == 4,dim1), ...
    clusterMatrix(pointIDs == 4,dim2),clusterMatrix(pointIDs == 4,dim3),20,'c','filled');
scatter3(centers(4,dim1),centers(4,dim2),centers(4,dim3),80, ...
    'Marker','x','MarkerEdgeColor',[0 0.6 0.6],'LineWidth',4);
xlabel('PC 1');
ylabel('PC 2');
zlabel('PC 3');

%scatter3(clusterMatrix(jakeSaysAxis,dim1), ...
%    clusterMatrix(jakeSaysAxis,dim2),clusterMatrix(jakeSaysAxis,dim3),40,'k','filled');

%%
clusterMatrix = spikeFormData.clusterMatrix;
centers = spikeFormData.clusterCenters;
pointIDs = spikeFormData.clusterPointIDs;
%jakeSaysAxis =

figure();
dim1 = 1; %index of the column you want to plot, i.e. PC1
dim2 = 2;
scatter(clusterMatrix(pointIDs == 1,dim1), ...
    clusterMatrix(pointIDs == 1,dim2),20,'r','filled');
hold on; 
scatter(centers(1,dim1),centers(1,dim2), ...
    80,'Marker','x','MarkerEdgeColor',[0.6 0 0],'LineWidth',4);
scatter(clusterMatrix(pointIDs == 2,dim1), ...
    clusterMatrix(pointIDs == 2,dim2),20,'b','filled');
scatter(centers(2,dim1),centers(2,dim2),80, ...
    'Marker','x','MarkerEdgeColor',[0 0 0.6],'LineWidth',4);
scatter(clusterMatrix(pointIDs == 3,dim1), ...
    clusterMatrix(pointIDs == 3,dim2),20,'g','filled');
scatter(centers(3,dim1),centers(3,dim2),80, ...
    'Marker','x','MarkerEdgeColor',[0 0.6 0],'LineWidth',4);
scatter(clusterMatrix(pointIDs == 4,dim1), ...
    clusterMatrix(pointIDs == 4,dim2),20,'c','filled');
scatter(centers(4,dim1),centers(4,dim2),80, ...
    'Marker','x','MarkerEdgeColor',[0 0.6 0.6],'LineWidth',4);
scatter(clusterMatrix(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,dim1),clusterMatrix(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,dim2),120,'xk')
xlabel('ISI PC 1');
ylabel('ISI PC 2');

%%
%clusterMatrix columns 
%   [PCs (variable #columns, usually 5) | spike widths | firing rate]
clusterMatrix = spikeFormData.clusterMatrix;
centers = spikeFormData.clusterCenters;
pointIDs = spikeFormData.clusterPointIDs;
%jakeSaysAxis =

% plot = true(542,1);
% plot(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio) = 0;

figure();
dim1 = 6; %index of the column you want to plot, i.e. PC1
%dim2 = 6;
scatter(clusterMatrix(:,dim1), ...
    spikeFormData.burstIndex,20,'g','filled');
hold on; 
% scatter(clusterMatrix(pointIDs == 1,dim1), ...
%     clusterMatrix(pointIDs == 1,dim2),20,'r','filled');
% scatter(clusterMatrix(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,dim1),clusterMatrix(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,dim2),120,'xk')
xlabel('Firing Rate');
ylabel('Burst Index');