function [ spikeFormData, waveFormData ] = NatEmFullAnalysisFunction( neuronListFileName)%, saveFileName )
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
waveFormData = NatEmWaveFormFunction(neuronListFileName);
spikeFormData = NatEmSpikeWidthFunction(waveFormData);
spikeFormData = NatEmISIpcaFunction(spikeFormData);
spikeFormData = NatEmFiringRateFunction(spikeFormData);
%save(saveFileName, 'spikeFormData');
spikeFormData = NatEmKmeanCluster4CenFunction(spikeFormData);
end