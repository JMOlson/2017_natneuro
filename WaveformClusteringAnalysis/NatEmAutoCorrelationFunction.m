function [ spikeFormData ] = NatEmAutoCorrelationFunction( spikeFormInput )
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
spikeFormData = spikeFormInput;
autointensitySpikes = zeros(size(spikeFormData.rmapFilePath,2),2001); % 1 second before and after.
autointensityOcc = autointensitySpikes;

for i=1:size(spikeFormData.rmapFilePath,2)
    load(char(spikeFormData.rmapFilePath(i)), 'tfileList','allTfiles');
    spikeTimes = allTfiles{spikeFormData.neuronNumber(i)};
    disp(i);
% spikeTimes = allTfiles{9};

    spikeTimesInMS = 1+round(spikeTimes*1000);
    
    spikesByMS = zeros(1,1+1000*ceil(max(spikeTimes,[],1)));
    for spikeIndex = 1:size(spikeTimes,1)
        spikesByMS(1,1+round(spikeTimes*1000)) = 1;
    end

    for iSpike = 1:length(spikeTimesInMS)
        if spikeTimesInMS(iSpike) < 1001
            offset = spikeTimesInMS(iSpike)-1;
            autointensitySpikes(i,1001-offset:end) = autointensitySpikes(i,1001-offset:end)+...
                spikesByMS(1:spikeTimesInMS(iSpike)+1000);
            autointensityOcc(i,1001-offset:end) = autointensityOcc(i,1001-offset:end)+1;
        elseif spikeTimesInMS(iSpike) > length(spikesByMS)-1000
            offset = length(spikesByMS)-spikeTimesInMS(iSpike);
            autointensitySpikes(i,1:1001+offset) = autointensitySpikes(i,1:1001+offset)+...
                spikesByMS(spikeTimesInMS(iSpike)-1000:end);
            autointensityOcc(i,1:1001+offset) = autointensityOcc(i,1:1001+offset)+1;
        else
            autointensitySpikes(i,:) = autointensitySpikes(i,:)+...
                spikesByMS(spikeTimesInMS(iSpike)-1000:spikeTimesInMS(iSpike)+1000);
            autointensityOcc(i,:) = autointensityOcc(i,:)+1;
        end
    end
%     autocov = xcov(spikesByMs,1000);
%     autocor = autocorr(spikesByMs,1000);
%     spikeFormData.autoCov(i,:) = autocov;
%     spikeFormData.autoCorrelation(i,:) = autocor;
end
autointensityNormed = autointensitySpikes./autointensityOcc; % spikes/ms
spikeFormData.autointensitySpikesPerSec = autointensityNormed*1000; 
sum6ms = sum(spikeFormData.autointensitySpikesPerSec(:, 1002:1007), 2);
sum20ms = sum(spikeFormData.autointensitySpikesPerSec(:, 1002:1021), 2);
burstIndex=sum6ms./sum20ms;
spikeFormData.burstIndex=burstIndex;
end
