function [ DataStruct, NeuronStruct ] = pathCorrelations( DataStruct, NeuronStruct)
%NAMEHERE Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.

% Only calc if there are at least 5 runs of each.
goodToCorr = [];
iOverallNeuron = 1;
for iRec = 1:DataStruct.nRecs
    dataFile = DataStruct.recFile{iRec};
    subNeuronInds = DataStruct.neuronList{iRec};
    load(dataFile,'nRuns');
    DataStruct.nRunsEachPath{iRec} = nRuns;
    nCells = length(subNeuronInds);
    
    % Rat and Rec Data.
    tmp = strfind(DataStruct.recFile{iRec},'\');
    DataStruct.rat{iRec} = DataStruct.recFile{iRec}(tmp(end)+1:tmp(end)+4);
    if strcmp(DataStruct.recFile{iRec}(tmp(end)+10),'_');
        DataStruct.rec(iRec) = str2double(DataStruct.recFile{iRec}(tmp(end)+9));
    else
        DataStruct.rec(iRec) = str2double(DataStruct.recFile{iRec}(tmp(end)+9:tmp(end)+10));
    end
    for iCell = 1:nCells
        NeuronStruct.rat{iOverallNeuron+iCell-1} = DataStruct.rat{iRec};
        NeuronStruct.rec(iOverallNeuron+iCell-1) = DataStruct.rec(iRec);
    end
    NeuronStruct.neuronIndex(iOverallNeuron:iOverallNeuron+nCells-1) = DataStruct.neuronList{iRec};
    NeuronStruct.channelIndex(iOverallNeuron:iOverallNeuron+nCells-1) = DataStruct.channelList{iRec};
    iOverallNeuron = iOverallNeuron+nCells;
    % End Rat and Rec Data.
    
    % Corr stuff
    all4Paths = sum(DataStruct.pathList{iRec} > 0 & DataStruct.pathList{iRec} <= 4) == 4;
    all4Paths = all4Paths & all(DataStruct.nRunsEachPath{iRec}(1:4)>=5);
    
    goodToCorr = [goodToCorr; repmat(all4Paths,length(DataStruct.neuronList{iRec}),1)];
end

NeuronStruct.corrValsRuns = nan(4,4,length(NeuronStruct.neuronIndex));
NeuronStruct.corrValsRuns1stTurnOn = nan(4,4,length(NeuronStruct.neuronIndex));
NeuronStruct.corrValsReturns = nan(2,2,length(NeuronStruct.neuronIndex));

NeuronStruct.corrPValsRuns = nan(4,4,length(NeuronStruct.neuronIndex));
NeuronStruct.corrPValsRuns1stTurnOn = nan(4,4,length(NeuronStruct.neuronIndex));
NeuronStruct.corrPValsReturns = nan(2,2,length(NeuronStruct.neuronIndex));

for iOverallNeuron = 1:length(NeuronStruct.neuronIndex)
    % Full Run Correlation Values & without 1st straight.    
    if goodToCorr
        [NeuronStruct.corrValsRuns(:,:,iOverallNeuron), NeuronStruct.corrPValsRuns(:,:,iOverallNeuron)] = ...
            corr([squeeze(NeuronStruct.MeanLinearRatesCompiled{1}(2:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{2}(2:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{3}(2:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{4}(2:end-1,iOverallNeuron,1))],'type','Spearman');
        [NeuronStruct.corrValsRuns1stTurnOn(:,:,iOverallNeuron), NeuronStruct.corrPValsRuns1stTurnOn(:,:,iOverallNeuron)] = ...
            corr([squeeze(NeuronStruct.MeanLinearRatesCompiled{1}(46:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{2}(46:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{3}(46:end-1,iOverallNeuron,1)),...
            squeeze(NeuronStruct.MeanLinearRatesCompiled{4}(46:end-1,iOverallNeuron,1))],'type','Spearman');
    end
    % Full Return Correlation Values.
    [NeuronStruct.corrValsReturns(:,:,iOverallNeuron), NeuronStruct.corrPValsReturns(:,:,iOverallNeuron)] = ...
        corr([squeeze(NeuronStruct.MeanLinearRatesCompiled{9}(2:end-1,iOverallNeuron,1)),...
        squeeze(NeuronStruct.MeanLinearRatesCompiled{10}(2:end-1,iOverallNeuron,1))],'type','Spearman');
    
end
NeuronStruct.corrSigRuns = NeuronStruct.corrPValsRuns < 0.05;
NeuronStruct.corrSigRuns1stTurnOn = NeuronStruct.corrPValsRuns1stTurnOn < 0.05;
NeuronStruct.corrSigReturns = NeuronStruct.corrPValsReturns < 0.05;

% NeuronStruct.pathCorrSigDiff = pathCorrSigRuns-pathCorrSigRuns1stTurnOn;
% NeuronStruct.pathCorrSigTotal = sum(sum(sum(pathCorrSigRuns)));
% NeuronStruct.pathCorrSigWith1Only = sum(sum(sum(pathCorrSigDiff == -1)));
% NeuronStruct.pathCorrSigWOut1Only = sum(sum(sum(pathCorrSigDiff == 1)));
end
