function [ DataStruct, NeuronStruct] = hdBy2DSpatialReliabilityCalculator(DataStruct, NeuronStruct, mapToUse, iTimeSegment, runningAnalysisFlag, isRunning)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015

percentBelow = DataStruct.HDSpatialReliabilityPercentOfMeanThreshold; % XX percent of mean is included as reliable.
cutoffVal = DataStruct.HDSpatialMinimumSampling; % 1/6th of second 

binSizeDegrees = DataStruct.binSizeDegrees;
nHDBins = NeuronStruct.HDMapsNBins;
nNeurons = length(NeuronStruct.neuronIndex);

rates = mapToUse(:,:,:,:,1);
sufficientSampling = mapToUse(:,:,:,:,3)>=cutoffVal;
rates(~sufficientSampling) = NaN;

if runningAnalysisFlag && isRunning
    meanHDFiring = NeuronStruct.hdRMapsRunning(:,:,iTimeSegment,1); % This should just be mean firing rate - pull from hdrmaps.
    NeuronStruct.HDBy2DSpatialReliabilityIsRunning(:,:,iTimeSegment) = ...
        squeeze(nansum(nansum(rates>repmat(reshape(meanHDFiring*percentBelow,[1,1,nHDBins,nNeurons]),...
        [size(rates,1),size(rates,2),1,1]) & sufficientSampling,2),1)./nansum(nansum(sufficientSampling,2),1));
    NeuronStruct.HDBy2DSpatialReliabilityIsRunningSampling(:,:,iTimeSegment) = nansum(nansum(sufficientSampling,2),1);
elseif runningAnalysisFlag && ~isRunning
    meanHDFiring = NeuronStruct.hdRMapsStill(:,:,iTimeSegment,1); % This should just be mean firing rate - pull from hdrmaps.
    NeuronStruct.HDBy2DSpatialReliabilityStill(:,:,iTimeSegment) = ...
        squeeze(nansum(nansum(rates>repmat(reshape(meanHDFiring*percentBelow,[1,1,nHDBins,nNeurons]),...
        [size(rates,1),size(rates,2),1,1]) & sufficientSampling,2),1)./nansum(nansum(sufficientSampling,2),1));
    NeuronStruct.HDBy2DSpatialReliabilityStillSampling(:,:,iTimeSegment) = nansum(nansum(sufficientSampling,2),1);
elseif ~runningAnalysisFlag
    meanHDFiring = NeuronStruct.hdRMaps(:,:,iTimeSegment,1); % This should just be mean firing rate - pull from hdrmaps.
    NeuronStruct.HDBy2DSpatialReliability(:,:,iTimeSegment) = ...
        squeeze(nansum(nansum(rates>repmat(reshape(meanHDFiring*percentBelow,[1,1,nHDBins,nNeurons]),...
        [size(rates,1),size(rates,2),1,1]) & sufficientSampling,2),1)./nansum(nansum(sufficientSampling,2),1));
    NeuronStruct.HDBy2DSpatialReliabilitySampling(:,:,iTimeSegment) = nansum(nansum(sufficientSampling,2),1);
end

end

