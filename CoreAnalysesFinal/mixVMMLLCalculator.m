function [ logLikelihood, modelVals, SSE] = mixVMMLLCalculator( hdRMap, mix, theta, m )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
try
% Init output variable
logLikelihood = nan;
modelVals = nan(length(hdRMap),1);
SSE = nan;
% ESS = nan;
% TSS = nan;
% sumErr = nan;

% In case there is no firing for this neuron, so zeros everywhere.
if all(isnan(hdRMap)) || max(hdRMap) < 0.05
    return
end
% Create sample data from HD FR data. Start at 2*pi/(#ofsamples) - just
% like the real data being read in is binned.
dataSamples = [];
for iBin = 1:length(hdRMap)
    dataSamples = [dataSamples; repmat(iBin*2*pi/length(hdRMap),round(hdRMap(iBin)*10),1)];
end
nDataPoints = length(dataSamples);

% Initialize variables for computation speed purposes
nVMDists = length(theta);
vonMisesVal = zeros(nDataPoints,nVMDists);
modelValDist = zeros(length(hdRMap),nVMDists);

% Probabilities (pdf) of data for initalized Von Mises Distributions.
for iDist = 1:nVMDists
    % CircStat toolbox calculation for von mises PDF for the string of angles given.
    vonMisesVal(:,iDist) = circ_vmpdf(dataSamples,theta(iDist),m(iDist));
    % Find model values at each angle. MAKE SURE TO START AT SAME POINT AS
    % ACTUAL DATA.
    modelValDist(:,iDist) = ...
        circ_vmpdf(2*pi/length(hdRMap):2*pi/length(hdRMap):2*pi,theta(iDist),m(iDist))./...
        (length(hdRMap)./(2*pi));
end
logLikelihood = sum(log(sum(repmat(mix',nDataPoints,1).*vonMisesVal,2)),1);

%SSE
modelVals = sum(repmat(mix',length(hdRMap),1).*modelValDist,2);
dataNormed = hdRMap./sum(hdRMap);
SSE = sum((modelVals-dataNormed).^2);
%% Not valid for our data or decided not to use.
% ESS - explained sum of squares
% ESS = sum((modelVals-mean(dataNormed)).^2);
% TSS = sum((dataNormed-mean(dataNormed)).^2);
% percentExplained = sum(abs(modelVals-dataNormed));
% sumErr = sum(abs(modelVals-dataNormed));
catch
    disp('hopefully debugger opens first');
end


