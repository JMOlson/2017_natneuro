function [ DataStruct, NeuronStruct] = isRunningAnalysis( DataStruct, NeuronStruct, nTimeSegments)
%ISRUNNINGANALYSIS Adds smoothed velocity, angular velocity and running times
%   All outputs are added to datastruct.
%   Running marks line markers for each segment where the animals movements are above the velocity
%   below the angular velocity thresholds.
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.


minRunVel = 10; % pixels per second - 10 is approx equal to running 3cm/sec
maxTurnVel = 20; % radians per second - approx equivalent to turning head a quarter turn in 1/12 of a second. Fast!
DataStruct.minRunningVel = minRunVel;
DataStruct.maxRunningTurnVel = maxTurnVel;

nRecs = DataStruct.nRecs;
for iRec = 1:nRecs
    dataFile = DataStruct.recFile{iRec};
    processedDvtFile = DataStruct.dvtFile{iRec};
    load(processedDvtFile,'vel','processedDVT');
    load(dataFile,'timeStamps','sampleRate');
    DataStruct.sampleRate(iRec) = sampleRate;
    DataStruct.timeStamps{iRec} = timeStamps;
    
    timeToOffsetDueToSmoothing = round((length(processedDVT)-length(vel))/2);
    velAvg = nanmean(vel(:,1,:),3); % vel is in pixels/second. angle is in radians. Combines the two lights.
    % calculated over 1/10 of a second (6 samples apart).   

    % velDirections is in: -pi : pi - this calculates angular velocity.
    velDir = circ_mean(squeeze(vel(:,2,:)),[],2);
    velDir2 = velDir;
    velDir2(velDir2<0) = velDir2(velDir<0)+2*pi;
    angVelTmp = [NaN;diff(velDir)];
    angVelTmp2 = [NaN;diff(velDir2)];
    angVel = angVelTmp;
    tmp2RightAngVel = abs(angVelTmp) > abs(angVelTmp2);
    angVel(tmp2RightAngVel) = angVelTmp2(tmp2RightAngVel);
    
    % Convert to radians per second - calculated at sampling rate (60hz) 
    % but direction is calculated over vector calculated from samples 1/10
    % of a second apart - like the velocity.
    angVel = angVel*60;
    
    % Smoothing over 1/10 of a second.
    velAvgSmo = smooth(velAvg(:,1),6,'moving');
    angVelSmo = smooth(angVel,6,'moving');
    
    for i = 1:nTimeSegments
        velAvgTimeSeg = velAvgSmo(timeStamps(2*i-1)-timeToOffsetDueToSmoothing:...
            timeStamps(2*i)-timeToOffsetDueToSmoothing,1);
        angVelTimeSeg = angVelSmo(timeStamps(2*i-1)-timeToOffsetDueToSmoothing:...
            timeStamps(2*i)-timeToOffsetDueToSmoothing);
        
        isRunning = velAvgTimeSeg > minRunVel & abs(angVelTimeSeg) < maxTurnVel;
        DataStruct.runningLineIndicesBySegment{i,iRec} = find(isRunning);
        DataStruct.angVel{i,iRec} = velAvgTimeSeg;
        DataStruct.vel{i,iRec} = angVelTimeSeg;
    end
    
    % Plotting commented out.
    
    % for i= 1109:500:27339
    % figure(1)
    % subplot(211);
    % plot(velAvgSmo(i:i+500),'r')
    % title('Velocity')
    % subplot(212);
    % plot(angVelSmo(i:i+500))
    % title('angVel')
    % figure(2);
    % subplot(121)
    % scatter(pos(i:i+500,2),pos(i:i+500,3),10,[0:500]);
    % axis([0,400,0,400])
    % subplot(122)
    % scatter(pos(i:i+500,2),pos(i:i+500,3),10,angVelSmo(i:i+500));
    % axis([0,400,0,400])
    % colorbar;
    % pause;
    % end
    
    % breakPoints = diff(diff(DataStruct.platformRunningTimes{iRec})>1);
    % starts = [0;find(breakPoints == -1)]+1;
    % ends = [find(breakPoints == 1);length(breakPoints)]+1;
    % minLength = 30;
    %
    % runningArena = [];
    % for i = 1:length(starts)
    %     if ends(i)-starts(i) > minLength
    %     scatter(pos(DataStruct.platformRunningTimes{iRec}(starts(i)):...
    %       DataStruct.platformRunningTimes{iRec}(ends(i)),2),...
    %       pos(DataStruct.platformRunningTimes{iRec}(starts(i)):...
    %       DataStruct.platformRunningTimes{iRec}(ends(i)),3),'k.')
    %     runningArena = [runningArena, (DataStruct.platformRunningTimes{iRec}(starts(i)):DataStruct.platformRunningTimes{iRec}(ends(i)))];
    %     axis([0,600,0,700]);
    %     hold on;
    %     pause;
    %     end
    % end
    
end
end

