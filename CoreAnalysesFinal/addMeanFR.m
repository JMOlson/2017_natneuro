function [ DataStruct, NeuronStruct ] = addMeanFR( DataStruct, NeuronStruct )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


for iNeuron = 1:length(NeuronStruct.posNSpikesAll)
    iRec = NeuronStruct.recCount(iNeuron);
    timeStamps = DataStruct.timeStamps{iRec};
    nTimeSegments = length(timeStamps)/2;
    iNeuNSpikes = NeuronStruct.posNSpikesAll{iNeuron};
    sampleRate = DataStruct.sampleRate(iRec);
    
    for iTimeSegment = 2:2
        nSpikesTimeSegment = ...
            iNeuNSpikes(timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
        NeuronStruct.overallMeanFR(iNeuron,iTimeSegment) = ...
            (sum(nSpikesTimeSegment)/length(nSpikesTimeSegment))*sampleRate;
    end
end

% [SubPaperDataStruct, SubPaperNeuronStruct] = addMeanFR(SubPaperDataStruct,SubPaperNeuronStruct);
% platformMeanFR = mean(SubPaperNeuronStruct.overallMeanFR(:,1));
% trackMeanFR = mean(SubPaperNeuronStruct.overallMeanFR(:,2));
% [SubPaperDataStruct, SubPaperNeuronStruct] = addMeanFR(SubPaperDataStruct,SubPaperNeuronStruct);
% platformMeanFR = mean(SubPaperNeuronStruct.overallMeanFR(:,1));
% trackMeanFR = mean(SubPaperNeuronStruct.overallMeanFR(:,2));
% load('D:\subPaper\FinalAnalysisResults\MainDataset\SubPaperMainAxisCellResultsRunning.mat')
% platformMeanFRAxis = mean(SubPaperNeuronStruct.overallMeanFR(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,1));
% trackMeanFRAxis = mean(SubPaperNeuronStruct.overallMeanFR(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,2));

% meanHDFR = squeeze(nanmean(SubPaperNeuronStruct.hdRMaps(:,:,:,1),1));
% platformMeanHDFR = nanmean(meanHDFR(:,1));
% trackMeanHDFR = mean(meanHDFR(:,2));
% platformMeanHDFRAxis = nanmean(meanHDFR(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,1));
% trackMeanHDFRAxis = nanmean(meanHDFR(SubPaperMainTrackRunningCategorizedAxisCells.finalAxisLIPReliableRatio,2));
end

