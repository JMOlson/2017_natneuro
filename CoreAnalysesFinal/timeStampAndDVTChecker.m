function [ a ] = timeStampAndDVTChecker( DataStruct )
%NAMEHERE Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.
% DVT Scaling and TimeStamps existing checker. - A quick little
% verification tool for the data.
for iRec = 1:DataStruct.nRecs
    dataFile = DataStruct.recFile{iRec};
    processedDvtFile = DataStruct.dvtFile{iRec};
    load(dataFile,'timeStamps','pos','pixelDvt');
    load(processedDvtFile,'processedDVT');
if exist('timeStamps','var');
    a.timeStamps{iRec} = timeStamps;
else
    disp(iRec);
end
a.pos(iRec) = pos(60,1);
a.pixelDvt(iRec,:) = pixelDvt(60,1:2);
a.processedDvt(iRec,:) = processedDVT(60,1:2);
clear timeStamps pos pixelDvt processedDVT 
end