function [ DataStruct, NeuronStruct ] = addMeanLinearRates( DataStruct)
%NAMEHERE Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.

nRecs = DataStruct.nRecs;
iOverallNeuron = 1;
for iRec = 1:nRecs
    dataFile = DataStruct.recFile{iRec};
    subNeuronInds = DataStruct.neuronList{iRec};
    load(dataFile, 'MeanLinearRates', 'pathList');
    DataStruct.pathList{iRec} = pathList;
    nNeurons = length(subNeuronInds);
    
    for iNeuron = 1:nNeurons
        iSubNeuron = subNeuronInds(iNeuron);
    
        for iPath = 1:length(pathList)
            NeuronStruct.MeanLinearRatesCompiled{pathList(iPath)}(:,iOverallNeuron,1) = ...
                squeeze(MeanLinearRates{iPath}(iSubNeuron,:,1));
            NeuronStruct.MeanLinearRatesCompiled{pathList(iPath)}(:,iOverallNeuron,2) = ...
                squeeze(MeanLinearRates{iPath}(iSubNeuron,:,2));
            NeuronStruct.MeanLinearRatesCompiled{pathList(iPath)}(:,iOverallNeuron,3) = ...
                squeeze(MeanLinearRates{iPath}(iSubNeuron,:,3));
        end
        iOverallNeuron = iOverallNeuron+1;
    end
end
end
