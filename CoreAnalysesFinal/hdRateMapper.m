function [ DataStruct, NeuronStruct ] = hdRateMapper( DataStruct, NeuronStruct, nTimeSegments )
%HDRATEMAPPER Calc cell firing rate at each head direction (binned)
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.

binSizeDegrees = 10;
DataStruct.binSizeDegrees = binSizeDegrees;
nHDBins = 360/binSizeDegrees;
NeuronStruct.HDMapsNBins = nHDBins;

nRecs = DataStruct.nRecs;
iOverallNeuron = 1;
for iRec = 1:nRecs
    subNeuronInds = DataStruct.neuronList{iRec};
    sampleRate = DataStruct.sampleRate(iRec);
    timeStamps = DataStruct.timeStamps{iRec};
    nCells = length(subNeuronInds);
    
    %% Actual hdRMapping
    for iNeuron = 1:nCells
        % using the offset corrected radian values.
        iNeuHDRadians = NeuronStruct.posHDRadiansAllCorrected{iOverallNeuron+iNeuron-1};
        iNeuPath = NeuronStruct.posPathAndBinAll{iOverallNeuron+iNeuron-1}(:,1);
        iNeuNSpikes = NeuronStruct.posNSpikesAll{iOverallNeuron+iNeuron-1};
        
        for iTimeSegment = 1:nTimeSegments
            if timeStamps(iTimeSegment*2)-timeStamps(iTimeSegment*2-1) > 5
                hdRadiansRelevant = iNeuHDRadians(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                posNSpikesRelevant = iNeuNSpikes(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                % note for the line below:
                % mod to get all positive values.
                % round to nearest bin center. 
                hdRadiansRelevantBinned =...
                    round(mod(radtodeg(hdRadiansRelevant),360)/binSizeDegrees);
                % put all values from bin 0 into the last bin (same bin, circle).
                hdRadiansRelevantBinned(hdRadiansRelevantBinned==0) = nHDBins;
                hdRadiansRelevantBinned(isnan(hdRadiansRelevant)) = NaN;
                
                runValsRelevant = false(length(posNSpikesRelevant),1);
                runValsRelevant(DataStruct.runningLineIndicesBySegment{iTimeSegment,iRec}) = true;
                
                hdRadiansRelevantRunningBinned = hdRadiansRelevantBinned(runValsRelevant);
                posNSpikesRelevantRunning = posNSpikesRelevant(runValsRelevant);
                
                hdRadiansRelevantStillBinned = hdRadiansRelevantBinned(~runValsRelevant);
                posNSpikesRelevantStill = posNSpikesRelevant(~runValsRelevant);
                
                
                
                hdOccs=zeros(nHDBins,1);
                hdSpikes=zeros(nHDBins,1);
                hdRates=zeros(nHDBins,1);
                
                hdOccsRunning=zeros(nHDBins,1);
                hdSpikesRunning=zeros(nHDBins,1);
                hdRatesRunning=zeros(nHDBins,1);
                
                hdOccsStill=zeros(nHDBins,1);
                hdSpikesStill=zeros(nHDBins,1);
                hdRatesStill=zeros(nHDBins,1);
                
                for iHDBins = 1:nHDBins
                    hdSpikes(iHDBins) = sum(posNSpikesRelevant(hdRadiansRelevantBinned == iHDBins));
                    hdOccs(iHDBins) = sum(hdRadiansRelevantBinned == iHDBins);
                    
                    hdSpikesRunning(iHDBins) = sum(posNSpikesRelevantRunning(hdRadiansRelevantRunningBinned == iHDBins));
                    hdOccsRunning(iHDBins) = sum(hdRadiansRelevantRunningBinned == iHDBins);
                    
                    hdSpikesStill(iHDBins) = sum(posNSpikesRelevantStill(hdRadiansRelevantStillBinned == iHDBins));
                    hdOccsStill(iHDBins) = sum(hdRadiansRelevantStillBinned == iHDBins);
                end
                isOcc = hdOccs>0;
                hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*sampleRate;
                hdRates(~isOcc) = NaN;
                
                isOccRunning = hdOccsRunning>0;
                hdRatesRunning(isOccRunning) = (hdSpikesRunning(isOccRunning)./hdOccsRunning(isOccRunning))*sampleRate;
                hdRatesRunning(~isOccRunning) = NaN;
                
                isOccStill = hdOccsStill>0;
                hdRatesStill(isOccStill) = (hdSpikesStill(isOccStill)./hdOccsStill(isOccStill))*sampleRate;
                hdRatesStill(~isOccStill) = NaN;
                
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=hdRates;
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=hdSpikes;
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=hdOccs;
                
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=hdRatesRunning;
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=hdSpikesRunning;
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=hdOccsRunning;
                
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=hdRatesStill;
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=hdSpikesStill;
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=hdOccsStill;
                
                if iTimeSegment > 1
                    % Additional state info. %Time Segment
                    segmentPathData = iNeuPath(timeStamps(iTimeSegment*2-1):...
                        timeStamps(iTimeSegment*2),:);
                    changeStarts = find(diff(segmentPathData));
                    changeEnds = [changeStarts(2:end);length(segmentPathData)];
                    if iTimeSegment == 2
                        pathsToGrab = [1,2,3,4,9,10];
                    elseif iTimeSegment == 3
                        pathsToGrab = [11,12,13,14,19,20];
                    end
                    
                    wantedStarts = false(length(changeStarts),1);
                    for iPath = 1:length(pathsToGrab)
                        wantedStarts = (segmentPathData(changeEnds)==pathsToGrab(iPath))|...
                            wantedStarts;
                    end
                    pathsWanted = find(wantedStarts);
                    if ~isempty(pathsWanted)
                        changeStarts = changeStarts(pathsWanted);
                        changeEnds = changeEnds(pathsWanted);
                        
                        trimmedPosNSpikesRelevant = [];
                        trimmedHDRadiansRelevantBinned = [];
                        for iSegment = 1:length(changeStarts);
                            trimmedPosNSpikesRelevant = [trimmedPosNSpikesRelevant;...
                                posNSpikesRelevant(changeStarts(iSegment):changeEnds(iSegment))];
                            trimmedHDRadiansRelevantBinned = [trimmedHDRadiansRelevantBinned;...
                                hdRadiansRelevantBinned(changeStarts(iSegment):changeEnds(iSegment))];
                        end
                       
                        hdOccs=zeros(nHDBins,1);
                        hdSpikes=zeros(nHDBins,1);
                        hdRates=zeros(nHDBins,1);
                        
                        for iHDBins = 1:nHDBins
                            hdSpikes(iHDBins) = sum(trimmedPosNSpikesRelevant(trimmedHDRadiansRelevantBinned == iHDBins));
                            hdOccs(iHDBins) = sum(trimmedHDRadiansRelevantBinned == iHDBins);
                        end
                        isOcc = hdOccs>0;
                        hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*sampleRate;
                        hdRates(~isOcc) = NaN;
                        
                        NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=hdRates;
                        NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=hdSpikes;
                        NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=hdOccs;
                    end
                end
            else
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=NaN;
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=NaN;
                NeuronStruct.hdRMaps(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=NaN;
                
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=NaN;
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=NaN;
                NeuronStruct.hdRMapsRunning(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=NaN;
                
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=NaN;
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=NaN;
                NeuronStruct.hdRMapsStill(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=NaN;
                if iTimeSegment > 1
                    NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,1)=NaN;
                    NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,2)=NaN;
                    NeuronStruct.hdRMapsCleanRunsOnly(:,iOverallNeuron+iNeuron-1,iTimeSegment,3)=NaN;
                end
            end
        end
    end
    iOverallNeuron = iOverallNeuron+nCells;
    %disp(iRec);
end
end