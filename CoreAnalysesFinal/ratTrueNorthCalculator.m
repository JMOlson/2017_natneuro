function [ dataStruct ] = ratTrueNorthCalculator( dataStruct )
%RATTRUENORTHCALCULATOR Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.

nRecs = dataStruct.nRecs;
for iRec = 1:nRecs
    dataFile = dataStruct.recFile{iRec};
    processedDvtFile = dataStruct.dvtFile{iRec};
    load(deblank(dataFile),'pathList','runPosMarkers','pos','nRuns','PathTemplate');
    load(deblank(processedDvtFile),'HDRadians');
    
    if abs(length(HDRadians)-length(pos)) > 20
        display('Wrong dvt file!?!?!')
        return
    end
    % Average direction from straight segments from runs 9 and 10. Define
    % this as "rat true north" for that recording. Using bins 90-100.
    straightNorthBins = 90:100;
    
    ninePathIndex = find(pathList == 9);
    tenPathIndex = find(pathList == 10);
    nineLineMarks = [runPosMarkers{ninePathIndex,1},...
        runPosMarkers{ninePathIndex,2}]; % Start and finish line markers.
    tenLineMarks = [runPosMarkers{tenPathIndex,1},...
        runPosMarkers{tenPathIndex,2}]; % Start and finish line markers.
    nNineRuns = nRuns(ninePathIndex);
    nTenRuns = nRuns(tenPathIndex);
    templateNine = PathTemplate{ninePathIndex};
    templateTen = PathTemplate{tenPathIndex};
    hdSampleIndex = 0;
    hdSample = [];
    for iRun=1:nNineRuns
        allXsThisRun = pos(nineLineMarks(iRun,1):nineLineMarks(iRun,2),2);
        allYsThisRun = pos(nineLineMarks(iRun,1):nineLineMarks(iRun,2),3);
        for iPoint = 1:length(allXsThisRun)
            if allXsThisRun(iPoint)>1 && allYsThisRun(iPoint)>1
                templateDist = dist(...
                    [allXsThisRun(iPoint),allYsThisRun(iPoint)],templateNine(:,2:3)');
                [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                if ~isempty(find(nearestPointInd(1) == straightNorthBins,1))
                    if ~isnan(HDRadians(nineLineMarks(iRun,1)+iPoint-1))
                        hdSampleIndex = hdSampleIndex+1;
                        hdSample(hdSampleIndex) = ...
                            HDRadians(nineLineMarks(iRun,1)+iPoint-1);
                    end
                end
            end
        end
    end
    for iRun=1:nTenRuns
        allXsThisRun = pos(tenLineMarks(iRun,1):tenLineMarks(iRun,2),2);
        allYsThisRun = pos(tenLineMarks(iRun,1):tenLineMarks(iRun,2),3);
        for iPoint = 1:length(allXsThisRun)
            if allXsThisRun(iPoint)>1 && allYsThisRun(iPoint)>1
                templateDist = dist(...
                    [allXsThisRun(iPoint),allYsThisRun(iPoint)],templateTen(:,2:3)');
                [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                if ~isempty(find(nearestPointInd(1)==straightNorthBins,1))
                    if ~isnan(HDRadians(tenLineMarks(iRun,1)+iPoint-1))
                        hdSampleIndex = hdSampleIndex+1;
                        hdSample(hdSampleIndex) = ...
                            HDRadians(tenLineMarks(iRun,1)+iPoint-1);
                    end
                end
            end
        end
    end
    dataStruct.trueNorthRadians(iRec) = mean(hdSample);
    dataStruct.northSamples{iRec} = hdSample;
%     hist(hdSample,[-pi:pi/16:pi]);
%     disp(iRec);
end

end
