function [ DataStruct, NeuronStruct] = hdByPathSpatialReliabilityCalculator(DataStruct, NeuronStruct)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015

% paths 1-4 are of length 140, paths 9 and 10 are 197.
% So each neuron needs a matrix of 4*140+2*197 x 360/binSizeDegrees.
% I need to lump the spaces in together correctly - and ditch the first and
% last bin of each run.
percentBelow = 1/2;

sampleRate = DataStruct.sampleRate(1); % all recording sample rates are the same.
binSizeDegrees = DataStruct.binSizeDegrees;
nHDBins = 360./binSizeDegrees;
nNeurons = length(NeuronStruct.neuronIndex);
timeStamps = DataStruct.timeStamps{1}; % all number of time segments are the same.
nTimeSegments = length(timeStamps)/2;
for iTimeSegment = 1:nTimeSegments-1
spikes = zeros(4*140+2*197,nHDBins,nNeurons);
occs = spikes;
isOcc = false(size(occs));
rates = nan(size(occs));
iOverall = 1;
for i = 2:46 % all four
    spikes(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i:140:i+3*140],:,:,iTimeSegment,2),1);
    occs(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i:140:i+3*140],:,:,iTimeSegment,3),1);
    iOverall = iOverall+1;
end
for i = 47:113 % 1 and 2
    spikes(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i,i+140],:,:,iTimeSegment,2),1);
    occs(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i,i+140],:,:,iTimeSegment,3),1);
    iOverall = iOverall+1;
end
for i = 47:113 % 3 and 4
    spikes(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i+2*140,i+3*140],:,:,iTimeSegment,2),1);
    occs(iOverall,:,:) = ...
        nansum(NeuronStruct.pathByHDRMaps([i+2*140,i+3*140],:,:,iTimeSegment,3),1);
    iOverall = iOverall+1;
end
for i = 114:139 % 1
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
for i = 114:139 % 2
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+140,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+140,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
for i = 114:139 % 3
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+2*140,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+2*140,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
for i = 114:139 % 4
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+3*140,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+3*140,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
for i = 2:196 % 9
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+4*140,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+4*140,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
for i = 2:196 % 10
    spikes(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+4*140+197,:,:,iTimeSegment,2);
    occs(iOverall,:,:) = ...
        NeuronStruct.pathByHDRMaps(i+4*140+197,:,:,iTimeSegment,3);
    iOverall = iOverall+1;
end
isOcc = occs > 0;
rates(isOcc) = (spikes(isOcc)./occs(isOcc)).*sampleRate;

NeuronStruct.pathByHDRMapsCollapsed(:,:,:,iTimeSegment,1) = rates;
NeuronStruct.pathByHDRMapsCollapsed(:,:,:,iTimeSegment,2) = spikes;
NeuronStruct.pathByHDRMapsCollapsed(:,:,:,iTimeSegment,3) = occs;
end

DataStruct.HDSpatialReliabilityPercentOfMeanThreshold = percentBelow; % XX percent of mean is included as reliable.
sampleRate = 60;
cutoffVal = sampleRate/6; % 1/6th of second
DataStruct.HDSpatialMinimumSampling = cutoffVal;
rates = NeuronStruct.pathByHDRMapsCollapsed(:,:,:,:,1);
sufficientSampling = NeuronStruct.pathByHDRMapsCollapsed(:,:,:,:,3)>=cutoffVal;
rates(~sufficientSampling) = NaN;
meanHDFiring = NeuronStruct.hdRMapsCleanRunsOnly(:,:,2:nTimeSegments,1); % This should just be mean firing rate - pull from hdrmaps. 
NeuronStruct.HDPathSpatialReliability = squeeze(nansum(rates>repmat(reshape(meanHDFiring*percentBelow,[1,nHDBins,nNeurons,nTimeSegments-1]),...
    [size(rates,1),1,1,1])...
    & sufficientSampling,1)./nansum(sufficientSampling,1));
NeuronStruct.HDPathSpatialReliabilitySampling = squeeze(nansum(sufficientSampling,1));
end

