function [DataStruct, twoDRMaps, twoDRMapsStill] = twoDRateMapper( DataStruct, NeuronStruct, ...
    iTimeSegment, binSizePixels, xPixelSize, yPixelSize, isRunningFlag)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015

% 6.666 pixels is ~2cm square bins for platform - RECOMMENDED
% probably 6 pixels would be about the same distance for the track, since
% it is about 1 brick lower. Try 6 pixels - RECOMMENDED
% For the platform, 300 by 300 pixelSize variables gives a 45 bin by 45 bin square.

DataStruct.twoDBinSizePixels(iTimeSegment) = binSizePixels;
DataStruct.twoDXPixelSize(iTimeSegment) = xPixelSize;
DataStruct.twoDYPixelSize(iTimeSegment) = yPixelSize;

%For regular datasets, 
maxX = 650;
maxY = 500;
% 1 Codes for use all of the data.
if xPixelSize == 1
	xPixelSize = maxX;  
end
if yPixelSize == 1
    yPixelSize = maxY;
end

xSize = ceil(xPixelSize/binSizePixels);
ySize = ceil(yPixelSize/binSizePixels);
twoDRMaps = nan(xSize,ySize,length(NeuronStruct.posNSpikesAll));
if isRunningFlag
    twoDRMapsStill = nan(xSize,ySize,length(NeuronStruct.posNSpikesAll));
else
    twoDRMapsStill = [];
end
for iNeuron = 1:length(NeuronStruct.posNSpikesAll)
    iRec = NeuronStruct.recCount(iNeuron);
    iNeuNSpikes = NeuronStruct.posNSpikesAll{iNeuron};
    
    sampleRate = DataStruct.sampleRate(iRec);
    timeStamps = DataStruct.timeStamps{iRec};

    dataFile = DataStruct.recFile{iRec};
    load(dataFile,'narrowGaussianNormed','pos');

    for iAnalysis = 1:isRunningFlag+1 % so 1 or 2, as the flag is 0 or 1.
    posNSpikesRelevant = iNeuNSpikes(...
        timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
    posRelevant = pos(timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
    
    if iAnalysis == 1 && isRunningFlag
        runVals = false(timeStamps(2)-timeStamps(1),1);
        runVals(DataStruct.runningLineIndicesBySegment{iTimeSegment,iRec}) = true;
        posNSpikesRelevant = posNSpikesRelevant(runVals);
        posRelevant = posRelevant(runVals,:);
    elseif iAnalysis == 2 && isRunningFlag
        posNSpikesRelevant = posNSpikesRelevant(~runVals);
        posRelevant = posRelevant(~runVals,:);
    end
    
    % Binning the twoD bins - perhaps downsampling.
    if xPixelSize == maxX
        xOffset = 0;
    else
        plotMinX = nanmean(posRelevant(posRelevant(:,2)>1,2))-xPixelSize/2;
        xOffset = ceil(plotMinX/binSizePixels)-1;
    end
    if yPixelSize == maxY
        yOffset = 0;
    else
        plotMinY = nanmean(posRelevant(posRelevant(:,2)>1,3))-yPixelSize/2;
        yOffset = floor(plotMinY/binSizePixels)-1;
    end
    binX = ceil(posRelevant(:,2)/binSizePixels)-xOffset;
    binY = ceil(posRelevant(:,3)/binSizePixels)-yOffset;
    
    sampleOutOfGrid = binX <= 0 | binY <= 0 | ...
        binX > xPixelSize/binSizePixels | binY > yPixelSize/binSizePixels;
    binXGood = binX(~sampleOutOfGrid); % Throwing out points where it would be outside the grid.
    binYGood = binY(~sampleOutOfGrid); % Throwing out points where it would be outside the grid.
    posNSpikesGood = posNSpikesRelevant(~sampleOutOfGrid);
    
    spikes = zeros(xSize,ySize);
    occs = spikes;
    isOcc = false(size(occs));
    rates = nan(size(occs));
    for iSample = 1:length(posNSpikesGood)
        occs(binXGood(iSample),binYGood(iSample)) = ...
            occs(binXGood(iSample),binYGood(iSample))+1;
        
        spikes(binXGood(iSample),binYGood(iSample)) = ...
            spikes(binXGood(iSample),binYGood(iSample)) +...
            posNSpikesGood(iSample);
    end
    isOcc = occs > 0;
    rates(isOcc) = (spikes(isOcc)./occs(isOcc)).*sampleRate;
    
    if isRunningFlag
        if iAnalysis == 1
            twoDRMaps(:,:,iNeuron) = rates;
%             twoDRMaps(:,:,iNeuron,1) = rates;
%             twoDRMaps(:,:,iNeuron,2) = spikes;
%             twoDRMaps(:,:,iNeuron,3) = occs;
        else
            twoDRMapsStill(:,:,iNeuron) = rates;
%             twoDRMapsStill(:,:,iNeuron,1) = rates;
%             twoDRMapsStill(:,:,iNeuron,2) = spikes;
%             twoDRMapsStill(:,:,iNeuron,3) = occs;
        end
    else
        twoDRMaps(:,:,iNeuron) = rates;
%         twoDRMaps(:,:,iNeuron,1) = rates;
%         twoDRMaps(:,:,iNeuron,2) = spikes;
%         twoDRMaps(:,:,iNeuron,3) = occs;
    end
    end
end

