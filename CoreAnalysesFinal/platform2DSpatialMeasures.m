function [ DataStruct, NeuronStruct ] = platform2DSpatialMeasures( DataStruct, NeuronStruct, mapsToUse)
%NAMEHERE Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, March 2016.

for iOverallNeuron = 1:length(NeuronStruct.neuronIndex)
    iRec = NeuronStruct.recCount(iOverallNeuron);
    sampleRate = DataStruct.sampleRate(iRec);

    dataFile = DataStruct.recFile{iRec};
    load(dataFile,'pixelDvt');

    minOccForSpatialStats = sampleRate/10;  % 1/10 of a second.
    DataStruct.minOccForStatInclusion(iRec) = minOccForSpatialStats; 
    
    binOcc = mapsToUse(:,:,iOverallNeuron,3);
    nOccs = sum(sum(binOcc(binOcc >= minOccForSpatialStats))); % Only include places that were occupied at least x times.
    pBin = binOcc./nOccs;
    pBin(binOcc < minOccForSpatialStats) = 0; % Only include places that were occupied at least x times.
    goodBins = (pBin ~= 0);
    binFRs = mapsToUse(:,:,iOverallNeuron,1);
    if sum(sum(goodBins))
        meanFR = sum(sum(binFRs(goodBins).*pBin(goodBins)));
        NeuronStruct.platformMeanFR(iOverallNeuron) = meanFR;
        NeuronStruct.platformMaxFR(iOverallNeuron) = max(max(binFRs(goodBins)));
    else
        meanFR = 0;
        NeuronStruct.platformMeanFR(iOverallNeuron) = meanFR;
        NeuronStruct.platformMaxFR(iOverallNeuron) = 0;
    end
    if any(isnan(binFRs(goodBins)))
        disp('Nan in FR where there shouldn''t be - see line 40');
        pause;
    end
    
    if meanFR == 0
        NeuronStruct.platformSpatialInfoPerSpikeInBits(iOverallNeuron) = NaN;
        NeuronStruct.platformSpatialInfoInBits(iOverallNeuron) = NaN;
        NeuronStruct.platformSpatialSparsity(iOverallNeuron) = NaN;
        NeuronStruct.platformSpatialCoherence(iOverallNeuron) = NaN;
    else
        %spatial information per spike - 1993 Skaggs et al. NIPS
        tmpSpInfo = (pBin(goodBins).*...
            binFRs(goodBins)/meanFR).*...
            log2(binFRs(goodBins)/meanFR);
        NeuronStruct.platformSpatialInfoPerSpikeInBits(iOverallNeuron) = ...
            sum(sum(tmpSpInfo(~isnan(tmpSpInfo))));
        
        %spatial information per second- 1993 Skaggs et al. NIPS
        tmpSpInfo = (pBin(goodBins).*...
            binFRs(goodBins)).*...
            log2(binFRs(goodBins)/meanFR);
        NeuronStruct.platformSpatialInfoInBits(iOverallNeuron) = ...
            sum(sum(tmpSpInfo(~isnan(tmpSpInfo))));
        
        % spatial sparsity - 1996 Skaggs  et al. hippocampus
        NeuronStruct.platformSpatialSparsity(iOverallNeuron) = ...
            nansum(nansum(pBin.*binFRs))^2/...
            nansum(nansum(pBin.*binFRs.^2));
        
        % Spatial Coherence - from Andy - a la Kubie Muller Bostock 1990
        data = squeeze(mapsToUse(:,:,iOverallNeuron,1));
        clear neighborFR actualFR;
        countBin = 0;
        for x = 2:size(data,1)-1;
            for y = 2:size(data,2)-1;
                if binOcc(x,y) ~= 0
                    countBin = countBin + 1;
                    neighborhood = data(x-1:x+1,y-1:y+1);
                    neighborFR(countBin) = nanmean(neighborhood([1:4,6:9]));
                    actualFR(countBin) = data(x,y);
                end
            end
        end
        [NeuronStruct.platformSpatialCoherence(iOverallNeuron), NeuronStruct.platformSpatialCoherencePVal(iOverallNeuron)] = ...
            corr(actualFR',neighborFR','rows','complete','type','Spearman','tail','right');
    end

end
    

        


