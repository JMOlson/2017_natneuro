function [ DataStruct, NeuronStruct ] = mapSpikesAndLocsToSamples( DataStruct, NeuronStruct)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015


% Create a dvt w/ nSpikes at that sample and the path and bin of the
% sample. Can then get HD, vel, etc, for each sample from the vectors of
% corresponding length already made.

nRecs = DataStruct.nRecs;
iOverallNeuron = 1;
for iRec = 1:nRecs
    dataFile = DataStruct.recFile{iRec};
    processedDvtFile = DataStruct.dvtFile{iRec};
    subNeuronInds = DataStruct.neuronList{iRec};
    load(dataFile,'pos','allTfiles','sampleRate','nRuns',...
        'runPosMarkers','pathList','PathTemplate'); %,'filterValuesNormed','filterMedian'); % last 2 for linear rmaps
    load(processedDvtFile,'processedDVT','HDRadians');

    nCells = length(subNeuronInds);
    
    %% Check that both processedDvt and the spikes are all sorted ascending and
    % pos and processedDvt are equal.
    if any(pos(:,1) ~= processedDVT(:,2))
        disp('Pos times doesn''t equal processedDvt times. Yikes!');
        return
    end
    if any(diff(processedDVT(:,2))<0)
        disp(['Processed dvt is not in chronological order.']);
        return
    end
    for iCell = 1:nCells
        if any(diff(allTfiles{iCell})<0)
            disp(['Neuron ', num2str(iCell),' spikes are not in chronological order.']);
            return;
        end
    end


    %% Calc nSpikes for each sample.
    posNSpikes = zeros(size(pos,1),nCells);
    nSamples = size(pos,1);
    for iCell=1:nCells
        tfile = allTfiles{subNeuronInds(iCell)};
        posIndex = 1;
        for iSpike =1:length(tfile)
            % Cycle through dvt until you hit the time that this spike goes to.
            while tfile(iSpike) > (processedDVT(posIndex,2)+(1/(2*sampleRate))) &&...
                    posIndex <= nSamples
                posIndex = posIndex+1;
            end
            if posIndex > nSamples
                disp('Spikes exist after the last sample!');
                return
            else
                posNSpikes(posIndex,iCell) = posNSpikes(posIndex,iCell) + 1;
            end
        end
    end

    %% Find the closest bin for each path (includes commented out Linear RMaps)
    posPathAndBin = nan(size(pos,1),2);
    for iPath = 1:length(pathList)
        template = PathTemplate{iPath};
        thisPathPosMarkers = [runPosMarkers{iPath,1},runPosMarkers{iPath,2}];
        % Firing Rate Vectors
        % 4th dimension is firing rate, spikes, occ
        linearRates=zeros(nCells,length(template),nRuns(iPath),3);
        for iRun=1:length(thisPathPosMarkers(:,1))  % each outbound run
            for iPoint = thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2)  % each time-point during this run
                if pos(iPoint,2)>1 && pos(iPoint,3)>1  % if we have valid tracking
                    
                    % Find the closest bin for the current path.
                    templateDist = dist(pos(iPoint,2:3),template(:,2:3)');
                    [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                    posPathAndBin(iPoint,:) = [pathList(iPath),nearestPointInd(1)];
                    spikes = posNSpikes(iPoint,:);
            
     %% linear ratemap portion
%             if nearestPointInd <= filterMedian
%                 occIndFiltered = [1:nearestPointInd(1)+filterMedian];
%                 linearRates(:,occIndFiltered,iRun,3) =...
%                     linearRates(:,occIndFiltered,iRun,3)+...
%                     repmat(filterValuesNormed(end-length(occIndFiltered)+1:end),nCells,1);
%                 linearRates(:,occIndFiltered,iRun,2) =...
%                     linearRates(:,occIndFiltered,iRun,2)+...
%                     repmat(spikes',1,length(occIndFiltered)).*...
%                     repmat(filterValuesNormed(end-length(occIndFiltered)+1:end),nCells,1);
%             elseif nearestPointInd >= length(template)-filterMedian
%                 occIndFiltered = [nearestPointInd(1)-filterMedian:length(template)];
%                 linearRates(:,occIndFiltered,iRun,3) =...
%                     linearRates(:,occIndFiltered,iRun,3)+...
%                     repmat(filterValuesNormed(1:length(occIndFiltered)),nCells,1);
%                 linearRates(:,occIndFiltered,iRun,2) =...
%                     linearRates(:,occIndFiltered,iRun,2)+...
%                     repmat(spikes',1,length(occIndFiltered)).*...
%                     repmat(filterValuesNormed(1:length(occIndFiltered)),nCells,1);
%             else
%                 occIndFiltered = ...
%                     [nearestPointInd(1)-filterMedian:...
%                     nearestPointInd(1)+filterMedian];
%                 linearRates(:,occIndFiltered,iRun,3) =...
%                      linearRates(:,occIndFiltered,iRun,3)+repmat(filterValuesNormed,nCells,1);
%                 linearRates(:,occIndFiltered,iRun,2) =...
%                     linearRates(:,occIndFiltered,iRun,2)+...
%                     repmat(spikes',1,length(occIndFiltered)).*...
%                     repmat(filterValuesNormed,nCells,1);
%             end
                end
            end
        end

        
        %% %calc firing rates for each run
        % rates = zeros(length(template),nRuns(iPath));
        % meanLinearRates=zeros(nCells,length(template),3);
        % for iCell = 1:nCells
        %     rates = rates*0;
        %     isOcc = squeeze(linearRates(iCell,:,:,3)>0);
        %     spikes = squeeze(linearRates(iCell,:,:,2));
        %     occs = squeeze(linearRates(iCell,:,:,3));
        %     rates(isOcc) = sampleRate*spikes(isOcc)./occs(isOcc);
        %     linearRates(iCell,:,:,1) = rates;
        %
        %     %calc mean rates across laps
        %     meanLinearRates(iCell,:,1) = mean(rates,2);
        %     meanLinearRates(iCell,:,2) = std(rates,0,2);
        %     meanLinearRates(iCell,:,3) = std(rates,0,2)/(sqrt(nRuns(iPath)-1));
        % end
        %
        % LinearRates{iPath} = linearRates;
        % MLR{iPath} = meanLinearRates;
    end
    for iCell = 1:nCells
        NeuronStruct.posNSpikesAll{iOverallNeuron+iCell-1} = posNSpikes(:,iCell);
        NeuronStruct.posPathAndBinAll{iOverallNeuron+iCell-1} = posPathAndBin;
        NeuronStruct.posHDRadiansAll{iOverallNeuron+iCell-1} = HDRadians;
        NeuronStruct.posHDRadiansAllCorrected{iOverallNeuron+iCell-1} = HDRadians-DataStruct.trueNorthRadians(iRec);
    end

    iOverallNeuron = iOverallNeuron+nCells;
end
