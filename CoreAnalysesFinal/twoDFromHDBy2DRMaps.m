function [ twoDRMaps] = twoDFromhdBy2DRMaps(hdBy2DRMaps,sampleRate)
%NAMEHERE Calculates offset of animal's lights orientation
%from the camera "north" direction.
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.

twoDSpikes = squeeze(nansum(hdBy2DRMaps(:,:,:,:,2),3));
twoDOccs = squeeze(nansum(hdBy2DRMaps(:,:,:,:,3),3));
twoDRates = nan(size(twoDOccs));

isOcc = twoDOccs>0;
twoDRates(isOcc) = (twoDSpikes(isOcc)./twoDOccs(isOcc))*sampleRate;

twoDRMaps(:,:,:,1)=twoDRates;
twoDRMaps(:,:,:,2)=twoDSpikes;
twoDRMaps(:,:,:,3)=twoDOccs;
end


                
