function [ DataStruct, NeuronStruct ] = hdByPathBinsRateMapper( DataStruct, NeuronStruct, nTimeSegments )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015

% paths 1-4 are of length 140, paths 9 and 10 are 197.
% So each neuron needs a matrix of 4*140+2*197 x 360/binSizeDegrees.
binSizeDegrees = DataStruct.binSizeDegrees;
nHDBins = NeuronStruct.HDMapsNBins;

% data is # of unique path bins x angle bins x nNeurons x 3
% (rate,spikes,occ)
NeuronStruct.pathByHDRMaps = nan(4*140+2*197, 360./binSizeDegrees,length(NeuronStruct.posNSpikesAll),nTimeSegments-1,3);
for iNeuron = 1:length(NeuronStruct.posNSpikesAll)
    iNeuHDRadians = NeuronStruct.posHDRadiansAllCorrected{iNeuron};
    iNeuPath = NeuronStruct.posPathAndBinAll{iNeuron}(:,1);
    iNeuPathBin = NeuronStruct.posPathAndBinAll{iNeuron}(:,2);
    iNeuNSpikes = NeuronStruct.posNSpikesAll{iNeuron};
    iRec = NeuronStruct.recCount(iNeuron);
    sampleRate = DataStruct.sampleRate(iRec);
    timeStamps = DataStruct.timeStamps{iRec};
    for iTimeSegment = 2:nTimeSegments
        if iTimeSegment == 2
            pathList = [1,2,3,4,9,10]; % Only non rotation paths.
            nPathBins = [140,140,140,140,197,197];
        elseif iTimeSegment == 3
            pathList = [11,12,13,14,19,20]; % Only rotation paths.
            nPathBins = [140,140,140,140,197,197];
        end
        hdRadiansRelevant = iNeuHDRadians(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
        posPathRelevant = iNeuPath(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
        posPathBinRelevant = iNeuPathBin(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
        posNSpikesRelevant = iNeuNSpikes(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
        
        spikes = zeros(4*140+2*197, nHDBins);
        occs = spikes;
        isOcc = false(size(occs));
        rates = nan(size(occs));
        
        nRunningTotalPathBins = 0;
        for iPath = 1:length(pathList)
            currPathID = pathList(iPath);
            currPathSamples = posPathRelevant==currPathID;
            currPathBin = posPathBinRelevant(currPathSamples);
            currPathNSpikes = posNSpikesRelevant(currPathSamples);
            currPathHDRadians = hdRadiansRelevant(currPathSamples);
            currPathHDRadiansBinned = round(mod(radtodeg(currPathHDRadians),360)/binSizeDegrees);
            currPathHDRadiansBinned(currPathHDRadiansBinned==0) = nHDBins;
            currPathHDRadiansBinned(isnan(currPathHDRadians)) = NaN;
            
            iPathBins = nPathBins(iPath);
            for iSample = 1:length(currPathBin)
                if ~isnan(currPathHDRadiansBinned(iSample))
                occs(currPathBin(iSample)+nRunningTotalPathBins,currPathHDRadiansBinned(iSample)) = ...
                    occs(currPathBin(iSample)+nRunningTotalPathBins,currPathHDRadiansBinned(iSample))+1;
                spikes(currPathBin(iSample)+nRunningTotalPathBins,currPathHDRadiansBinned(iSample)) = ...
                    spikes(currPathBin(iSample)+nRunningTotalPathBins,currPathHDRadiansBinned(iSample)) +...
                    currPathNSpikes(iSample);
                end
            end
            nRunningTotalPathBins = nRunningTotalPathBins + iPathBins;
        end
        isOcc = occs > 0;
        rates(isOcc) = (spikes(isOcc)./occs(isOcc)).*sampleRate;
        
        NeuronStruct.pathByHDRMaps(:,:,iNeuron,iTimeSegment-1,1) = rates;
        NeuronStruct.pathByHDRMaps(:,:,iNeuron,iTimeSegment-1,2) = spikes;
        NeuronStruct.pathByHDRMaps(:,:,iNeuron,iTimeSegment-1,3) = occs;
    end
end
end

