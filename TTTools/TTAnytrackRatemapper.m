function  TTAnytrackRatemapper(saveFile, binTemplateMatrix, varargin)
% TTANYTRACKRATEMAPPER2014 Creates rate maps for all paths of the triple t
% maze.
%       TTAnytrackRatemapper2014(saveFile,binTemplateMatrix) blah blah blah
%       Comments displayed at the command line in response
%       to the help command. add a which light flag to this when you make
%       it a function. right now it just determines the "best" light. This
%       uses inpaint nans and I should credit that author here. put the
%       bins for each segment and each run into a matrix and input into the
%       fn. then i wont have to prompt user.

%% Prompt for necessary files and load into workspace.
%List of files that contain spiking data exported from neuroexplorer.
[tfileFileName, tfilePathName] = uigetfile('*.txt', 'Choose the tfile list file.');
tfileFID = fopen(fullfile(tfilePathName,tfileFileName));
TfileCell = textscan(tfileFID,'%s');
tfileList = TfileCell{1};
fclose(tfileFID);

%behavioral scoring - spiral events and pixelDvt
[runsFileName, runsPathName] = uigetfile('*.mat', 'Choose the spiral events file.');
load(fullfile(runsPathName,runsFileName));

allTfiles = cell(length(tfileList),1);
for iTfile=1:length(allTfiles)
    allTfiles{iTfile} = load(tfileList{iTfile});
end
nCells=length(allTfiles);

pos(:,1) = pixelDvt(:,2);
% [leastBadSpots, leastBadSpotsInd] = min(sum(pixelDvt(:,3:end-1)==0));
% bestLight = leastBadSpotsInd(1)+2;
pos(:,2:3) = pixelDvt(:,end-2:end-1); %Use the mashup light.
clear tfileFID TfileCell iTfile leastBadSpots*

%% Creation of smoothing functions
%2D ratemap smoothing function **************************************
halfNarrow = 7;
narrowStdev = 3.5;
[xGridVals, yGridVals]=meshgrid(-halfNarrow:1:halfNarrow);
narrowGaussian = exp(-0.5 .* (xGridVals.^2+yGridVals.^2)/narrowStdev^2)/(narrowStdev*(sqrt(2*pi)));
narrowGaussianNormed=narrowGaussian./sum(sum(narrowGaussian));

% Create a linear ratemap smoothing filter.
filterWidth = 13; %Must be odd.
filterMedian = (filterWidth-1)/2;
filterSigma = 1;
filterBins=0:1:filterWidth-1;
filterValues=gaussmf(filterBins,[filterSigma, filterMedian]);
filterValuesNormed=filterValues/sum(filterValues);
clear xGridVals yGridVals filterBins

%% Create lists for each run type.

% % In case you need to change a button code before processing the runs,
% % use this code as a model to how.
% for i=1:length(spiral_events)/2
%     if (spiral_events(2*i-1,3)==1) && (spiral_events(2*i,3)==4)
%         spiral_events(2*i-1,3)=11;
%         spiral_events(2*i,3)=12;
%     end
% end

pathList = unique(spiral_events(1:end,3));
pathList = pathList(pathList>0 & pathList<99); % Good runs only.
nPaths = length(pathList);
runStarts = cell(nPaths,1);
runEnds = cell(nPaths,1);
runPosMarkers = cell(nPaths,2);
runTimeMarkers = cell(nPaths,2);
nRuns = zeros(nPaths,1);

for iPath = 1:nPaths
    runStarts{iPath} = find(spiral_events(:,3) == pathList(iPath));
    runEnds{iPath} = find(spiral_events(:,3) == pathList(iPath))+1;
    runPosMarkers{iPath,1} = spiral_events(runStarts{iPath},1);
    runPosMarkers{iPath,2} = spiral_events(runEnds{iPath},1);
    runTimeMarkers{iPath,1} = spiral_events(runStarts{iPath},2);
    runTimeMarkers{iPath,2} = spiral_events(runEnds{iPath},2);
    nRuns(iPath) = length(runPosMarkers{iPath,1});
end
clear iPath runStarts runEnds

%% Create master template
if ~isempty(varargin)
    masterTemplate = varargin{1};
    nMasterPoints = size(masterTemplate,1);
else
    masterTemplate = [];
    nMasterPoints = 0;
end
if nMasterPoints == 0
    figure(1);
    clf;
    hold on;
    
    % Grab only the run portions of the data.
    pathsToGrab = [1 2 3 4 9 10 11 12 13 14 19 20];
    changeStarts = [find(diff(pixelDvt(:,end)))];
    changeEnds = [changeStarts(2:end);length(pixelDvt)];
    wantedStarts = false(length(changeStarts),1);
    for iPathToPlot = 1:length(pathsToGrab)
        wantedStarts = (pixelDvt(changeEnds,end)==pathsToGrab(iPathToPlot))|...
            wantedStarts;
    end
    pathsWanted = find(wantedStarts);
    changeStarts = changeStarts(pathsWanted);
    changeEnds = changeEnds(pathsWanted);
    trimmedPixelDvt = [];
    for iSegment = 1:length(changeStarts);
        trimmedPixelDvt = [trimmedPixelDvt;...
            pixelDvt(changeStarts(iSegment):changeEnds(iSegment),:)];
    end
    plot(trimmedPixelDvt(:,9),trimmedPixelDvt(:,10)); %Don't plot missing data or non-run data.
    clear changeStarts changeEnds wantedStarts pathsWanted pathsToGrab iPathToPlot trimmedPixelDvt
    % plot(pos(pos(:,2)>1,2),pos(pos(:,2)>1,3)); %Don't plot missing data.
    
    
    newInput = 0;
    while ~isempty(newInput)
        % ask for one input per loop, then immediately plot that point before
        % asking for the next input. Keep looping until one of the inputs is
        % empty (meaning the user pressed 'enter').
        if strcmp(version(),'8.5.0.197613 (R2015a)')
            newInput = finput(1);
        else
            newInput = ginput(1);
        end
        if ~isempty(newInput)
            nMasterPoints = nMasterPoints + 1;
            masterTemplate(nMasterPoints,:) = round(newInput);
            %             load('fakeClicks'); % Testing Line
            %             masterTemplate(nMasterPoints,:) = Clicks{iPath}(nMasterPoints,:); % Testing Line
            if nMasterPoints>1
                % find the Euclidean distance between the last two input points
                eucDist = dist(masterTemplate(nMasterPoints-1,:),masterTemplate(nMasterPoints,:)');
                if eucDist < 5 %Double click - ignore second.
                    nMasterPoints = nMasterPoints-1;
                    masterTemplate = masterTemplate(nMasterPoints,:);
                else
                    figure(1); hold on;
                    % plot the user-defined points in a distinct color
                    plot(masterTemplate(:,1),masterTemplate(:,2),'r+')
                end
            end
            % plot the most recent input point even more distinctly
            plot(newInput(1),newInput(2),'c*');
        end
    end
    
    save masterTemplate masterTemplate
end
%% Create templates and linear ratemaps
defaultBinSize = 3;
sampleRate = round(1/(pixelDvt(2,2)-pixelDvt(1,2)));
for iPath = 1:nPaths
    thisPathPosMarkers = [runPosMarkers{iPath,1},runPosMarkers{iPath,2}];
    thisPathTimeMarkers = [runTimeMarkers{iPath,1},runTimeMarkers{iPath,2}];
    
    figure(1)
    clf;
    hold on
    % Plot the position data for the path.
    for iRun=1:nRuns(iPath)
        % Don't plot lost position spots.
        allXsThisRun = pos(thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2),2);
        allYsThisRun = pos(thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2),3);
        %         plot(allXsThisRun,allYsThisRun); % See where you have tracking gaps.
        plot(allXsThisRun(allXsThisRun>1),allYsThisRun(allXsThisRun>1),'b');
        % Plot the master template points.
        plot(masterTemplate(:,1),masterTemplate(:,2),'r+');
    end
    
    newInput = 0;
    nPts = 0;
    nBinsThisSegment = [];
    clickDist = [];
    thisPathBinXs = {};
    thisPathBinYs = {};
    while ~isempty(newInput)
        % ask for one input per loop, then immediately plot that point before
        % asking for the next input. Keep looping until one of the inputs is
        % empty (meaning the user pressed 'enter').
        newInput = ginput(1);
        if ~isempty(newInput)
            nPts = nPts + 1;
            [~,closestTemplatePoint(iPath,nPts)] = min(dist(newInput,masterTemplate'));
            coords(nPts,:) = masterTemplate(closestTemplatePoint(iPath,nPts),:);
            %             load('fakeClicks'); % Testing Line
            %             coords(nPts,:) = Clicks{iPath}(nPts,:); % Testing Line
            if nPts>1
                if closestTemplatePoint(iPath,nPts) == closestTemplatePoint(iPath,nPts-1) %Double click - ignore second.
                    nPts = nPts-1;
                    closestTemplatePoint(iPath,nPts) = 0;
                else
                    %  nBinsThisSegment = str2double(inputdlg('How many bins between the last 2 points?'));
                    if ~max(binTemplateMatrix(:))
                        nBinsThisSegment = round(dist(coords(nPts,:),coords(nPts-1,:)')/defaultBinSize);
                    else
                        nBinsThisSegment = binTemplateMatrix(pathList(iPath),nPts-1);
                    end
                    % interpolate and plot the current segment of the template
                    % find the Euclidean distance between the last two input points
                    clickDist(nPts-1,1) = coords(nPts,1) - coords(nPts-1,1);
                    clickDist(nPts-1,2) = coords(nPts,2) - coords(nPts-1,2);
                    stepSize = clickDist(nPts-1,2)*(1/nBinsThisSegment); %Find y stepsize
                    thisPathBinYs{nPts-1} = coords(nPts-1,2):stepSize:coords(nPts,2);
                    thisPathBinXs{nPts-1} = interp1(coords(nPts-1:nPts,2),...
                        coords(nPts-1:nPts,1),thisPathBinYs{nPts-1});
                    % +0.05.*randn(2,1) didn't fix our interp horizontal row problem.
                    
                    figure(1); hold on;
                    plot(thisPathBinXs{nPts-1},thisPathBinYs{nPts-1},'k*');
                end
            end
            % plot the most recent click point even more distinctly
            plot(coords(nPts,1),coords(nPts,2),'c*');
        end
    end
    Clicks{iPath} = coords;
    
    % Removes duplicate bin issue. - We create bin centers, and we create
    % one for the last bin of each click and the first bin of each click in
    % the same place. This removes the duplicate. Note: Also removes
    % last bin on last click, because there is no duplicate here.
    for iBinLine=1:length(thisPathBinXs) % is clicks-1, i.e. (nPts-1)
        thisPathBinXs{iBinLine} = thisPathBinXs{iBinLine}(1:end-1);
        thisPathBinYs{iBinLine} = thisPathBinYs{iBinLine}(1:end-1);
        nBins(iBinLine,iPath) = length(thisPathBinXs{iBinLine});
        if iBinLine == 1;
            cumSumNBins{iPath}(iBinLine) = nBins(iBinLine,iPath);
        else
            cumSumNBins{iPath}(iBinLine) = cumSumNBins{iPath}(iBinLine-1) +...
                nBins(iBinLine,iPath);
        end
    end
    
    iBinAtEachClick = [0,cumSumNBins{iPath}];
    
    template = zeros(iBinAtEachClick(end)-1,3); %Because we cut the last bin off.
    template(:,1)=1:length(template);
    
    for iRun = 1:(length(iBinAtEachClick)-1);
        template(iBinAtEachClick(iRun)+1:iBinAtEachClick(iRun+1),2)=thisPathBinXs{iRun}(1:end);
        template(iBinAtEachClick(iRun)+1:iBinAtEachClick(iRun+1),3)=thisPathBinYs{iRun}(1:end);
    end
    PathTemplate{iPath} = template;
    
    
    % Location to bin mappings. - Only do for runs that will need it.
    if find([1:8,11:18]==pathList(iPath))
        pathData = [runPosMarkers{iPath,1},runPosMarkers{iPath,2}];
        
        binCenters(:,1) = PathTemplate{iPath}(:,2);
        binCenters(:,2) = PathTemplate{iPath}(:,3);
        
        binOcc = cell(length(PathTemplate{iPath}),nRuns(iPath));
        binDistance = cell(length(PathTemplate{iPath}),nRuns(iPath));
        fillVals = cell(nRuns(iPath),1);
        templateLoc = zeros(length(PathTemplate{iPath}),nRuns(iPath),3); %3 is nOccs, xAvg, yAvg
        
        for iRun = 1:nRuns(iPath)
            clear runActPoints
            runActPoints(:,1) = pos(pathData(iRun,1):pathData(iRun,2),2);
            runActPoints(:,2) = pos(pathData(iRun,1):pathData(iRun,2),3);
            
            %matrix of distances of all run points to all bins - size is runPoints x binCenters
            binDistMat = dist(runActPoints,binCenters');
            lostTrackingRows = sum(runActPoints,2) == 2; % Should be size runActPoints;
            [dist2ClosestBin, distInd] = min(binDistMat,[],2); % Should be size runActPoints;
            badTrackingRows = dist2ClosestBin > 20;
            closestBin = distInd(:,1);  % Should be size runActPoints; 1 is in case it is equally close to 2, grab the 1st only.
            closestBin(lostTrackingRows) = 141; %BIN 141 is throwaway!
            closestBin(badTrackingRows) = 142; %BIN 142 is throwaway!
            
            for iBin = 1:length(PathTemplate{iPath})
                binOcc{iBin,iRun} = [find(closestBin == iBin),runActPoints(closestBin == iBin,:)];
                if sum(closestBin == iBin)
                    binDistance{iBin,iRun} = dist2ClosestBin(closestBin == iBin);
                    templateLoc(iBin,iRun,1) =...
                        sum(closestBin == iBin);
                    templateLoc(iBin,iRun,2:3) =...
                        mean(runActPoints(closestBin == iBin,:),1);
                end
            end
            coordinates = templateLoc(:,iRun,2:3);
            coordinates(coordinates == 0) = NaN; %Need this for my interp. Filling ends to be same as first point.
            fillFront = find(~isnan(coordinates(:,1,1)),1,'first');
            fillBack = find(~isnan(coordinates(:,1,1)),1,'last');
            fillVals{iRun} = [fillFront,fillBack];
            coordinates(1:fillFront,1,:) = repmat(coordinates(fillFront,1,:),[fillFront,1,1]);
            coordinates(fillBack:end,1,:) = repmat(coordinates(fillBack,1,:),[length(coordinates)-fillBack+1,1,1]);
            templateLoc(:,iRun,2:3) = coordinates;
            templateLoc(:,iRun,2)=inpaint_nans(templateLoc(:,iRun,2),1);
            templateLoc(:,iRun,3)=inpaint_nans(templateLoc(:,iRun,3),1);
        end
        
        meanTemplateOcc(1:length(PathTemplate{iPath}),pathList(iPath),1:2) = ...
            reshape(mean(templateLoc(:,:,2:3),2),[length(PathTemplate{iPath}),1,2]);
        meanTemplateOcc(1:length(PathTemplate{iPath}),pathList(iPath),3:4) = ...
            reshape(std(templateLoc(:,:,2:3),0,2),[length(PathTemplate{iPath}),1,2]);
        meanTemplateOcc(1:length(PathTemplate{iPath}),pathList(iPath),5:6) = ...
            reshape(std(templateLoc(:,:,2:3),0,2)/(sqrt(nRuns(iPath)-1)),[length(PathTemplate{iPath}),1,2]);
        
        timeSpent = templateLoc(:,:,1)*10000/60;
        
        templateOccupancyStats(pathList(iPath)).binOcc = binOcc;
        templateOccupancyStats(pathList(iPath)).binDistance = binDistance;
        templateOccupancyStats(pathList(iPath)).templateLoc = templateLoc;
        templateOccupancyStats(pathList(iPath)).timeSpent = timeSpent;
        templateOccupancyStats(pathList(iPath)).binCenters = binCenters;
        templateOccupancyStats(pathList(iPath)).fillVals = fillVals;
    end
    
    % Firing Rate Vectors
    % 4th dimension is firing rate, spikes, occ
    linearRates=zeros(nCells,length(template),nRuns(iPath),3);
    
    for iCell=1:nCells
        tfile = allTfiles{iCell};
        for iRun=1:length(thisPathPosMarkers(:,1))  % each outbound run
            for iPoint = thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2)  % each time-point during this run
                if pos(iPoint,2)>1 && pos(iPoint,3)>1  % if we have valid tracking
                    templateDist = dist(pos(iPoint,2:3),template(:,2:3)');
                    [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                    spikes = find(tfile>=(pos(iPoint,1)-1/(sampleRate*2)) &...
                        tfile<(pos(iPoint,1)+1/(sampleRate*2)));
                    if nearestPointInd <= filterMedian
                        occIndFiltered = [1:nearestPointInd(1)+filterMedian];
                        linearRates(iCell,occIndFiltered,iRun,3) =...
                            linearRates(iCell,occIndFiltered,iRun,3)+...
                            filterValuesNormed(end-length(occIndFiltered)+1:end);
                        linearRates(iCell,occIndFiltered,iRun,2) =...
                            linearRates(iCell,occIndFiltered,iRun,2)+...
                            length(spikes)*filterValuesNormed(end-length(occIndFiltered)+1:end);
                    elseif nearestPointInd >= length(template)-filterMedian
                        occIndFiltered = [nearestPointInd(1)-filterMedian:length(template)];
                        linearRates(iCell,occIndFiltered,iRun,3) =...
                            linearRates(iCell,occIndFiltered,iRun,3)+...
                            filterValuesNormed(1:length(occIndFiltered));
                        linearRates(iCell,occIndFiltered,iRun,2) =...
                            linearRates(iCell,occIndFiltered,iRun,2)+...
                            length(spikes)*filterValuesNormed(1:length(occIndFiltered));
                    else
                        occIndFiltered = ...
                            [nearestPointInd(1)-filterMedian:...
                            nearestPointInd(1)+filterMedian];
                        linearRates(iCell,occIndFiltered,iRun,3) =...
                            linearRates(iCell,occIndFiltered,iRun,3)+filterValuesNormed;
                        linearRates(iCell,occIndFiltered,iRun,2) =...
                            linearRates(iCell,occIndFiltered,iRun,2)+length(spikes)*filterValuesNormed;
                    end
                end
            end
        end
        linear_ratemap_completed=iCell
    end
    
    
    %calc firing rates for each run
    rates = zeros(length(template),nRuns(iPath));
    meanLinearRates=zeros(nCells,length(template),3);
    for iCell = 1:nCells
        rates = rates*0;
        isOcc = squeeze(linearRates(iCell,:,:,3)>0);
        spikes = squeeze(linearRates(iCell,:,:,2));
        occs = squeeze(linearRates(iCell,:,:,3));
        rates(isOcc) = sampleRate*spikes(isOcc)./occs(isOcc);
        linearRates(iCell,:,:,1) = rates;
        
        %calc mean rates across laps
        meanLinearRates(iCell,:,1) = mean(rates,2);
        meanLinearRates(iCell,:,2) = std(rates,0,2);
        meanLinearRates(iCell,:,3) = std(rates,0,2)/(sqrt(nRuns(iPath)-1));
    end
    clf;
    
    
    LinearRates{iPath} = linearRates;
    MeanLinearRates{iPath} = meanLinearRates;
end
clear newInput nPts coords nBinsOneClick clickDist thisPath* eucDist stepSize
clear template* occIndFiltered nearest*
clear rates isOcc spikes occs linearRates meanLinearRates linear_ratemap_completed
clear iBin* iCell iPath iPoint iPos iTfile
clear allXsThisRun allYsThisRun
clear runActPoints binDistMat lostTrackingrows dist2ClosestBin 
clear binOcc binDistance fillVals templateLoc binCenters pathData
clear distInd badTrackingRows closestBin coordinates fill*

%% 2-D Ratemaps
for iPath = 1:nPaths
    for iCell = 1:nCells
        
        tfile = allTfiles{iCell};
        maxX = 700;
        maxY = 500;
        posSpikes=zeros(maxX,maxY);
        posOccs=zeros(maxX,maxY);
        posRates=zeros(maxX,maxY);
        
        for iRun=1:nRuns(iPath)
            for iPos=runPosMarkers{iPath,1}(iRun):runPosMarkers{iPath,2}(iRun)
                if any(pos(iPos,2:3) <= 1)
                else
                    posOccs(pos(iPos,2),pos(iPos,3)) = ...
                        posOccs(pos(iPos,2),pos(iPos,3))+1;
                
                    spikes = find(tfile>=(pos(iPos,1)-1/(sampleRate*2)) &...
                        tfile<(pos(iPos,1)+1/(sampleRate*2)));
                    posSpikes(pos(iPos,2),pos(iPos,3)) = ...
                        posSpikes(pos(iPos,2),pos(iPos,3))+length(spikes);
                end
            end
        end
        
        isOcc = posOccs>0;
        posRates(isOcc) = (posSpikes(isOcc)./posOccs(isOcc))*sampleRate;
        posRates(~isOcc) = -.0001;
        
        % Filter 2D Ratemap
        posRates=conv2(posRates,narrowGaussianNormed,'same');
        PosRateMaps{iPath}(iCell,:,:,1)=posRates;
        PosRateMaps{iPath}(iCell,:,:,2)=posSpikes;
        PosRateMaps{iPath}(iCell,:,:,3)=posOccs;
    end
end

% Overall 2D Ratemap
for iCell = 1:nCells
    
    tfile = allTfiles{iCell};
    maxX = 700;
    maxY = 500;
    posSpikes=zeros(maxX,maxY);
    posOccs=zeros(maxX,maxY);
    posRates=zeros(maxX,maxY);
    
    for iPos=1:length(pos)
        if (any(pos(iPos,2:3) <= 1))
        else
            posOccs(pos(iPos,2),pos(iPos,3)) = ...
                posOccs(pos(iPos,2),pos(iPos,3))+1;
            
            spikes = find(tfile>=(pos(iPos,1)-1/(sampleRate*2)) &...
                tfile<(pos(iPos,1)+1/(sampleRate*2)));
            posSpikes(pos(iPos,2),pos(iPos,3)) = ...
                posSpikes(pos(iPos,2),pos(iPos,3))+length(spikes);
        end
    end
    
    isOcc = posOccs>0;
    posRates(isOcc) = (posSpikes(isOcc)./posOccs(isOcc))*sampleRate;
    posRates(~isOcc) = -.0001;
    
    % Filter 2D Ratemap
    posRates=conv2(posRates,narrowGaussianNormed,'same');
    TwoDRateMaps(iCell,:,:,1)=posRates;
    TwoDRateMaps(iCell,:,:,2)=posSpikes;
    TwoDRateMaps(iCell,:,:,3)=posOccs;
end
clear iPath iCell tfile posSpikes posOccs posRates iRun iPos spikes isOcc maxX maxY

%% Save
save(saveFile, '-v7.3');

%% Plotting
% whichPathPlotMapping = [1,2,3,4,0,0,0,0,5,6,7,8,9,10,0,0,0,0,11,12];
% for iCell=1:nCells
%     disp(iCell)
%     disp(tfileList{iCell})
%     for iPath = 1:nPaths
%         maxRate(iPath) = max(MeanLinearRates{iPath}(iCell,:,1));
%         nClicks(iPath) = size(Clicks{iPath},1)-1; %cumSumNBins ignores 1st click.
%     end
%     maxY = max(maxRate);
%     
%     figure(1);
%     clf;
%     for iPath = 1:nPaths
%         whichPath = pathList(iPath);
%         if find([1:4,9:14,19:20]==whichPath) %Only plot these runs.
%             
%             subplot(6,2,whichPathPlotMapping(whichPath)); % Might change 3 to 6.
%             bar(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(iCell,:,1));
%             hold on;
%             plot(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(iCell,:,2),'g');
%             if find([1:4,11:14]==whichPath)
%                 nClicksToPlot = 3;
%                 whichClicks = [3,5,7];
%             else
%                nClicksToPlot = 2;
%                 whichClicks = [2,4]; 
%             end
%             for iClick = 1:nClicksToPlot
%                 plot([cumSumNBins{iPath}(whichClicks(iClick)-1), cumSumNBins{iPath}(whichClicks(iClick)-1)],[0, maxY+1],'r')
%             end
%             axis([0 length(MeanLinearRates{iPath}) 0 maxY+1])
%         end
%     end
%     figure(2)
%     colormap jet;
%     imagesc(squeeze(TwoDRateMaps(iCell,:,:,1)));
%     colorbar;
%     figure(3)
%     imagesc(squeeze(TwoDRateMaps(iCell,:,:,3)));
%     caxis([0 20])
%     pause;
% end
clear iCell iPath maxY iClick maxRate nClicks whichPath whichClicks nClicksToPlot whichPathPlotMapping

end
